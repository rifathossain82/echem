import 'dart:convert';
import 'dart:developer';

import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/models/response/rp_login_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../resources/api/api_checker.dart';
import '../../resources/api_provider/auth_provider.dart';
import 'auth_state.dart';


class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitialState());

  static AuthCubit get(context) => BlocProvider.of(context);

  final AuthProvider authProvider = AuthProvider();

  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _addressController = TextEditingController();
  //init
  bool _isLoading = false;
  User _userData = User();

  //Encapsulation
  bool get isLoading => _isLoading;
  User get userData => _userData;
  TextEditingController get nameController => _nameController;
  TextEditingController get phoneController => _phoneController;
  TextEditingController get emailController => _emailController;
  TextEditingController get addressController => _addressController;

  //set current user info
  void getSharedUserInfo() async {
    try {
      _isLoading = true;
      User user = await authProvider.getSharedUserInfo();
      _userData = user;
      _isLoading = false;
      emit(AuthGetState());
    } finally {
      _isLoading = false;
      emit(AuthGetState());
    }
  }

  //set current user info
  void setUserData(User user) async {
    try {
      _isLoading = true;
      User user = await authProvider.getSharedUserInfo();
      _nameController.text = user.name ?? '';
      _phoneController.text = user.phone ?? '';
      _emailController.text = user.email ?? '';
      _addressController.text = user.address ?? '';
      _isLoading = false;
      emit(AuthGetState());
    } finally {
      _isLoading = false;
      emit(AuthGetState());
    }
  }

  void updateProfile(BuildContext context) async {
  final body = {
    "phone": _phoneController.text,
    "address": _addressController.text,
    "name": _nameController.text
  };
    final response = await authProvider.profileInfoUpdate(context, body);
    log('status body${response.body}');
    if (response.statusCode == 200 || response.statusCode == 201) {
      RpLoginData data = RpLoginData.fromJson(jsonDecode(response.body));
      //dismiss dialog
      authProvider.updateSharedUserInfo(data);
      context.read<AuthCubit>().getSharedUserInfo();
      Navigator.of(context).pop();
      NavigatorHelper.openDashboardScreen(context);
      showCustomSnackBar(context, 'Profile update successfully!', isError: false);
      _isLoading = false;
      emit(AuthGetState());
    } else {
      //Dialog dismiss
      Navigator.of(context).pop();
      ApiChecker.checkApi(response, context);
    }
  }

}