abstract class AuthState {}

class AuthInitialState extends AuthState {}

class AuthLoading extends AuthState {}

class AuthGetState extends AuthState {}
