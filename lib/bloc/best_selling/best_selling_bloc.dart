import 'package:flutter_bloc/flutter_bloc.dart';
import '../../constants/strings.dart';
import '../../resources/repository/products_repository.dart';
import 'best_selling_event.dart';
import 'best_selling_state.dart';

class BestSellingBloc extends Bloc<BestSellingEvent, BestSellingState> {
  BestSellingBloc() : super(BestSellingInitial()) {
    final ProductsRepository apiRepository = ProductsRepository();


    on<GetBestSellingList>((event, emit) async {
      try {
        emit(BestSellingLoading());
        final mList = await apiRepository.fetchProductList(
          startIndex: 0,
          filterType: kProductFilterTypeBestSelling,
        );
        emit(BestSellingLoaded(mList));
      } on NetworkError {
        emit(const BestSellingError(
            "Failed to fetch data. is your device online?"));
      }
    });
  }
}
