import 'package:equatable/equatable.dart';

abstract class BestSellingEvent extends Equatable {
  const BestSellingEvent();

  @override
  List<Object> get props => [];
}

class GetBestSellingList extends BestSellingEvent {}