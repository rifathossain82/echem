import 'package:equatable/equatable.dart';
import '../../models/response/product_list.dart';

abstract class BestSellingState extends Equatable {
  const BestSellingState();

  @override
  List<Object?> get props => [];
}

class BestSellingInitial extends BestSellingState {}

class BestSellingLoading extends BestSellingState {}

class BestSellingLoaded extends BestSellingState {
  final List<ProductList> bestSelling;

  const BestSellingLoaded(this.bestSelling);
}

class BestSellingError extends BestSellingState {
  final String? message;

  const BestSellingError(this.message);
}
