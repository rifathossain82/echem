import 'package:echem/bloc/big_save/big_save_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../constants/strings.dart';
import '../../resources/repository/products_repository.dart';
import 'big_save_event.dart';

class BigSaveBloc extends Bloc<BigSaveEvent, BigSaveState> {
  BigSaveBloc() : super(BigSaveInitial()) {
    final ProductsRepository apiRepository = ProductsRepository();

    on<GetBigSaveList>((event, emit) async {
      try {
        emit(BigSaveLoading());
        final mList = await apiRepository.fetchProductList(
          startIndex: 0,
          filterType: kProductFilterTypeBigSave,
        );
        emit(BigSaveLoaded(mList));
      } on NetworkError {
        emit(
            const BigSaveError("Failed to fetch data. is your device online?"));
      }
    });
  }
}
