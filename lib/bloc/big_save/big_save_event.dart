import 'package:equatable/equatable.dart';

abstract class BigSaveEvent extends Equatable {
  const BigSaveEvent();

  @override
  List<Object> get props => [];
}

class GetBigSaveList extends BigSaveEvent {}