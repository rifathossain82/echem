import 'package:equatable/equatable.dart';
import '../../models/response/product_list.dart';

abstract class BigSaveState extends Equatable {
  const BigSaveState();

  @override
  List<Object?> get props => [];
}

class BigSaveInitial extends BigSaveState {}

class BigSaveLoading extends BigSaveState {}

class BigSaveLoaded extends BigSaveState {
  final List<ProductList> productList;

  const BigSaveLoaded(this.productList);
}

class BigSaveError extends BigSaveState {
  final String? message;

  const BigSaveError(this.message);
}
