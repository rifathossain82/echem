import 'dart:convert';
import 'dart:developer';
import 'package:echem/models/response/cart_model.dart';
import 'package:echem/models/response/rp_product_details.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../constants/style_data.dart';
import '../../resources/repository/cart_repository.dart';
import '../../resources/repository/products_repository.dart';
import 'cart_state.dart';

class CartCubit extends Cubit<CartState> {
  CartCubit() : super(CartInitialState());

  static CartCubit get(context) => BlocProvider.of(context);

  final CartRepository repository = CartRepository();
  bool _isLoading = false;
  List<CartModel> _cartList = [];
  int _totalPrice = 0;
  double _subtotalCurrencyPrice = 0.0;
  int _totalQuantity = 0;
  //Encapsulation
  bool get isLoading => _isLoading;
  int get totalPrice => _totalPrice;
  double get subtotalCurrencyPrice => _subtotalCurrencyPrice;
  int get totalQuantity => _totalQuantity;
  List<CartModel> get cartList => _cartList;

  void addToCart(BuildContext context,ProductDetails product, {
    required int quantity,
    required int price,
    required int prevPrice,
    int? stockQty,
    int? sizeIndex,
    String? sizeCurrencyPrice,
    String? size,
    String? color,}) async {
    _cartList = [];
    _isLoading = true;
    try {
      emit(CartLoading());
      var cartList = await repository.getAllCartList();
      _cartList.addAll(cartList);
      _isLoading = false;
      emit(CartGetState());
      //for adding existing items to the cart and increase quantity
      log('cart Length : ${cartList.length}');
      var contain = _cartList.where((element) => element.id == product.id);
      if (contain.isEmpty) {
        //value not exists
        log('cart value not exists ${product.id ?? 0}');

        ///added new cart item
        //add model
        double crnSizePrice = double.parse(sizeCurrencyPrice ?? '0.0');
        double totalCrnPrice = crnSizePrice + double.parse(product.price ?? '0.0');
        CartModel rpShopCartModel = CartModel(
          id: product.id ?? 0,
          qty: quantity,
          price: price,
          prevPrice: prevPrice,
          currencySizePrice: sizeCurrencyPrice,
          currencyPrice: totalCrnPrice.toString(),
          currencyPrevPrice: product.previousPrice ?? '0.0', //just currency price don't calculate size currency price
          currencyShippingFee: '0.0',
          currencyPackingFee: '0.0',
          sizeKeyIndex: sizeIndex,
          size: size,
          color: color,
          stock: stockQty,
          photo: product.photo,
          title: product.name ?? '',
          products: jsonEncode(product),
        );
        //insert cart from sqlite
        repository.insertCart(rpShopCartModel);
        //get all cart data list
        getAllCartList();
        showCustomSnackBar(context, 'Added to cart Successfully!', isError: false);
      } else {
        //value exists
        log('cart value exists ${product.id ?? 0}');

        //update cart
        for (int i = 0; i < _cartList.length; i++) {
          if (_cartList[i].id == product.id!) {
            int qty = (_cartList[i].qty ?? 0) + 1;
            log('${qty}Quantity');
            updateCartQuantity(product.id ?? 0, qty,);
            showCustomSnackBar(context,'Update from cart Successfully!', isError: false);
          }
        }
      }
    } on NetworkError {
      _isLoading = false;
    }
  }

  void getAllCartList({
    bool isTamUp = false,
  }) async {
    try {
      _isLoading = true;
      _cartList.clear();
      emit(CartGetState());
      var product = await repository.getAllCartList();
      _cartList.addAll(product);
      _isLoading = false;
      emit(CartGetState());
      totalCartSum();
    } finally {
      _isLoading = false;
      emit(CartGetState());
    }
  }

  //cart remove from item
  Future<void> removeFromCart(BuildContext context, int id) async {
    try {
      _isLoading = true;
      await repository.deleteCart(id);
      showCustomSnackBar(context, 'Delete from cart Successfully!',);
      getAllCartList();
    } finally {
      _isLoading = false;
      emit(CartGetState());
    }
  }

  Future<void> removeFullCart() async {
    try {
      _isLoading = true;
      await repository.removeFullCart();
      getAllCartList();
    } finally {
      _isLoading = false;
      emit(CartGetState());
    }
  }
  void updateCartQuantity(int id, int qty,) async {
    try {
      _isLoading = true;
     // log('${qty}Quantity check');
      await repository.updateCartQty(id, qty,);
      getAllCartList();
    } finally {
      _isLoading = false;
      emit(CartGetState());
    }
  }
  //calculation all quantity, price etc
  void totalCartSum() async {
    try {
      _isLoading = true;
      _totalPrice = 0;
      _subtotalCurrencyPrice = 0.0;
      _totalQuantity = 0;
      for (int i = 0; i < _cartList.length; i++) {
        _totalPrice = ((_cartList[i].price ?? 0) * (_cartList[i].qty ?? 0)) + _totalPrice;
        _subtotalCurrencyPrice += double.parse(_cartList[i].currencyPrice ?? '0.0') * (_cartList[i].qty ?? 0);
        _totalQuantity = (_cartList[i].qty ?? 0) + _totalQuantity;

        log('${_totalPrice}total price');
        log('${_subtotalCurrencyPrice}total currency price');
        log('${_totalQuantity}total quantity');
        _isLoading = false;
        emit(CartGetState());
      }
    } finally {
      _isLoading = false;
      emit(CartGetState());
    }
  }

}