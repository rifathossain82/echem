abstract class CartState {}

class CartInitialState extends CartState {}

class CartLoading extends CartState {}

class CartGetState extends CartState {}
