import 'package:flutter_bloc/flutter_bloc.dart';
import '../../resources/repository/api_repository.dart';
import 'category_event.dart';
import 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc() : super(CategoryInitial()) {
    on<GetCategoryList>(_categoryLoaded);
  }

  final ApiRepository apiRepository = ApiRepository();

  void _categoryLoaded(
    GetCategoryList event,
    Emitter<CategoryState> emit,
  ) async {
    try {
      emit(CategoryLoading());
      var list = await apiRepository.fetchCategoryList();
      emit(CategoryLoaded(list));
      if (list.errors != null) {
        emit(CategoryError(list.errors));
      }
    } on NetworkError {
      emit(const CategoryError("Failed to fetch data. is your device online?"));
    }
  }
}
