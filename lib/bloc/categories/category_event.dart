import 'package:equatable/equatable.dart';
abstract class CategoryEvent extends Equatable {
  const CategoryEvent();

  @override
  List<Object> get props => [];
}

class GetCategoryList extends CategoryEvent {}

class UpdateScrollIndex extends CategoryEvent {}

class UpdateExpandedPosition extends CategoryEvent {}

class UpdateSubExpandedPosition extends CategoryEvent {}