import 'package:equatable/equatable.dart';
import '../../models/response/rp_categories.dart';

abstract class CategoryState extends Equatable {
  const CategoryState();

  @override
  List<Object?> get props => [];
}

class CategoryInitial extends CategoryState {}

class CategoryLoading extends CategoryState {}

class CategoryLoaded extends CategoryState {
  final RpCategories categoryModel;

  const CategoryLoaded(
    this.categoryModel,
  );
}

class CategoryError extends CategoryState {
  final String? message;

  const CategoryError(this.message);
}
