import 'dart:convert';
import 'dart:developer';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/models/body/cart_item.dart';
import 'package:echem/models/response/city_model.dart';
import 'package:echem/models/response/rp_login_data.dart';
import 'package:echem/models/response/rp_product_details.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:echem/resources/api_provider/api_provider.dart';
import 'package:echem/resources/repository/api_repository.dart';
import 'package:echem/resources/repository/order_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sslcommerz/model/SSLCAdditionalInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCCustomerInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCEMITransactionInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCSdkType.dart';
import 'package:flutter_sslcommerz/model/SSLCShipmentInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCTransactionInfoModel.dart';
import 'package:flutter_sslcommerz/model/SSLCommerzInitialization.dart';
import 'package:flutter_sslcommerz/model/SSLCurrencyType.dart';
import 'package:flutter_sslcommerz/model/sslproductinitilizer/General.dart';
import 'package:flutter_sslcommerz/model/sslproductinitilizer/SSLCProductInitializer.dart';
import 'package:flutter_sslcommerz/sslcommerz.dart';
import '../../constants/strings.dart';
import '../../constants/style_data.dart';
import '../../models/body/create_order.dart';
import '../../models/response/cart_model.dart';
import '../../resources/api/api_checker.dart';
import '../../resources/api_provider/auth_provider.dart';
import '../../ui/widgets/custom_loader.dart';
import '../carts/cart_cubit.dart';
import 'checkout_state.dart';

class CheckoutCubit extends Cubit<CheckoutState> {
  CheckoutCubit() : super(CheckoutInitialState());

  static CheckoutCubit get(context) => BlocProvider.of(context);

  final AuthProvider authProvider = AuthProvider();
  final OrderRepository orderRepository = OrderRepository();
  final ApiProvider apiProvider = ApiProvider();

  dynamic formData = {};
  bool _isLoading = false;
  double _shippingCost = 0;
  int _subtotalPrice = 0;
  double _subtotalCurrencyPrice = 0.0;
  int _totalQty = 0;
  String _paymentType = 'cod';
  final Map _cartItem = {};
  final List<Countries> _districtList = [];
  final List<Upazilas> _upazilaList = [];

  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _addressController = TextEditingController();
  final _districtController = TextEditingController();
  final _cityController = TextEditingController();
  final _postalController = TextEditingController();
  final _noteController = TextEditingController();

  //Encapsulation
  bool get isLoading => _isLoading;

  double get shippingCost => _shippingCost;

  int get subtotalPrice => _subtotalPrice;

  double get subtotalCurrencyPrice => _subtotalCurrencyPrice;

  int get totalQty => _totalQty;

  String get paymentType => _paymentType;

  Map get cartItem => _cartItem;

  List get districtList => _districtList;

  List get upazilaList => _upazilaList;

  TextEditingController get nameController => _nameController;

  TextEditingController get phoneController => _phoneController;

  TextEditingController get emailController => _emailController;

  TextEditingController get addressController => _addressController;

  TextEditingController get districtController => _districtController;

  TextEditingController get cityController => _cityController;

  TextEditingController get postalController => _postalController;

  TextEditingController get noteController => _noteController;

  void createOrder(
    BuildContext context, {
    required String transId,
  }) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => const CustomLoader());

    CreateOrder order = CreateOrder(
        subTotal: _subtotalCurrencyPrice,
        discountAmount: 0,
        tax: 0,
        couponId: '',
        dp: '',
        deliveryCharge: _shippingCost,
        packingCost: 0,
        vendorShippingId: 0,
        vendorPackingId: 0,
        tranId: transId,
        note: _noteController.text,
        paymentType: _paymentType,
        shipping: Shipping(
            shippingType: 'address',
            phone: _phoneController.text,
            name: _nameController.text,
            email: _emailController.text,
            address: _addressController.text,
            pickupLocation: null,
            customerUpazila: _districtController.text,
            customerCity: _cityController.text,
            postCode: _postalController.text),
        cart: Cart(
            totalQty: _totalQty,
            totalPrice: _subtotalCurrencyPrice,
            items: _cartItem));
    log('order request data ${jsonEncode(order)}');
    final response = await orderRepository.createOrder(createOrder: order);
    log('status code${response.statusCode}');
    log('status body${response.body}');
    if (response.statusCode == 200 || response.statusCode == 201) {
      Map body = jsonDecode(response.body);
      //log('====> Http Response: $response.body');
      //delete cart all data
      CartCubit().removeFullCart();
      //dismiss dialog
      Navigator.of(context).pop();
      _isLoading = false;
      showCustomSnackBar(context, 'Your order place Successfully!',
          isError: false);
      if (_paymentType == 'cod') {
        //open new class
        NavigatorHelper.openCheckoutCompleteScreen(context,
            orderId: body['data']['order']['id'],
            orderNumber: '${body['data']['order']['order_number']}');
        CartCubit().getAllCartList();
      } else if (_paymentType == 'ssl') {
        sslCommerzPayment(
            context: context,
            amount:(_subtotalPrice + _shippingCost).toDouble(),
            order: order,
            transId: transId,
            orderId: body['data']['order']['id'],
            orderNumber: '${body['data']['order']['order_number']}',);
      }
    } else {
      //Dialog dismiss
      Navigator.of(context).pop();
      ApiChecker.checkApi(response, context);
    }
  }

  //set current user info
  void setUserData() async {
    try {
      _isLoading = true;
      User user = await authProvider.getSharedUserInfo();
      _nameController.text = user.name ?? '';
      _phoneController.text = user.phone ?? '';
      _emailController.text = user.email ?? '';
      _addressController.text = user.address ?? '';
      _districtController.text = user.country ?? '';
      _cityController.text = user.city ?? '';
      _postalController.text = user.zip ?? '';
      _isLoading = false;
      emit(CheckoutGetState());
    } finally {
      _isLoading = false;
      emit(CheckoutGetState());
    }
  }

  /// fetch district and upazila
  void setCity() async {
    try {
      _isLoading = true;
      CityModel cityModel = await apiProvider.fetchCity();
      _districtList.addAll(cityModel.countries ?? []);
      _upazilaList.addAll(cityModel.upazilas ?? []);
      _isLoading = false;
      emit(CheckoutGetState());
    } finally {
      _isLoading = false;
      emit(CheckoutGetState());
    }
  }

  void setCartData({
    required List<CartModel> cart,
    required int subtotal,
    required int totalQty,
    required double shippingCost,
    required double currencySubtotal,
  }) {
    _subtotalPrice = subtotal;
    _totalQty = totalQty;
    _shippingCost = shippingCost;
    _subtotalCurrencyPrice = currencySubtotal;

    for (var item in cart) {
      ProductDetails product =
          ProductDetails.fromJson(jsonDecode(item.products ?? ''));
      _cartItem[item.id.toString()] = CartItem(
        qty: item.qty,
        sizeKey: item.sizeKeyIndex,
        sizeQty: item.size != null ? item.stock.toString() : '',
        size: item.size ?? '',
        sizePrice: item.currencySizePrice ?? '',
        color: item.color ?? '',
        stock: item.stock,
        price: (double.parse(item.currencyPrice ?? '0.0')) * (item.qty ?? 0),
        itemPrice: double.parse(item.currencyPrice ?? '0.0'),
        //item
        item: Item(
          id: product.id,
          userId: product.userId,
          slug: product.slug,
          name: product.name,
          photo: product.photo,
          size: product.size.toString(),
          weight: product.weight,
          sizeQty: product.sizeQty.toString(),
          sizePrice: product.sizePrice.toString(),
          color: product.color.toString(),
          price: double.parse(product.price!),
          cashback: product.cashback,
          stock: product.stock,
          type: product.type,
          file: product.file,
          link: product.link,
          license: product.license,
          licenseQty: product.licenseQty,
          measure: product.measure,
          wholeSellQty: product.wholeSellQty.toString(),
          wholeSellDiscount: product.wholeSellDiscount.toString(),
          attributes: product.attributes,
        ),
        //under item
        license: product.license,
        dp: "",
        keys: "",
        values: "",
        weight: product.weight,
      );
    }
    log('cart map item: ${jsonEncode(_cartItem)}');
    _isLoading = false;
    emit(CheckoutGetState());
  }

  //update payment method type
  void updatePaymentType(String type) async {
    _paymentType = type;
    emit(CheckoutGetState());
  }

  //SSL commerz
  Future<void> sslCommerzPayment({
    required BuildContext context,
    required double amount,
    required CreateOrder order,
    required String transId,
    required int orderId,
    required String orderNumber,
  }) async {
    if (kStoreId.isEmpty) {
      showCustomSnackBar(context, 'Your ssl commerz store id hare');
      return;
    } else if (kStorePassword.isEmpty) {
      showCustomSnackBar(context, 'Your ssl commerz store password hare');
      return;
    }
    Sslcommerz sslCommerz = Sslcommerz(
      initializer: SSLCommerzInitialization(
        multi_card_name: formData['multicard'],
        currency: SSLCurrencyType.BDT,
        product_category: "Chemical",
        ipn_url: '$basePath/ipn',
        sdkType: SSLCSdkType.LIVE,
        store_id: kStoreId,
        store_passwd: kStorePassword,
        total_amount: amount,
        tran_id: transId,
      ),
    );
    sslCommerz
        .addEMITransactionInitializer(
          sslcemiTransactionInitializer: SSLCEMITransactionInitializer(
            emi_options: 1,
            emi_max_list_options: 3,
            emi_selected_inst: 2,
          ),
        )
        .addShipmentInfoInitializer(
          sslcShipmentInfoInitializer: SSLCShipmentInfoInitializer(
            shipmentMethod: "yes",
            numOfItems: 10,
            shipmentDetails: ShipmentDetails(
              shipAddress1: order.shipping?.address ?? '',
              shipCity: order.shipping?.customerCity ?? '',
              shipCountry: order.shipping?.customerUpazila ?? '',
              shipName: 'your ship name',
              shipPostCode: order.shipping?.postCode ?? '',
            ),
          ),
        )
        .addCustomerInfoInitializer(
          customerInfoInitializer: SSLCCustomerInfoInitializer(
            customerName: order.shipping?.name ?? '',
            customerEmail: order.shipping?.email ?? '',
            customerAddress1: order.shipping?.address ?? '',
            customerState: order.shipping?.customerUpazila ?? '',
            customerCity: order.shipping?.customerCity ?? '',
            customerPostCode: order.shipping?.postCode ?? '',
            customerCountry: 'Bangladesh',
            customerPhone: order.shipping?.postCode ?? '',
          ),
        )
        .addProductInitializer(
          sslcProductInitializer: SSLCProductInitializer(
            productName: "Gadgets",
            productCategory: "Widgets",
            general: General(
              general: "General Purpose",
              productProfile: "Product Profile",
            ),
          ),
        )
        .addAdditionalInitializer(
          sslcAdditionalInitializer: SSLCAdditionalInitializer(
            valueA: "app",
            valueB: "value b",
            valueC: "value c",
            valueD: "value d",
          ),
        );
    try {
      SSLCTransactionInfoModel result = await sslCommerz.payNow();

      if (result.status!.toLowerCase() == "failed") {
        //open new class
        NavigatorHelper.openCheckoutCompleteScreen(context,
            orderId: orderId, orderNumber: orderNumber);
        CartCubit().getAllCartList();
        showCustomSnackBar(context, 'Transaction is Failed....');
      } else {
        //open new class
        NavigatorHelper.openCheckoutCompleteScreen(context,
            orderId: orderId, orderNumber: orderNumber);
        CartCubit().getAllCartList();
        showCustomSnackBar(context, 'Payment success', isError: false);
      }
    } catch (e) {
      //open new class
      NavigatorHelper.openCheckoutCompleteScreen(context,
          orderId: orderId, orderNumber: orderNumber);
      CartCubit().getAllCartList();
      showCustomSnackBar(context, 'Payment Failed');
    }
  }
}
