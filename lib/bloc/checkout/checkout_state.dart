abstract class CheckoutState {}

class CheckoutInitialState extends CheckoutState {}

class CheckoutLoading extends CheckoutState {}

class CheckoutGetState extends CheckoutState {}
