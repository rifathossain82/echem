import 'dart:convert';
import 'dart:developer';

import 'package:echem/bloc/filter/filter_state.dart';
import 'package:echem/models/response/product_list.dart';
import 'package:echem/models/response/rp_only_category.dart';
import 'package:echem/resources/repository/api_repository.dart';
import 'package:echem/resources/repository/products_repository.dart' as pro;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../models/response/category_list.dart';
import '../../models/response/rp_brand_data.dart';
import '../products/products_bloc.dart';
import '../products/products_event.dart';

class FilterCubit extends Cubit<FilterState> {
  FilterCubit() : super(FilterInitialState());

  static FilterCubit get(context) => BlocProvider.of(context);

  final ApiRepository apiRepository = ApiRepository();
  final pro.ProductsRepository productsRepository = pro.ProductsRepository();
  RpOnlyCategory? rpOnlyCategory;
  RpBrandData? rpBrandData;
  bool _isLoading = false;
  bool _isProductLoading = true;
  bool _isLoadingMore = false;
  final bool _isProductsShimmer= false;
  List<ProductList> _productList = [];
  List<CategoryList> _categoryList = [];
  List<BrandList> _brandList = [];
  int _offset = 1;
  int _currentIndex = 0;
  bool _isSortOpen = false;
  String _selectedSort = '';
  String _selectedFilterSort = '';
  String _selectedFilterSortName = 'Sort by Relevance(Default)';
  String _selectedMinPrice = '';
  String _selectedMaxPrice = '';
  int? _selectedFilterPriceIndex;
  int? _selectedFilterBrandId;
  int? _selectedFilterCategoryId;
  int? _selectedFilterSubId;
  int _selectedFilterTypeIndex = 0;
  
 
  //Encapsulation
  bool get isLoading => _isLoading;
  bool get isProductLoading => _isProductLoading;
  bool get isLoadingMore => _isLoadingMore;
  bool get isProductsShimmer => _isProductsShimmer;
  int get offset => _offset;
  int get currentIndex => _currentIndex;
  List<ProductList> get productList => _productList;
  List<CategoryList> get categoryList => _categoryList;
  List<BrandList> get brandList => _brandList;
  bool get isSortOpen => _isSortOpen;
  int get selectedFilterTypeIndex => _selectedFilterTypeIndex;
  int? get selectedFilterCategoryId => _selectedFilterCategoryId;
  int? get selectedFilterSubId => _selectedFilterSubId;
  int? get selectedFilterPriceIndex => _selectedFilterPriceIndex;
  int? get selectedFilterBrandId => _selectedFilterBrandId;
  String get selectedSort => _selectedSort;
  String get selectedFilterSort => _selectedFilterSort;
  String get selectedFilterSortName => _selectedFilterSortName;
  String get selectedMinPrice => _selectedMinPrice;
  String get selectedMaxPrice => _selectedMaxPrice;

  //search filter
  List<Map<String, dynamic>> sortList = [
    {'name': 'Default soring', 'sort': ''},
    {'name': 'Sort by average ratting', 'sort': 'rating'},
    {'name': 'Sort by popularity', 'sort': 'popularity'},
    {'name': 'New by newness', 'sort': 'new'},
    {'name': 'New by oldest', 'sort': 'old'},
    {'name': 'Sort by price: low to high', 'sort': 'low_high'},
    {'name': 'Sort by price: high to low', 'sort': 'high_low'},
    //['low_high', 'high_low', 'new', 'old', 'rating', 'popularity']
  ];
  //pruduct filter
  List<Map<String, dynamic>> filterSortList = [
    {'name': 'Sort by Relevance(Default)', 'sort': null},
    {'name': 'New Arrival', 'sort': 'new_arrival'},
    {'name': 'Sort by price: low to high', 'sort': 'low_high'},
    {'name': 'Sort by price: high to low', 'sort': 'high_low'},
  ];
  //search screen filter type
  List<String> filterTypeList = [
    'CATEGORY',
    'BRANDS',
    'PRICE',
    'SORT',
  ];
  //filter screen
  List<String> filter2TypeList = [
    'Sub category',
    'Price',
  ];
  List<Map<String, dynamic>> filterPriceList = [
    {'name': 'BDT 0 - BDT 499', 'min': '0', 'max': '499',},
    {'name': 'BDT 500 - BDT 990', 'min': '500', 'max': '990',},
    {'name': 'BDT 1000 - BDT 1990', 'min': '1000', 'max': '1990',},
    {'name': 'BDT 2000 - BDT 2990', 'min': '2000', 'max': '2990',},
    {'name': 'BDT 3000 & Above', 'min': '3000', 'max': '300000',},
  ];


  void getProductList(bool reload,{
    int startIndex = 0,
    int postLimit = 20,
    String? searchKey,
    String? type, // ["best", 'latest', 'featured', 'top', 'hot', 'big', '	trending']
    String? sorting, //['low_high', 'high_low', 'new', 'old', 'rating']
    String? categoryId,
    String? subCatId,
    String? childCatId,
    String? brandId,
    String? minPrice,
    String? maxPrice,}) async {

    if(reload){
      _productList = [];
      _isProductLoading = true;
      emit(FilterGetCategoryState());
    }


    try {
      emit(FilterLoading());
      final posts = await productsRepository.fetchProductList(
        startIndex: _productList.length,
        postLimit: postLimit,
        search: searchKey,
        carId: categoryId,
        subCatId: subCatId,
        childCatId: childCatId,
        filterType: type,
        sort: sorting,
        brandId: brandId,
        minPrice: minPrice,
        maxPrice: maxPrice,
      );
      if(_productList.isEmpty){
        _productList.clear();
        getCategoryList();
        log('clear products');
        if(posts.isNotEmpty){
          _productList.addAll(posts);
          log('add products: ${jsonEncode(_productList)}');
        }
      }else{
        if(posts.isNotEmpty){
          for(var product in posts){
            _productList.add(product);
            log('add products: ${jsonEncode(_productList)}');
          }
        }
      }
      /*if(posts.isNotEmpty){
        _productList.add(posts);
        log('add products: ${jsonEncode(_productList)}');
      }*/
      _isProductLoading = false;
      _isLoadingMore = false;
      emit(FilterGetCategoryState());
    } on NetworkError {
      //emit(const DetailsError("Failed to fetch data. is your device online?"));
    }
  }
  void changeOffset() {
    ++_offset;
    emit(FilterGetCategoryState());
  }

  void showBottomLoader() {
    _isLoadingMore = true;
    emit(FilterGetCategoryState());
  }

  void getCategoryList() async {
    rpOnlyCategory = null;
    _categoryList = [];
    _isLoading = true;

    try {
      emit(FilterLoading());
      final data = await apiRepository.fetchOnlyCategoryList();
      rpOnlyCategory = data;
      if(rpOnlyCategory!.data!.isNotEmpty){
        _categoryList.assignAll(rpOnlyCategory!.data!);
      }
      _isLoading = false;
      emit(FilterGetCategoryState());
      getBrandList();
    } on NetworkError {
      //emit(const DetailsError("Failed to fetch data. is your device online?"));
    }
  }

  void getBrandList() async {
    rpBrandData = null;
    _brandList = [];
    _isLoading = true;

    try {
      emit(FilterLoading());
      final data = await apiRepository.fetchBrandList();
      rpBrandData = data;
      if(rpBrandData!.data!.isNotEmpty){
        _brandList.addAll(rpBrandData!.data!);
      }
      _isLoading = false;
      emit(FilterGetBrandState());

    } on NetworkError {
      //emit(const DetailsError("Failed to fetch data. is your device online?"));
    }
  }
  //Slider update current index
  void setCurrentIndex(int index) {
    _currentIndex = index;
    emit(FilterGetState());
  }

  void updateFilterType(int index) {
    _selectedFilterTypeIndex = index;
    emit(FilterGetState());
  }

  void updateFilterCategory(int id) {
    if(id == _selectedFilterCategoryId){
      _selectedFilterCategoryId = null;
    }else{
      _selectedFilterCategoryId = id;
    }
    emit(FilterGetState());
  }
  void updateFilterSubCat(int id) {
    if(id == _selectedFilterSubId){
      _selectedFilterSubId = null;
    }else{
      _selectedFilterSubId = id;
    }
    emit(FilterGetState());
  }
  void updateFilterBrand(int id) {
    if(id == _selectedFilterBrandId){
      _selectedFilterBrandId = null;
    }else{
      _selectedFilterBrandId = id;
    }
    emit(FilterGetState());
  }

  void updateFilterPrice(int index,String min, String max) {
    if(_selectedFilterPriceIndex == index){
      _selectedFilterPriceIndex = null;
    }else{
      _selectedFilterPriceIndex = index;
    }
    _selectedMaxPrice = max;
    _selectedMinPrice = min;
    emit(FilterGetState());
  }
  void sortSelectedUpdate(String name) {
    _isSortOpen = !_isSortOpen;
    _selectedSort = name;
    emit(FilterGetState());
  }
  void updateFilterSort(String sortType, String name) {
    _isSortOpen = !_isSortOpen;
    _selectedFilterSort = sortType;
    _selectedFilterSortName = name;
    _selectedFilterSubId = null;
    _selectedMinPrice = '';
    _selectedMaxPrice = '';
    emit(FilterGetState());
  }
  void resetFilter(){
    _selectedFilterTypeIndex = 0;
    _selectedFilterCategoryId = null;
    _selectedFilterBrandId = null;
    _selectedFilterPriceIndex = null;
    _selectedMaxPrice = '';
    _selectedMinPrice = '';
    emit(FilterGetState());
  }
  ProductBloc addedFilterAction(){
    return ProductBloc()..add(ProductsFetched());
  }

}