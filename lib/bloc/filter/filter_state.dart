abstract class FilterState {}

class FilterInitialState extends FilterState {}

class FilterLoading extends FilterState {}

class FilterGetState extends FilterState {}
class FilterGetCategoryState extends FilterState {}
class FilterGetBrandState extends FilterState {}
