import 'package:flutter_bloc/flutter_bloc.dart';
import '../../resources/repository/products_repository.dart';
import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    final ProductsRepository apiRepository = ProductsRepository();

    on<GetSliderList>((event, emit) async {
      try {
        emit(SliderLoading());
        final mList = await apiRepository.fetchSliderList();
        final banner = await apiRepository.fetchBannerList();
        emit(SliderLoaded(mList,banner));
        if (mList.errors != null) {
          emit(SliderError(mList.errors));
        }
      } on NetworkError {
        emit(const SliderError("Failed to fetch data. is your device online?"));
      }
    });

  }
}
