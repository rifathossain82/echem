import 'package:echem/models/response/rp_banner_data.dart';
import 'package:equatable/equatable.dart';
import '../../models/response/rp_slider.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object?> get props => [];
}

class HomeInitial extends HomeState {}

class SliderLoading extends HomeState {}

class SliderLoaded extends HomeState {
  final RpSlider sliderModel;
  final RpBannerData bannerData;

  const SliderLoaded(this.sliderModel, this.bannerData,);
}


class SliderError extends HomeState {
  final String? message;

  const SliderError(this.message);
}

