import 'package:flutter_bloc/flutter_bloc.dart';
import '../../constants/strings.dart';
import '../../resources/repository/products_repository.dart';
import 'new_arrival_event.dart';
import 'new_arrival_state.dart';

class NewArrivalBloc extends Bloc<NewArrivalEvent, NewArrivalState> {
  NewArrivalBloc() : super(NewArrivalInitial(),) {
    final ProductsRepository apiRepository = ProductsRepository();

    on<GetNewArrivalList>((event, emit) async {
      try {
        emit(NewArrivalLoading());
        final mList = await apiRepository.fetchProductList(
          startIndex: 0,
          filterType: kProductFilterTypeNewArrival,
        );
        emit(NewArrivalLoaded(mList));
      } on NetworkError {
        emit(
            const NewArrivalError("Failed to fetch data. is your device online?"));
      }
    });
  }
}
