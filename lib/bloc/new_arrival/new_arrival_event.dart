import 'package:equatable/equatable.dart';

abstract class NewArrivalEvent extends Equatable {
  const NewArrivalEvent();

  @override
  List<Object> get props => [];
}

class GetNewArrivalList extends NewArrivalEvent {}