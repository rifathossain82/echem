import 'package:equatable/equatable.dart';
import '../../models/response/product_list.dart';

abstract class NewArrivalState extends Equatable {
  const NewArrivalState();

  @override
  List<Object?> get props => [];
}

class NewArrivalInitial extends NewArrivalState {}

class NewArrivalLoading extends NewArrivalState {}

class NewArrivalLoaded extends NewArrivalState {
  final List<ProductList> productList;

  const NewArrivalLoaded(this.productList);
}

class NewArrivalError extends NewArrivalState {
  final String? message;

  const NewArrivalError(this.message);
}
