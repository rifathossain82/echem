import 'package:flutter_bloc/flutter_bloc.dart';
import '../../resources/repository/api_repository.dart';
import 'only_category_event.dart';
import 'only_category_state.dart';

class OnlyCategoryBloc extends Bloc<OnlyCategoryEvent, OnlyCategoryState> {
  OnlyCategoryBloc() : super(CategoryInitial()) {
    on<GetCategoryList>(_categoryLoaded);

  }
  final ApiRepository apiRepository = ApiRepository();

  void _categoryLoaded(
    GetCategoryList event,
    Emitter<OnlyCategoryState> emit,
  ) async {
    try {
      emit(CategoryLoading());
      final mList = await apiRepository.fetchOnlyCategoryList();
      emit(CategoryLoaded(mList));
      if (mList.error != null) {
        emit(CategoryError(mList.error));
      }
    } on NetworkError {
      emit(const CategoryError("Failed to fetch data. is your device online?"));
    }
  }
}
