import 'package:equatable/equatable.dart';

abstract class OnlyCategoryEvent extends Equatable {
  const OnlyCategoryEvent();

  @override
  List<Object> get props => [];
}

class GetCategoryList extends OnlyCategoryEvent {}
