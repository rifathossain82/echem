import 'package:equatable/equatable.dart';

import '../../models/response/rp_only_category.dart';

abstract class OnlyCategoryState extends Equatable {
  const OnlyCategoryState();

  @override
  List<Object?> get props => [];
}

class CategoryInitial extends OnlyCategoryState {}

class CategoryLoading extends OnlyCategoryState {}

class CategoryLoaded extends OnlyCategoryState {
  final RpOnlyCategory categoryModel;
  const CategoryLoaded(this.categoryModel);
}

class CategoryError extends OnlyCategoryState {
  final String? message;
  const CategoryError(this.message);
}