import 'dart:async';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';
import '../../resources/repository/order_repository.dart';
import 'order_state.dart';
import 'orders_event.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  OrderBloc({this.orderLimit = 20, this.orderId}) : super(const OrderState()) {
    on<OrderFetched>(_onPostFetched, transformer: throttleDroppable(throttleDuration),);
    on<OrderDetailsFetched>(_onDetailsFetch);
  }

  final OrderRepository apiRepository = OrderRepository();
  final int orderLimit;
  final int? orderId;

  Future<void> _onPostFetched(OrderFetched event,
      Emitter<OrderState> emit,) async {
    if (state.hasReachedMax) return;
    try {
      if (state.status == OrderStatus.initial) {
        final posts = await apiRepository.fetchOrderList(postLimit: orderLimit);
        return emit(
          state.copyWith(
            status: OrderStatus.success,
            posts: posts,
            hasReachedMax: false,
            orderDetails: null,
          ),
        );
      }
      final posts = await apiRepository.fetchOrderList(
        startIndex: state.posts.length,
        postLimit: orderLimit,
      );
      posts.isEmpty
          ? emit(state.copyWith(hasReachedMax: true))
          : emit(
        state.copyWith(
          status: OrderStatus.success,
          posts: List.of(state.posts)..addAll(posts),
          orderDetails: null,
          hasReachedMax: false,
        ),
      );
    } catch (_) {
      emit(state.copyWith(status: OrderStatus.failure));
    }
  }

  Future<void> _onDetailsFetch(OrderDetailsFetched event,
      Emitter<OrderState> emit,) async {
    if (state.hasReachedMax) return;
    try {
      final orderDetails = await apiRepository.fetchOrderDetails(orderId: orderId,);
      return emit(
        state.copyWith(
            status: OrderStatus.success,
            posts: state.posts.isEmpty ? [] : state.posts,
            hasReachedMax: false,
            orderDetails: orderDetails,
        ),
      );
    } catch (_) {
      emit(state.copyWith(status: OrderStatus.failure));
    }
  }
}
