import 'package:equatable/equatable.dart';
import '../../models/response/rp_order_details.dart';
import '../../models/response/rp_order_list.dart';

enum OrderStatus { initial, success, failure }

class OrderState extends Equatable {
  const OrderState({
    this.status = OrderStatus.initial,
    this.posts = const <OrderList>[],
    this.hasReachedMax = false,
    this.orderDetails,
  });

  final OrderStatus status;
  final List<OrderList> posts;
  final bool hasReachedMax;
  final RpOrderDetails? orderDetails;

  OrderState copyWith({
    OrderStatus? status,
    List<OrderList>? posts,
    bool? hasReachedMax,
    RpOrderDetails? orderDetails,
  }) {
    return OrderState(
      status: status ?? this.status,
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      orderDetails: orderDetails ?? this.orderDetails,
    );
  }

  @override
  String toString() {
    return '''OrderState { status: $status, hasReachedMax: $hasReachedMax, posts: ${posts.length} }''';
  }

  @override
  List<Object> get props => [status, posts, hasReachedMax];
}
