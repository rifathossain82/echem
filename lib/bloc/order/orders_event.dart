import 'package:equatable/equatable.dart';

abstract class OrderEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class OrderFetched extends OrderEvent {}

class OrderDetailsFetched extends OrderEvent {}