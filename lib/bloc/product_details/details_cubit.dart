import 'package:echem/utils/base_method.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import '../../models/response/product_list.dart';
import '../../models/response/rp_product_details.dart';
import '../../resources/repository/products_repository.dart';
import 'details_state.dart';

class DetailsCubit extends Cubit<DetailsState> {
  DetailsCubit() : super(DetailsInitialState());

  static DetailsCubit get(context) => BlocProvider.of(context);

  final ProductsRepository apiRepository = ProductsRepository();
  RpProductDetails? rpProductDetails;
  ProductDetails productDetails = ProductDetails();
  bool _isLoading = false;
  bool _isProductsShimmer= false;
  List<ProductList> _productList = [];
  List<Galleries> _productGallery = [];
  int _currentIndex = 0;
  String? _selectedSize;
  String? _selectedCurrencySizePrice;
  String? _selectedColor;
  String? _selectedSizeStock;
  int? _selectedSizeIndex;
  int? _selectedPrice;

  int bottomNavigtionIndex = 0;
  List<String> titles = ['Todo Tasks', 'Done Tasks', 'Archived Tasks'];
  bool isBottomSheetShown = false;
  Icon floatingButtonIcon = const Icon(Icons.edit);
  //Encapsulation
  bool get isLoading => _isLoading;
  bool get isProductsShimmer => _isProductsShimmer;
  int get currentIndex => _currentIndex;
  String? get selectedSize => _selectedSize;
  String? get selectedColor => _selectedColor;
  String? get selectedSizeStock => _selectedSizeStock;
  String? get selectedCurrencySizePrice => _selectedCurrencySizePrice;
  int? get selectedSizeIndex => _selectedSizeIndex;
  int? get selectedPrice => _selectedPrice;
  List<ProductList> get productList => _productList;
  List<Galleries> get productGallery => _productGallery;
/*  void changeIndex(int index) {
    bottomNavigtionIndex = index;
    emit(AppChangeBottomNavBarState());
  }*/

  getProductDetails(String productId) async {
    rpProductDetails = null;
    _productGallery = [];
    productDetails = ProductDetails();
    _isLoading = true;

    try {
      emit(DetailsLoading());
      final data = await apiRepository.fetchProductDetails(productId);
      rpProductDetails = data;
      if(rpProductDetails?.data != null){
        productDetails = rpProductDetails!.data!;
      }
      _isLoading = false;
      emit(DetailsGetState());
      //added featured image in gallery image list
      addFeaturedImage(productDetails);
      //fetch sub category product list (use like me products)
      getLikeProducts((productDetails.subcategoryId ?? 0).toString());
    } on NetworkError {
      //emit(const DetailsError("Failed to fetch data. is your device online?"));
    }
  }
  
  void getLikeProducts(String subCategoryId) async {
    _isProductsShimmer = true;
    _productList = [];
    try {
      emit(DetailsLoading());
      final mList = await apiRepository.fetchProductList(startIndex: 0, subCatId: subCategoryId, postLimit: 15 );
      _productList.addAll(mList);
      _isProductsShimmer = false;
      emit(DetailsGetState());
    } on NetworkError {
      //emit(const DetailsError("Failed to fetch data. is your device online?"));
    }
  }
  
  //Slider update current index
  void setCurrentIndex(int index,) {
    _currentIndex = index;
    emit(DetailsGetState());
  }
  
  //added featured image in gallery list
  void addFeaturedImage(ProductDetails product){
    _productGallery = product.galleries!;
    _productGallery.add(Galleries(productId: product.id, photo: product.photo,));
    emit(DetailsGetState());
   /* for(var img in _productGallery){
      log('Product image: ${img.photo}');
    }*/
  }
  void updateSizeSelected(String size, String stock, String price, String mPrice, int index){
    _selectedSizeIndex = index;
    _selectedSize = size;
    _selectedCurrencySizePrice = price;
    _selectedSizeStock = stock;
    _selectedPrice = (BaseMethod.currencyToPriceCalc(mPrice: price) + BaseMethod.currencyToPriceCalc(mPrice: mPrice));
    emit(DetailsGetState());
  }
  void updateColorSelected(String color){
    _selectedColor = color;
    emit(DetailsGetState());
  }
  //context.read<ProductBloc>().add(ProductsFetched())
}