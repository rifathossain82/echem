abstract class DetailsState {}

class DetailsInitialState extends DetailsState {}

class DetailsLoading extends DetailsState {}

class DetailsGetState extends DetailsState {}

/*class DetailsError extends DetailsState {
  final String? message;

  const DetailsError(this.message);
}*/
class DetailsChangeBottomNavBarState extends DetailsState {}

class DetailsCreateDatabaseState extends DetailsState {}

class DetailsInsertDatabaseState extends DetailsState {}

class DetailsChangeBottomSheetState extends DetailsState {}

class DetailsUpdateDatabaseState extends DetailsState {}

class DetailsDeleteDatabaseState extends DetailsState {}