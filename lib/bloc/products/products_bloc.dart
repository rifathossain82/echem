import 'dart:async';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:echem/bloc/products/products_event.dart';
import 'package:echem/bloc/products/products_state.dart';
import 'package:echem/resources/repository/products_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class ProductBloc extends Bloc<ProductsEvent, ProductsState> {
  ProductBloc({
    this.searchKey,
    this.categoryId,
    this.subCatId,
    this.childCatId,
    this.type,
    this.sorting,
    this.brandId,
    this.minPrice,
    this.maxPrice,
    this.isFilter = false,
  }) : super(const ProductsState()) {
    on<ProductsFetched>(
      _onPostFetched,
      transformer: throttleDroppable(throttleDuration),
    );
  }

  final ProductsRepository apiRepository = ProductsRepository();
  final String? searchKey, categoryId, subCatId, childCatId, type, minPrice, maxPrice, brandId, sorting;
  final bool isFilter;
  Future<void> _onPostFetched(
    ProductsFetched event,
    Emitter<ProductsState> emit,
  ) async {
    if (state.hasReachedMax) return;
    try {
      if(isFilter){
        //emit(state.copyWith(hasReachedMax: true, posts: [], status: ProductStatus.initial));

      }else{
        if (state.status == ProductStatus.initial) {
          final posts = await apiRepository.fetchProductList(
            search: searchKey,
            carId: categoryId,
            subCatId: subCatId,
            childCatId: childCatId,
            filterType: type,
            sort: sorting,
            brandId: brandId,
            minPrice: minPrice,
            maxPrice: maxPrice,
          );
          return emit(
            state.copyWith(
              status: ProductStatus.success,
              posts: posts,
              hasReachedMax: false,
            ),
          );
        }
        final posts = await apiRepository.fetchProductList(
          startIndex: state.posts.length,
          search: searchKey,
          carId: categoryId,
          subCatId: subCatId,
          childCatId: childCatId,
          filterType: type,
          sort: sorting,
          brandId: brandId,
          minPrice: minPrice,
          maxPrice: maxPrice,
        );
        posts.isEmpty
            ? emit(state.copyWith(hasReachedMax: true))
            : emit(
          state.copyWith(
            status: ProductStatus.success,
            posts: List.of(state.posts)..addAll(posts),
            hasReachedMax: false,
          ),
        );
      }
    } catch (_) {
      emit(state.copyWith(status: ProductStatus.failure));
    }
  }
}
