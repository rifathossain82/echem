import 'package:equatable/equatable.dart';
import '../../models/response/product_list.dart';

enum ProductStatus { initial, success, failure }

class ProductsState extends Equatable {
  const ProductsState({
    this.status = ProductStatus.initial,
    this.posts = const <ProductList>[],
    this.hasReachedMax = false,
  });

  final ProductStatus status;
  final List<ProductList> posts;
  final bool hasReachedMax;

  ProductsState copyWith({
    ProductStatus? status,
    List<ProductList>? posts,
    bool? hasReachedMax,
  }) {
    return ProductsState(
      status: status ?? this.status,
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() {
    return '''ProductsState { status: $status, hasReachedMax: $hasReachedMax, posts: ${posts.length} }''';
  }

  @override
  List<Object> get props => [status, posts, hasReachedMax];
}
