import 'dart:async';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:echem/resources/repository/api_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';

import 'videos_event.dart';
import 'videos_state.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class VideosBloc extends Bloc<VideosEvent, VideosState> {
  VideosBloc({this.postLimit = 20}) : super(const VideosState()) {
    on<VideosFetched>(
      _onPostFetched,
      transformer: throttleDroppable(throttleDuration),
    );
  }

  final ApiRepository apiRepository = ApiRepository();
  final int postLimit;

  Future<void> _onPostFetched(
    VideosFetched event,
    Emitter<VideosState> emit,
  ) async {
    if (state.hasReachedMax) return;
    try {
      if (state.status == VideoStatus.initial) {
        final posts = await apiRepository.fetchVideoList(postLimit: postLimit);
        return emit(
          state.copyWith(
            status: VideoStatus.success,
            posts: posts,
            hasReachedMax: false,
          ),
        );
      }
      final posts = await apiRepository.fetchVideoList(
        startIndex: state.posts.length,
      );
      posts.isEmpty
          ? emit(state.copyWith(hasReachedMax: true))
          : emit(
              state.copyWith(
                status: VideoStatus.success,
                posts: List.of(state.posts)..addAll(posts),
                hasReachedMax: false,
              ),
            );
    } catch (_) {
      emit(state.copyWith(status: VideoStatus.failure));
    }
  }
}
