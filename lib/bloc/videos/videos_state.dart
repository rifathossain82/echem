import 'package:equatable/equatable.dart';
import '../../models/response/rp_video_data.dart';
enum VideoStatus { initial, success, failure }

class VideosState extends Equatable {
  const VideosState({
    this.status = VideoStatus.initial,
    this.posts = const <VideoList>[],
    this.hasReachedMax = false,
  });

  final VideoStatus status;
  final List<VideoList> posts;
  final bool hasReachedMax;

  VideosState copyWith({
    VideoStatus? status,
    List<VideoList>? posts,
    bool? hasReachedMax,
  }) {
    return VideosState(
      status: status ?? this.status,
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() {
    return '''VideosState { status: $status, hasReachedMax: $hasReachedMax, posts: ${posts.length} }''';
  }

  @override
  List<Object> get props => [status, posts, hasReachedMax];
}
