import 'package:echem/constants/strings.dart';

class Images {
  //Image PNG OR JPG
  static const String logo = kImageDir + 'logo.png';
  static const String homeLogo = kImageDir + 'home_logo.png';
  static const String userPlaceHolder = kImageDir + 'user_place_holder.png';
  static const String placeHolder = kImageDir + 'placeholder.jpg';
  static const String intro1 = kImageDir + 'intro1.png';
  static const String intro2 = kImageDir + 'intro2.png';
  static const String intro3 = kImageDir + 'intro3.png';
  static const String introDark1 = kImageDir + 'intro1.png';
  static const String introDark2 = kImageDir + 'intro2.png';
  static const String introDark3 = kImageDir + 'intro3.png';

  //SVG Image
  static const String home = kIConDir + 'home.svg';
  static const String account = kIConDir + 'account.svg';
  static const String cart = kIConDir + 'cart.svg';
  static const String cartProcessing = kIConDir + 'cart_processing.svg';
  static const String review = kIConDir + 'review.svg';
  static const String category = kIConDir + 'category.svg';
  static const String close = kIConDir + 'close.svg';
  static const String filter = kIConDir + 'filter.svg';
  static const String filterSearch = kIConDir + 'filter_search.svg';
  static const String sort = kIConDir + 'sort.svg';
  static const String heart = kIConDir + 'heart.svg';
  static const String heartBorder = kIConDir + 'heart_border.svg';
  static const String returnIcon = kIConDir + 'return.svg';
  static const String arrowRight = kIConDir + 'arrow_right.svg';
  static const String facebook = kIConDir + 'facebook.svg';
  static const String location = kIConDir + 'location.svg';
  static const String call = kIConDir + 'call.svg';
  static const String chat = kIConDir + 'chat.svg';
  static const String card = kIConDir + 'card.svg';
}
