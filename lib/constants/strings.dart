import 'package:echem/main.dart';
import '../resources/api/app_config.dart';
const String kIConDir = 'assets/icons/';
const String kImageDir = 'assets/images/';
const String kFlagDir = 'assets/flags/';
const String kBase64Extend = 'data:image/jpeg;base64,';
const String kBase64ExtendVideo = 'data:video/mp4;base64,';
const String kBase64ExtendAudio = 'data:audio/mp3;base64,';

///Combined User Id to create single chatroom id
String combinedUserId(String id1, String id2) {
  return id1.compareTo(id2) > 0 ? '$id1-$id2' : '$id2-$id1';
}
//current sign
String kCurrency = getCurrencySign();
String getCurrencySign() {
  String sign = '৳';
  if(prefs.containsKey(currencySign)){
    sign = prefs.getString(currencySign) ?? '৳';
  }
  return sign;
}
const int timeoutRequest = 60;
const String serverDateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS'Z'";
//SSL Commerz credential
String kStoreId = 'YourID';
String kStorePassword = 'YourPassword';
// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final RegExp htmlValidatorRegExp = RegExp(r"(<[^>]*>|&\w+;)");
const String kEmailNullError = "Please enter your email";
const String kInvalidEmailError = "Your email address is not valid";
const String kPassNullError = "Please enter your password";
const String kPassNewNullError = "Please enter your New password";
const String kPassConfirmNullError = "Please enter your Confirm password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNameNullError = "Please enter your full name";
const String kPhoneNumberNullError = "Please enter your phone number";
const String kAddressNullError = "Please enter your address";
const String kInvalidNumberError = "Invalid phone number";
const String kMinWithdrawLimit = "Minimum withdraw limit 1 USD";
const String kServerTimeoutMessage = "Request Time out. Please try again!";
const String kServerCode500Message =
    "OOPS! Server error. Sorry there were some technical issues while processing your request.";

///****Don't change***
//sql db data
const String dbName = '$appName.db';
const String tableNameCart = 'cart';
const int dbVersion = 1;

//["best", 'latest', 'featured', 'top', 'hot', 'big', '	trending']
const String kProductFilterTypeBestSelling = 'best';
const String kProductFilterTypeNewArrival = 'latest';
const String kProductFilterTypeFeatured = 'featured';
const String kProductFilterTypeTop = 'top';
const String kProductFilterTypeHot = 'hot';
const String kProductFilterTypeBigSave = 'big';
const String kProductFilterTypeTrending = 'trending';
//firebase notification subscriber name
const String topicName = 'web_app';
// Shared Key
const String theme = 'theme';
const String userInfo = 'user_info';
const String token = 'token';
const String userId = 'user_id';
const String userAvatar = 'user_avatar';
const String userName = 'user_name';
const String userPassword = 'user_pass';
const String userDisplayName = 'display_name';
const String userEmail = 'user_email';
const String firstName = 'first_name';
const String lastName = 'last_name';
const String userPhone = 'user_phone';
const String userAddress = 'user_address';
const String currencyName = 'currency_price';
const String currencyPrice = 'currency_price';
const String currencySign = 'currency_sign';

///****Don't change end***
//Image show and hide
const bool isImageShow = true;

const String kPrivacyPolices = "";

const String kTermsConditions = "";

List demoList = [
  'https://echem.com.bd/assets/images/sliders/1650782933oil.jpg',
  'https://echem.com.bd/assets/images/sliders/1619946753Echem-Screen-Printing.jpg',
  'https://echem.com.bd/assets/images/sliders/1656756083www.reallygreatsite.com.png',
  'https://echem.com.bd/assets/images/sliders/1649238725Deliv.jpg',
];
List detailsSliderList = [
  'https://media.allure.com/photos/5bfc35a6e2f5932d13493a64/1:1/w_2000,h_2000,c_limit/Sleeping%20Collagen%20Lid%20on%20Straight%20On.png',
  'https://cdn.shopify.com/s/files/1/0062/2516/6418/products/LazySusanBeautyOrganiser2_2048x.png?v=1629182859',
  'https://freepngclipart.com/download/cosmetics/69566-natural-beauty-products-skin-anti-aging-moisturizer-care.png',
  'https://media.allure.com/photos/5d5422293bc0000008e44fa4/master/w_1600%2Cc_limit/Beauty%2520Bosses%2520RPKGE_WholeFoods.png',
  'https://media.allure.com/photos/5f5908b3c1406ccc2076cd61/1:1/w_1314,h_1314,c_limit/Elaluz-BeautyOil-front-900x1400.png',
  'https://www.tofusecret.com/wp-content/uploads/2020/12/skii_banner_image-1.png',
  'https://www.tofusecret.com/wp-content/uploads/2020/12/shiseido_banner_image-1.png',
  'https://assets.teenvogue.com/photos/6260540d5154704efd48bbda/1:1/w_2560%2Cc_limit/FS_SPR_SUM22_T2PRODUCT_SILO_COOKIES_N_CLEAN_DETOX_FACE_MASK_OPEN_2000x2000.png',
];
List<String> cancelTypeList  = [
 'Change/Combine Order',
 'Offer Price Change',
 'Duplicate Order',
 'Mistakenly Placed',
 'Financial Issue',
 'Change Payment Method',
 'Forgot to Use Coupon/ Coupon Issue',
 'Change Delivery Address/ Contact No',
 'Found Cheaper Elsewhere',
 'Other',
];

List<Map<String, String>> categoryList = [
  {
    'image':
        'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image': 'https://cdn-icons-png.flaticon.com/512/1024/1024506.png',
    'name': 'Equipment'
  },
  {
    'image':
        'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
        'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image': 'https://cdn-icons-png.flaticon.com/512/1024/1024506.png',
    'name': 'Equipment'
  },
  {
    'image':
        'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
        'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image': 'https://cdn-icons-png.flaticon.com/512/1024/1024506.png',
    'name': 'Equipment'
  }
];

List<Map<String, String>> categoryList2 = [
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
  {
    'image':
    'https://i.postimg.cc/RhKgY3Hy/1620029696hkhjk-removebg-preview.png',
    'name': 'Refurbished'
  },
  {
    'image':
    'https://i.postimg.cc/HW7jFW8z/1620028363fdgfhfh-removebg-preview.png',
    'name': 'Chemicals'
  },
  {
    'image':
    'https://i.postimg.cc/4xFjVG47/1620029213fghgj-removebg-preview.png',
    'name': 'Equipment'
  },
];