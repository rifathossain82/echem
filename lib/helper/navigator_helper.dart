import 'package:echem/ui/screens/account/account_screen.dart';
import 'package:echem/ui/screens/auth/forgot_screen.dart';
import 'package:echem/ui/screens/auth/login_screen.dart';
import 'package:echem/ui/screens/auth/signup/signup__verification_screen.dart';
import 'package:echem/ui/screens/auth/signup/signup_screen.dart';
import 'package:echem/ui/screens/checkout/checkout_address_screen.dart';
import 'package:echem/ui/screens/checkout/checkout_screen.dart';
import 'package:echem/ui/screens/dashboard/dashboard_screen.dart';
import 'package:echem/ui/screens/details/product_details.dart';
import 'package:echem/ui/screens/intro/intro_pages.dart';
import 'package:echem/ui/screens/order/order_details_screen.dart';
import 'package:echem/ui/screens/order/order_screen.dart';
import 'package:echem/ui/screens/product_list/product_filter_screen.dart';
import 'package:echem/ui/screens/search/search_screen.dart';
import 'package:flutter/cupertino.dart';
import '../models/response/cart_model.dart';
import '../models/response/rp_categories.dart';
import '../ui/screens/account/profile_update_screen.dart';
import '../ui/screens/checkout/checkout_completed_screen.dart';
import '../ui/screens/product_list/product_list_screen.dart';

class NavigatorHelper {
  static goBack(
    BuildContext context,
  ) {
    return Navigator.of(context).pop();
  }

  static openDashboardScreen(BuildContext context, {int index = 0}) {
    return Navigator.pushAndRemoveUntil(
      context,
      CupertinoPageRoute(
        builder: (context) => DashboardScreen(
          index: index,
        ),
      ),
      (Route<dynamic> route) => false,
    );
  }

  static openIntroScreen(BuildContext context, {int index = 0}) {
    return Navigator.pushAndRemoveUntil(
      context,
      CupertinoPageRoute(
        builder: (context) => const IntroPages(),
      ),
      (Route<dynamic> route) => false,
    );
  }

  static openLoginScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const LoginScreen(),
      ),
    );
  }

  static openLoginScreenReplace(
    BuildContext context,
  ) {
    return Navigator.pushReplacement(
      context,
      CupertinoPageRoute(
        builder: (context) => const LoginScreen(),
      ),
    );
  }

  static openSignupScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const SignupScreen(),
      ),
    );
  }

  static openSignupVerificationScreen(
    BuildContext context, {
    required String email,
  }) {
    return Navigator.pushReplacement(
      context,
      CupertinoPageRoute(
        builder: (context) => SignupVerificationScreen(
          email: email,
        ),
      ),
    );
  }

  static openForgotScreen(
    BuildContext context, {
    required String email,
  }) {
    return Navigator.pushReplacement(
      context,
      CupertinoPageRoute(
        builder: (context) => ForgotScreen(
          email: email,
        ),
      ),
    );
  }

  static openAccountScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const AccountScreen(),
      ),
    );
  }

  static openAccountUpdateScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const ProfileUpdateScreen(),
      ),
    );
  }

  static openSearchScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const SearchScreen(),
      ),
    );
  }

  static openFilterScreen(
    BuildContext context, {
    required List<Subs> subCategoryList,
    String? categoryId,
    String? subCatId,
    String? childCatId,
  }) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => ProductFilterScreen(
          subCategoryList: subCategoryList,
          categoryId: categoryId,
          subCatId: subCatId,
          childCatId: childCatId,
        ),
      ),
    );
  }

  static openProductListScreen(
    BuildContext context, {
    int startIndex = 0,
    int postLimit = 20,
    String? searchKey,
    String? type,
    String? sorting,
    String? categoryId,
    String? subCatId,
    String? childCatId,
    String? brandId,
    String? minPrice,
    String? maxPrice,
  }) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => ProductListScreen(
          searchKey: searchKey,
          categoryId: categoryId,
          subCatId: subCatId,
          childCatId: childCatId,
          type: type,
          sorting: sorting,
          brandId: brandId,
          minPrice: minPrice,
          maxPrice: maxPrice,
        ),
      ),
    );
  }

  static openProductDetailsScreen(BuildContext context,
      {required int productId}) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => DetailsScreen(
          productId: productId,
        ),
      ),
    );
  }
  static openProductDetailsReplacement(BuildContext context,
      {required int productId}) {
    return Navigator.pushReplacement(
      context,
      CupertinoPageRoute(
        builder: (context) => DetailsScreen(
          productId: productId,
        ),
      ),
    );
  }
  static openCheckoutScreen(
    BuildContext context,
      double value
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) =>  CheckoutScreen(charge: value ),
      ),
    );
  }

  static openCheckoutAddressScreen(BuildContext context,{
    required List<CartModel> cart,
    required int subtotal,
    required int totalQty,
    required double currencySubtotal,
  }){
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) =>  CheckoutAddressScreen(cart: cart, totalQty: totalQty, subtotal: subtotal, currencySubtotal: currencySubtotal),
      ),
    );
  }

  static openCheckoutCompleteScreen(
    BuildContext context, {
      required int orderId,
      required String orderNumber,
      }) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) =>  CheckoutCompleteScreen(orderId: orderId, orderNumber: orderNumber,),
      ),
    );
  }

  static openOrderListScreen(
    BuildContext context,
  ) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const OrderScreen(),
      ),
    );
  }

  static openOrderDetailsScreen(BuildContext context, {required int orderId}) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => OrderDetailsScreen(
          orderId: orderId,
        ),
      ),
    );
  }
}
