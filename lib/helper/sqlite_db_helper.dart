import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import '../constants/strings.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initiateDatabase();
    return _database;
  }

  Future<Database> _initiateDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, dbName);
    var database = await openDatabase(path, version: dbVersion,
        onCreate: (Database db, int version) async {
      
      await db.execute('''
            CREATE TABLE $tableNameCart(
            ${CartColumn.columnId} INTEGER,
            ${CartColumn.columnQty} INTEGER,
            ${CartColumn.columnPrice} INTEGER,
            ${CartColumn.columnPrevPrice} INTEGER,
            ${CartColumn.columnStock} INTEGER,
            ${CartColumn.columnSizeKeyIndex} INTEGER,
            ${CartColumn.columnCurrencyPrice} TEXT,
            ${CartColumn.columnCurrencyPrevPrice} TEXT,
            ${CartColumn.columnCurrencyShippingFee} TEXT,
            ${CartColumn.columnCurrencyPackingFee} TEXT,
            ${CartColumn.columnCurrencySizePrice} TEXT,
            ${CartColumn.columnSize} TEXT,
            ${CartColumn.columnColor} TEXT,
            ${CartColumn.columnPhoto} TEXT,
            ${CartColumn.columnProducts} TEXT,
            ${CartColumn.columnTitle} TEXT
            
            )
            
        ''');

    });
    return database;
  }
}

class CartColumn {
  static const columnId = 'id';
  static const columnQty = 'qty';
  static const columnPrice = 'price';
  static const columnPrevPrice = 'prev_price';
  static const columnStock = 'stock';
  static const columnSizeKeyIndex = 'size_key_index';
  static const columnCurrencyPrice = 'currency_price';
  static const columnCurrencyPrevPrice = 'currency_prev_price';
  static const columnCurrencyShippingFee = 'currency_shipping_fee';
  static const columnCurrencyPackingFee = 'currency_packing_fee';
  static const columnCurrencySizePrice = 'currency_size_price';
  static const columnSize = 'size';
  static const columnColor = 'color';
  static const columnPhoto = 'photo';
  static const columnTitle = 'title';
  static const columnProducts = 'products';
}
