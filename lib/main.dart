import 'package:echem/bloc/auth/auth_bloc.dart';
import 'package:echem/bloc/best_selling/best_selling_bloc.dart';
import 'package:echem/bloc/best_selling/best_selling_event.dart';
import 'package:echem/bloc/big_save/big_save_event.dart';
import 'package:echem/bloc/categories/category_bloc.dart';
import 'package:echem/bloc/categories/category_event.dart' as cat;
import 'package:echem/bloc/checkout/checkout_cubit.dart';
import 'package:echem/bloc/home/home_bloc.dart';
import 'package:echem/bloc/new_arrival/new_arrival_bloc.dart';
import 'package:echem/bloc/new_arrival/new_arrival_event.dart';
import 'package:echem/bloc/videos/videos_event.dart';
import 'package:echem/constants/theme/light_theme.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bloc/big_save/big_save_bloc.dart';
import 'bloc/filter/filter_cubit.dart';
import 'bloc/home/home_event.dart';
import 'bloc/only_category/only_category_bloc.dart';
import 'bloc/only_category/only_category_event.dart';
import 'bloc/videos/videos_bloc.dart';
import 'helper/simple_bloc_observer.dart';
import 'ui/screens/splash/splash_screen.dart';

late SharedPreferences prefs;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //init get storage
  await GetStorage.init();
  //Init firebase
  await Firebase.initializeApp();
  //Init my app
  Bloc.observer = SimpleBlocObserver();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          //home screen bloc start
          BlocProvider(
            create: (_) => HomeBloc()..add(GetSliderList()),
          ),
          BlocProvider(
            create: (_) => BestSellingBloc()..add(GetBestSellingList()),
          ),
          BlocProvider(
            create: (_) => BigSaveBloc()..add(GetBigSaveList()),
          ),
          BlocProvider(
            create: (_) => NewArrivalBloc()..add(GetNewArrivalList()),
          ),
          BlocProvider(
            create: (_) => VideosBloc(postLimit: 4)..add(VideosFetched()),
          ),
          //end home screen bloc
          BlocProvider(
            create: (_) => AuthCubit()..getSharedUserInfo(),
          ),
          BlocProvider(
            create: (_) => CategoryBloc()..add(cat.GetCategoryList()),
          ),
          BlocProvider(
            create: (_) => OnlyCategoryBloc()..add(GetCategoryList()),
          ),
          BlocProvider(
            create: (_) => FilterCubit()..getProductList(false,startIndex: 0),
          ),
          BlocProvider(
            create: (_) => CheckoutCubit()..setUserData()..setCity()
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: appName,
          theme: light,
          home: const SplashScreen(),
        ));
  }
}
