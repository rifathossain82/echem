import 'dart:convert';
CartItem cartItemFromJson(String str) => CartItem.fromJson(json.decode(str));
String cartItemToJson(CartItem data) => json.encode(data.toJson());
class CartItem {
  CartItem({
      int? qty, 
      int? sizeKey, 
      String? sizeQty, 
      String? sizePrice, 
      String? size, 
      String? color, 
      int? stock, 
      double? price, 
      Item? item, 
      String? license, 
      String? dp, 
      String? keys, 
      String? values, 
      double? itemPrice, 
      num? weight,}){
    _qty = qty;
    _sizeKey = sizeKey;
    _sizeQty = sizeQty;
    _sizePrice = sizePrice;
    _size = size;
    _color = color;
    _stock = stock;
    _price = price;
    _item = item;
    _license = license;
    _dp = dp;
    _keys = keys;
    _values = values;
    _itemPrice = itemPrice;
    _weight = weight;
}

  CartItem.fromJson(dynamic json) {
    _qty = json['qty'];
    _sizeKey = json['size_key'];
    _sizeQty = json['size_qty'];
    _sizePrice = json['size_price'];
    _size = json['size'];
    _color = json['color'];
    _stock = json['stock'];
    _price = json['price'];
    _item = json['item'] != null ? Item.fromJson(json['item']) : null;
    _license = json['license'];
    _dp = json['dp'];
    _keys = json['keys'];
    _values = json['values'];
    _itemPrice = json['item_price'];
    _weight = json['weight'];
  }
  int? _qty;
  int? _sizeKey;
  String? _sizeQty;
  String? _sizePrice;
  String? _size;
  String? _color;
  int? _stock;
  double? _price;
  Item? _item;
  String? _license;
  String? _dp;
  String? _keys;
  String? _values;
  double? _itemPrice;
  num? _weight;

  int? get qty => _qty;
  int? get sizeKey => _sizeKey;
  String? get sizeQty => _sizeQty;
  String? get sizePrice => _sizePrice;
  String? get size => _size;
  String? get color => _color;
  int? get stock => _stock;
  double? get price => _price;
  Item? get item => _item;
  String? get license => _license;
  String? get dp => _dp;
  String? get keys => _keys;
  String? get values => _values;
  double? get itemPrice => _itemPrice;
  num? get weight => _weight;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['qty'] = _qty;
    map['size_key'] = _sizeKey;
    map['size_qty'] = _sizeQty;
    map['size_price'] = _sizePrice;
    map['size'] = _size;
    map['color'] = _color;
    map['stock'] = _stock;
    map['price'] = _price;
    if (_item != null) {
      map['item'] = _item?.toJson();
    }
    map['license'] = _license;
    map['dp'] = _dp;
    map['keys'] = _keys;
    map['values'] = _values;
    map['item_price'] = _itemPrice;
    map['weight'] = _weight;
    return map;
  }

}

Item itemFromJson(String str) => Item.fromJson(json.decode(str));
String itemToJson(Item data) => json.encode(data.toJson());
class Item {
  Item({
      int? id, 
      int? userId, 
      String? slug, 
      String? name, 
      String? photo, 
      String? size, 
      num? weight,
      String? sizeQty, 
      String? sizePrice, 
      String? color, 
      double? price, 
      int? cashback, 
      int? stock, 
      String? type, 
      dynamic file, 
      dynamic link, 
      String? license, 
      String? licenseQty, 
      dynamic measure, 
      String? wholeSellQty, 
      String? wholeSellDiscount, 
      dynamic attributes,}){
    _id = id;
    _userId = userId;
    _slug = slug;
    _name = name;
    _photo = photo;
    _size = size;
    _weight = weight;
    _sizeQty = sizeQty;
    _sizePrice = sizePrice;
    _color = color;
    _price = price;
    _cashback = cashback;
    _stock = stock;
    _type = type;
    _file = file;
    _link = link;
    _license = license;
    _licenseQty = licenseQty;
    _measure = measure;
    _wholeSellQty = wholeSellQty;
    _wholeSellDiscount = wholeSellDiscount;
    _attributes = attributes;
}

  Item.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _slug = json['slug'];
    _name = json['name'];
    _photo = json['photo'];
    _size = json['size'];
    _weight = json['weight'];
    _sizeQty = json['size_qty'];
    _sizePrice = json['size_price'];
    _color = json['color'];
    _price = json['price'];
    _cashback = json['cashback'];
    _stock = json['stock'];
    _type = json['type'];
    _file = json['file'];
    _link = json['link'];
    _license = json['license'];
    _licenseQty = json['license_qty'];
    _measure = json['measure'];
    _wholeSellQty = json['whole_sell_qty'];
    _wholeSellDiscount = json['whole_sell_discount'];
    _attributes = json['attributes'];
  }
  int? _id;
  int? _userId;
  String? _slug;
  String? _name;
  String? _photo;
  String? _size;
  num? _weight;
  String? _sizeQty;
  String? _sizePrice;
  String? _color;
  double? _price;
  int? _cashback;
  int? _stock;
  String? _type;
  dynamic _file;
  dynamic _link;
  String? _license;
  String? _licenseQty;
  dynamic _measure;
  String? _wholeSellQty;
  String? _wholeSellDiscount;
  dynamic _attributes;

  int? get id => _id;
  int? get userId => _userId;
  String? get slug => _slug;
  String? get name => _name;
  String? get photo => _photo;
  String? get size => _size;
  num? get weight => _weight;
  String? get sizeQty => _sizeQty;
  String? get sizePrice => _sizePrice;
  String? get color => _color;
  double? get price => _price;
  int? get cashback => _cashback;
  int? get stock => _stock;
  String? get type => _type;
  dynamic get file => _file;
  dynamic get link => _link;
  String? get license => _license;
  String? get licenseQty => _licenseQty;
  dynamic get measure => _measure;
  String? get wholeSellQty => _wholeSellQty;
  String? get wholeSellDiscount => _wholeSellDiscount;
  dynamic get attributes => _attributes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['slug'] = _slug;
    map['name'] = _name;
    map['photo'] = _photo;
    map['size'] = _size;
    map['weight'] = _weight;
    map['size_qty'] = _sizeQty;
    map['size_price'] = _sizePrice;
    map['color'] = _color;
    map['price'] = _price;
    map['cashback'] = _cashback;
    map['stock'] = _stock;
    map['type'] = _type;
    map['file'] = _file;
    map['link'] = _link;
    map['license'] = _license;
    map['license_qty'] = _licenseQty;
    map['measure'] = _measure;
    map['whole_sell_qty'] = _wholeSellQty;
    map['whole_sell_discount'] = _wholeSellDiscount;
    map['attributes'] = _attributes;
    return map;
  }

}