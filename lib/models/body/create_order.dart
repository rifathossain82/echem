import 'dart:convert';
CreateOrder createOrderFromJson(String str) => CreateOrder.fromJson(json.decode(str));
String createOrderToJson(CreateOrder data) => json.encode(data.toJson());
class CreateOrder {
  CreateOrder({
      double? subTotal,
      int? discountAmount, 
      int? tax, 
      String? couponId, 
      String? dp, 
      double? deliveryCharge,
      int? packingCost, 
      int? vendorShippingId, 
      int? vendorPackingId, 
      String? tranId, 
      String? note, 
      String? paymentType, 
      Shipping? shipping, 
      Cart? cart,}){
    _subTotal = subTotal;
    _discountAmount = discountAmount;
    _tax = tax;
    _couponId = couponId;
    _dp = dp;
    _deliveryCharge = deliveryCharge;
    _packingCost = packingCost;
    _vendorShippingId = vendorShippingId;
    _vendorPackingId = vendorPackingId;
    _tranId = tranId;
    _note = note;
    _paymentType = paymentType;
    _shipping = shipping;
    _cart = cart;
}

  CreateOrder.fromJson(dynamic json) {
    _subTotal = json['sub_total'];
    _discountAmount = json['discount_amount'];
    _tax = json['tax'];
    _couponId = json['coupon_id'];
    _dp = json['dp'];
    _deliveryCharge = json['delivery_charge'];
    _packingCost = json['packing_cost'];
    _vendorShippingId = json['vendor_shipping_id'];
    _vendorPackingId = json['vendor_packing_id'];
    _tranId = json['tran_id'];
    _note = json['note'];
    _paymentType = json['payment_type'];
    _shipping = json['shipping'] != null ? Shipping.fromJson(json['shipping']) : null;
    _cart = json['cart'] != null ? Cart.fromJson(json['cart']) : null;
  }
  double? _subTotal;
  int? _discountAmount;
  int? _tax;
  String? _couponId;
  String? _dp;
  double? _deliveryCharge;
  int? _packingCost;
  int? _vendorShippingId;
  int? _vendorPackingId;
  String? _tranId;
  String? _note;
  String? _paymentType;
  Shipping? _shipping;
  Cart? _cart;

  double? get subTotal => _subTotal;
  int? get discountAmount => _discountAmount;
  int? get tax => _tax;
  String? get couponId => _couponId;
  String? get dp => _dp;
  double? get deliveryCharge => _deliveryCharge;
  int? get packingCost => _packingCost;
  int? get vendorShippingId => _vendorShippingId;
  int? get vendorPackingId => _vendorPackingId;
  String? get tranId => _tranId;
  String? get note => _note;
  String? get paymentType => _paymentType;
  Shipping? get shipping => _shipping;
  Cart? get cart => _cart;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['sub_total'] = _subTotal;
    map['discount_amount'] = _discountAmount;
    map['tax'] = _tax;
    map['coupon_id'] = _couponId;
    map['dp'] = _dp;
    map['delivery_charge'] = _deliveryCharge;
    map['packing_cost'] = _packingCost;
    map['vendor_shipping_id'] = _vendorShippingId;
    map['vendor_packing_id'] = _vendorPackingId;
    map['tran_id'] = _tranId;
    map['note'] = _note;
    map['payment_type'] = _paymentType;
    if (_shipping != null) {
      map['shipping'] = _shipping?.toJson();
    }
    if (_cart != null) {
      map['cart'] = _cart?.toJson();
    }
    return map;
  }

}

Cart cartFromJson(String str) => Cart.fromJson(json.decode(str));
String cartToJson(Cart data) => json.encode(data.toJson());
class Cart {
  Cart({
      int? totalQty, 
      double? totalPrice,
      Map? items,}){
    _totalQty = totalQty;
    _totalPrice = totalPrice;
    _items = items;
}

  Cart.fromJson(dynamic json) {
    _totalQty = json['totalQty'];
    _totalPrice = json['totalPrice'];
    _items = json['items'];
  }
  int? _totalQty;
  double? _totalPrice;
  Map? _items;

  int? get totalQty => _totalQty;
  double? get totalPrice => _totalPrice;
  Map? get items => _items;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalQty'] = _totalQty;
    map['totalPrice'] = _totalPrice;
    map['items'] = _items;
    return map;
  }

}

Shipping shippingFromJson(String str) => Shipping.fromJson(json.decode(str));
String shippingToJson(Shipping data) => json.encode(data.toJson());
class Shipping {
  Shipping({
      String? shippingType, 
      String? phone, 
      String? name, 
      String? email, 
      String? address, 
      int? pickupLocation, 
      String? customerUpazila, 
      String? customerCity, 
      String? postCode,}){
    _shippingType = shippingType;
    _phone = phone;
    _name = name;
    _email = email;
    _address = address;
    _pickupLocation = pickupLocation;
    _customerUpazila = customerUpazila;
    _customerCity = customerCity;
    _postCode = postCode;
}

  Shipping.fromJson(dynamic json) {
    _shippingType = json['shipping_type'];
    _phone = json['phone'];
    _name = json['name'];
    _email = json['email'];
    _address = json['address'];
    _pickupLocation = json['pickup_location'];
    _customerUpazila = json['customer_upazila'];
    _customerCity = json['customer_city'];
    _postCode = json['post_code'];
  }
  String? _shippingType;
  String? _phone;
  String? _name;
  String? _email;
  String? _address;
  int? _pickupLocation;
  String? _customerUpazila;
  String? _customerCity;
  String? _postCode;

  String? get shippingType => _shippingType;
  String? get phone => _phone;
  String? get name => _name;
  String? get email => _email;
  String? get address => _address;
  int? get pickupLocation => _pickupLocation;
  String? get customerUpazila => _customerUpazila;
  String? get customerCity => _customerCity;
  String? get postCode => _postCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shipping_type'] = _shippingType;
    map['phone'] = _phone;
    map['name'] = _name;
    map['email'] = _email;
    map['address'] = _address;
    map['pickup_location'] = _pickupLocation;
    map['customer_upazila'] = _customerUpazila;
    map['customer_city'] = _customerCity;
    map['post_code'] = _postCode;
    return map;
  }

}