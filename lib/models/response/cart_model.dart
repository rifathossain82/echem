import 'dart:convert';
CartModel cartModelFromJson(String str) => CartModel.fromJson(json.decode(str));
String cartModelToJson(CartModel data) => json.encode(data.toJson());
class CartModel {
  CartModel({
      int? id, 
      int? qty, 
      int? price, 
      int? prevPrice, 
      int? stock, 
      int? sizeKeyIndex, 
      String? currencyPrice, 
      String? currencyPrevPrice, 
      String? currencyShippingFee, 
      String? currencyPackingFee, 
      String? currencySizePrice, 
      String? size, 
      String? color, 
      String? photo, 
      String? title, 
      String? products,}){
    _id = id;
    _qty = qty;
    _price = price;
    _prevPrice = prevPrice;
    _stock = stock;
    _sizeKeyIndex = sizeKeyIndex;
    _currencyPrice = currencyPrice;
    _currencyPrevPrice = currencyPrevPrice;
    _currencyShippingFee = currencyShippingFee;
    _currencyPackingFee = currencyPackingFee;
    _currencySizePrice = currencySizePrice;
    _size = size;
    _color = color;
    _photo = photo;
    _title = title;
    _products = products;
}

  CartModel.fromJson(dynamic json) {
    _id = json['id'];
    _qty = json['qty'];
    _price = json['price'];
    _prevPrice = json['prev_price'];
    _stock = json['stock'];
    _sizeKeyIndex = json['size_key_index'];
    _currencyPrice = json['currency_price'];
    _currencyPrevPrice = json['currency_prev_price'];
    _currencyShippingFee = json['currency_shipping_fee'];
    _currencyPackingFee = json['currency_packing_fee'];
    _currencySizePrice = json['currency_size_price'];
    _size = json['size'];
    _color = json['color'];
    _photo = json['photo'];
    _title = json['title'];
    _products = json['products'];
  }
  int? _id;
  int? _qty;
  int? _price;
  int? _prevPrice;
  int? _stock;
  int? _sizeKeyIndex;
  String? _currencyPrice;
  String? _currencyPrevPrice;
  String? _currencyShippingFee;
  String? _currencyPackingFee;
  String? _currencySizePrice;
  String? _size;
  String? _color;
  String? _photo;
  String? _title;
  String? _products;

  int? get id => _id;
  int? get qty => _qty;
  int? get price => _price;
  int? get prevPrice => _prevPrice;
  int? get stock => _stock;
  int? get sizeKeyIndex => _sizeKeyIndex;
  String? get currencyPrice => _currencyPrice;
  String? get currencyPrevPrice => _currencyPrevPrice;
  String? get currencyShippingFee => _currencyShippingFee;
  String? get currencyPackingFee => _currencyPackingFee;
  String? get currencySizePrice => _currencySizePrice;
  String? get size => _size;
  String? get color => _color;
  String? get photo => _photo;
  String? get title => _title;
  String? get products => _products;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['qty'] = _qty;
    map['price'] = _price;
    map['prev_price'] = _prevPrice;
    map['stock'] = _stock;
    map['size_key_index'] = _sizeKeyIndex;
    map['currency_price'] = _currencyPrice;
    map['currency_prev_price'] = _currencyPrevPrice;
    map['currency_shipping_fee'] = _currencyShippingFee;
    map['currency_packing_fee'] = _currencyPackingFee;
    map['currency_size_price'] = _currencySizePrice;
    map['size'] = _size;
    map['color'] = _color;
    map['photo'] = _photo;
    map['title'] = _title;
    map['products'] = _products;
    return map;
  }

}