import 'dart:convert';

import 'rp_categories.dart';

CategoryList dataFromJson(String str) => CategoryList.fromJson(json.decode(str));
String dataToJson(CategoryList data) => json.encode(data.toJson());
class CategoryList {
  CategoryList({
    int? id,
    String? name,
    String? slug,
    int? status,
    String? photo,
    int? isFeatured,
    int? isTopcategory,
    String? image,
    int? productsCount,
    List<Subs>? subs,}){
    _id = id;
    _name = name;
    _slug = slug;
    _status = status;
    _photo = photo;
    _isFeatured = isFeatured;
    _isTopcategory = isTopcategory;
    _image = image;
    _productsCount = productsCount;
    _subs = subs;
  }

  CategoryList.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _slug = json['slug'];
    _status = json['status'];
    _photo = json['photo'];
    _isFeatured = json['is_featured'];
    _isTopcategory = json['is_topcategory'];
    _image = json['image'];
    _productsCount = json['products_count'];
    if (json['subs'] != null) {
      _subs = [];
      json['subs'].forEach((v) {
        _subs?.add(Subs.fromJson(v));
      });
    }
  }
  int? _id;
  String? _name;
  String? _slug;
  int? _status;
  String? _photo;
  int? _isFeatured;
  int? _isTopcategory;
  String? _image;
  int? _productsCount;
  List<Subs>? _subs;

  int? get id => _id;
  String? get name => _name;
  String? get slug => _slug;
  int? get status => _status;
  String? get photo => _photo;
  int? get isFeatured => _isFeatured;
  int? get isTopcategory => _isTopcategory;
  String? get image => _image;
  int? get productsCount => _productsCount;
  List<Subs>? get subs => _subs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['slug'] = _slug;
    map['status'] = _status;
    map['photo'] = _photo;
    map['is_featured'] = _isFeatured;
    map['is_topcategory'] = _isTopcategory;
    map['image'] = _image;
    map['products_count'] = _productsCount;
    if (_subs != null) {
      map['subs'] = _subs?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}