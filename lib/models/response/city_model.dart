class CityModel {
  List<Countries>? countries;
  List<Upazilas>? upazilas;
  String? message;
  String? errors;

  CityModel({this.countries, this.upazilas, this.message});

  CityModel.withError(String errorMessage) {
    errors = errorMessage;
  }

  CityModel.fromJson(Map<String, dynamic> json) {
    if (json['countries'] != null) {
      countries = <Countries>[];
      json['countries'].forEach((v) {
        countries!.add(new Countries.fromJson(v));
      });
    }
    if (json['upazilas'] != null) {
      upazilas = <Upazilas>[];
      json['upazilas'].forEach((v) {
        upazilas!.add(new Upazilas.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.countries != null) {
      data['countries'] = this.countries!.map((v) => v.toJson()).toList();
    }
    if (this.upazilas != null) {
      data['upazilas'] = this.upazilas!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class Countries {
  String? zela;

  Countries({this.zela});

  Countries.fromJson(Map<String, dynamic> json) {
    zela = json['zela'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['zela'] = this.zela;
    return data;
  }
}

class Upazilas {
  String? upazila;

  Upazilas({this.upazila});

  Upazilas.fromJson(Map<String, dynamic> json) {
    upazila = json['upazila'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['upazila'] = this.upazila;
    return data;
  }
}