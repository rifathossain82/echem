import 'dart:convert';
ProductList productListFromJson(String str) => ProductList.fromJson(json.decode(str));
String productListToJson(ProductList data) => json.encode(data.toJson());
class ProductList {
  ProductList({
      int? id, 
      String? name, 
      String? photo, 
      int? price, 
      int? previousPrice, 
      int? isDiscount, 
      int? ratingsCount, 
      String? averageRating,}){
    _id = id;
    _name = name;
    _photo = photo;
    _price = price;
    _previousPrice = previousPrice;
    _isDiscount = isDiscount;
    _ratingsCount = ratingsCount;
    _averageRating = averageRating;
}

  ProductList.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _photo = json['photo'];
    _price = json['price'];
    _previousPrice = json['previous_price'];
    _isDiscount = json['is_discount'];
    _ratingsCount = json['ratings_count'];
    _averageRating = json['average_rating'];
  }
  int? _id;
  String? _name;
  String? _photo;
  int? _price;
  int? _previousPrice;
  int? _isDiscount;
  int? _ratingsCount;
  String? _averageRating;

  int? get id => _id;
  String? get name => _name;
  String? get photo => _photo;
  int? get price => _price;
  int? get previousPrice => _previousPrice;
  int? get isDiscount => _isDiscount;
  int? get ratingsCount => _ratingsCount;
  String? get averageRating => _averageRating;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['photo'] = _photo;
    map['price'] = _price;
    map['previous_price'] = _previousPrice;
    map['is_discount'] = _isDiscount;
    map['ratings_count'] = _ratingsCount;
    map['average_rating'] = _averageRating;
    return map;
  }

}