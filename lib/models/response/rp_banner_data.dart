import 'dart:convert';
RpBannerData rpBannerDataFromJson(String str) => RpBannerData.fromJson(json.decode(str));
String rpBannerDataToJson(RpBannerData data) => json.encode(data.toJson());
class RpBannerData {
  RpBannerData({
      String? message, 
      String? errors, 
      List<BannerList>? data,
      int? status,}){
    _message = message;
    _errors = errors;
    _data = data;
    _status = status;
}
  RpBannerData.withError(String errorMessage) {
    _errors = errorMessage;
  }
  RpBannerData.fromJson(dynamic json) {
    _message = json['message'];
    _errors = json['errors'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(BannerList.fromJson(v));
      });
    }
    _status = json['status'];
  }
  String? _message;
  String? _errors;
  List<BannerList>? _data;
  int? _status;

  String? get message => _message;
  String? get errors => _errors;
  List<BannerList>? get data => _data;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['errors'] = _errors;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['status'] = _status;
    return map;
  }

}

BannerList dataFromJson(String str) => BannerList.fromJson(json.decode(str));
String dataToJson(BannerList data) => json.encode(data.toJson());
class BannerList {
  BannerList({
      int? id, 
      String? photo, 
      String? link, 
      String? type,}){
    _id = id;
    _photo = photo;
    _link = link;
    _type = type;
}

  BannerList.fromJson(dynamic json) {
    _id = json['id'];
    _photo = json['photo'];
    _link = json['link'];
    _type = json['type'];
  }
  int? _id;
  String? _photo;
  String? _link;
  String? _type;

  int? get id => _id;
  String? get photo => _photo;
  String? get link => _link;
  String? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['photo'] = _photo;
    map['link'] = _link;
    map['type'] = _type;
    return map;
  }

}