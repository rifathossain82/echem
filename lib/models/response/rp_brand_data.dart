import 'dart:convert';
RpBrandData rpBrandDatFromJson(String str) => RpBrandData.fromJson(json.decode(str));
String rpBrandDatToJson(RpBrandData data) => json.encode(data.toJson());
class RpBrandData {
  RpBrandData({
      String? message, 
      String? errors, 
      List<BrandList>? data,
      int? status,}){
    _message = message;
    _errors = errors;
    _data = data;
    _status = status;
}
  RpBrandData.withError(String errorMessage) {
    _errors = errorMessage;
  }
  RpBrandData.fromJson(dynamic json) {
    _message = json['message'];
    _errors = json['errors'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(BrandList.fromJson(v));
      });
    }
    _status = json['status'];
  }
  String? _message;
  String? _errors;
  List<BrandList>? _data;
  int? _status;

  String? get message => _message;
  String? get errors => _errors;
  List<BrandList>? get data => _data;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['errors'] = _errors;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['status'] = _status;
    return map;
  }

}

BrandList dataFromJson(String str) => BrandList.fromJson(json.decode(str));
String dataToJson(BrandList data) => json.encode(data.toJson());
class BrandList {
  BrandList({
      int? id, 
      dynamic productId, 
      String? photo, 
      String? name,}){
    _id = id;
    _productId = productId;
    _photo = photo;
    _name = name;
}

  BrandList.fromJson(dynamic json) {
    _id = json['id'];
    _productId = json['product_id'];
    _photo = json['photo'];
    _name = json['name'];
  }
  int? _id;
  dynamic _productId;
  String? _photo;
  String? _name;

  int? get id => _id;
  dynamic get productId => _productId;
  String? get photo => _photo;
  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['product_id'] = _productId;
    map['photo'] = _photo;
    map['name'] = _name;
    return map;
  }

}