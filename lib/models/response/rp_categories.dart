import 'dart:convert';

import 'category_list.dart';
RpCategories rpCategoriesFromJson(String str) => RpCategories.fromJson(json.decode(str));
String rpCategoriesToJson(RpCategories data) => json.encode(data.toJson());
class RpCategories {
  RpCategories({
      List<CategoryList>? data,
      String? message, 
      String? errors,}){
    _data = data;
    _message = message;
    _errors = errors;
}

  RpCategories.withError(String errorMessage) {
    _errors = errorMessage;
  }

  RpCategories.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CategoryList.fromJson(v));
      });
    }
    _message = json['message'];
    _errors = json['errors'];
  }
  List<CategoryList>? _data;
  String? _message;
  String? _errors;

  List<CategoryList>? get data => _data;
  String? get message => _message;
  String? get errors => _errors;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['message'] = _message;
    map['errors'] = _errors;
    return map;
  }

}

Subs subsFromJson(String str) => Subs.fromJson(json.decode(str));
String subsToJson(Subs data) => json.encode(data.toJson());
class Subs {
  Subs({
      int? id, 
      int? categoryId, 
      String? name, 
      String? slug, 
      int? status, 
      List<Childs>? childs,}){
    _id = id;
    _categoryId = categoryId;
    _name = name;
    _slug = slug;
    _status = status;
    _childs = childs;
}

  Subs.fromJson(dynamic json) {
    _id = json['id'];
    _categoryId = json['category_id'];
    _name = json['name'];
    _slug = json['slug'];
    _status = json['status'];
    if (json['childs'] != null) {
      _childs = [];
      json['childs'].forEach((v) {
        _childs?.add(Childs.fromJson(v));
      });
    }
  }
  int? _id;
  int? _categoryId;
  String? _name;
  String? _slug;
  int? _status;
  List<Childs>? _childs;

  int? get id => _id;
  int? get categoryId => _categoryId;
  String? get name => _name;
  String? get slug => _slug;
  int? get status => _status;
  List<Childs>? get childs => _childs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['category_id'] = _categoryId;
    map['name'] = _name;
    map['slug'] = _slug;
    map['status'] = _status;
    if (_childs != null) {
      map['childs'] = _childs?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

Childs childsFromJson(String str) => Childs.fromJson(json.decode(str));
String childsToJson(Childs data) => json.encode(data.toJson());
class Childs {
  Childs({
      int? id, 
      int? subcategoryId, 
      String? name, 
      String? slug, 
      int? status,}){
    _id = id;
    _subcategoryId = subcategoryId;
    _name = name;
    _slug = slug;
    _status = status;
}

  Childs.fromJson(dynamic json) {
    _id = json['id'];
    _subcategoryId = json['subcategory_id'];
    _name = json['name'];
    _slug = json['slug'];
    _status = json['status'];
  }
  int? _id;
  int? _subcategoryId;
  String? _name;
  String? _slug;
  int? _status;

  int? get id => _id;
  int? get subcategoryId => _subcategoryId;
  String? get name => _name;
  String? get slug => _slug;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['subcategory_id'] = _subcategoryId;
    map['name'] = _name;
    map['slug'] = _slug;
    map['status'] = _status;
    return map;
  }

}