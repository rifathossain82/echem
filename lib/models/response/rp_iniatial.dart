import 'dart:convert';
RpIniatial rpIniatialFromJson(String str) => RpIniatial.fromJson(json.decode(str));
String rpIniatialToJson(RpIniatial data) => json.encode(data.toJson());
class RpIniatial {
  RpIniatial({
      Currency? currency, 
      String? message, 
      String? errors, 
      int? status,}){
    _currency = currency;
    _message = message;
    _errors = errors;
    _status = status;
}
  RpIniatial.withError(String errorMessage) {
    _errors = errorMessage;
  }
  RpIniatial.fromJson(dynamic json) {
    _currency = json['currency'] != null ? Currency.fromJson(json['currency']) : null;
    _message = json['message'];
    _errors = json['errors'];
    _status = json['status'];
  }
  Currency? _currency;
  String? _message;
  String? _errors;
  int? _status;

  Currency? get currency => _currency;
  String? get message => _message;
  String? get errors => _errors;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_currency != null) {
      map['currency'] = _currency?.toJson();
    }
    map['message'] = _message;
    map['errors'] = _errors;
    map['status'] = _status;
    return map;
  }

}

Currency currencyFromJson(String str) => Currency.fromJson(json.decode(str));
String currencyToJson(Currency data) => json.encode(data.toJson());
class Currency {
  Currency({
      int? id, 
      String? name, 
      String? sign,
      String? value,
      int? isDefault,}){
    _id = id;
    _name = name;
    _sign = sign;
    _value = value;
    _isDefault = isDefault;
}

  Currency.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _sign = json['sign'];
    _value = json['value'] == null ? '0.0' : json['value'].toString();
    _isDefault = json['is_default'];
  }
  int? _id;
  String? _name;
  String? _sign;
  String? _value;
  int? _isDefault;

  int? get id => _id;
  String? get name => _name;
  String? get sign => _sign;
  String? get value => _value;
  int? get isDefault => _isDefault;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['sign'] = _sign;
    map['value'] = _value;
    map['is_default'] = _isDefault;
    return map;
  }

}