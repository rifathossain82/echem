import 'dart:convert';

RpLoginData rpLoginDataFromJson(String str) =>
    RpLoginData.fromJson(json.decode(str));

String rpLoginDataToJson(RpLoginData data) => json.encode(data.toJson());

class RpLoginData {
  RpLoginData({
    User? user,
    String? message,
    String? errors,
    String? token,
    int? status,
  }) {
    _user = user;
    _message = message;
    _errors = errors;
    _token = token;
    _status = status;
  }

  RpLoginData.withError(String errorMessage) {
    _errors = errorMessage;
  }

  RpLoginData.fromJson(dynamic json) {
    _user = json['user'] != null ? User.fromJson(json['user']) : null;
    _message = json['message'];
    _errors = json['errors'];
    _token = json['token'];
    _status = json['status'];
  }

  User? _user;
  String? _message;
  String? _errors;
  String? _token;
  int? _status;

  User? get user => _user;

  String? get message => _message;

  String? get errors => _errors;

  String? get token => _token;

  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_user != null) {
      map['user'] = _user?.toJson();
    }
    map['message'] = _message;
    map['errors'] = _errors;
    map['token'] = _token;
    map['status'] = _status;
    return map;
  }
}

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    int? id,
    String? name,
    dynamic facebookId,
    dynamic googleId,
    String? photo,
    String? zip,
    String? city,
    String? country,
    String? address,
    String? phone,
    dynamic fax,
    String? email,
    String? createdAt,
    String? updatedAt,
    int? isProvider,
    int? status,
    String? verificationLink,
    String? emailVerified,
    String? affilateCode,
    int? affilateIncome,
    int? cashback,
    int? reward,
    int? totalIncome,
    dynamic shopName,
    dynamic ownerName,
    dynamic shopNumber,
    dynamic shopAddress,
    dynamic regNumber,
    dynamic shopMessage,
    dynamic shopDetails,
    dynamic shopImage,
    dynamic fUrl,
    dynamic gUrl,
    dynamic tUrl,
    dynamic lUrl,
    int? isVendor,
    int? fCheck,
    int? gCheck,
    int? tCheck,
    int? lCheck,
    int? mailSent,
    int? shippingCost,
    int? currentBalance,
    dynamic date,
    int? ban,
    String? otp,
    String? error,
  }) {
    _id = id;
    _name = name;
    _facebookId = facebookId;
    _googleId = googleId;
    _photo = photo;
    _zip = zip;
    _city = city;
    _country = country;
    _address = address;
    _phone = phone;
    _fax = fax;
    _email = email;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _isProvider = isProvider;
    _status = status;
    _verificationLink = verificationLink;
    _emailVerified = emailVerified;
    _affilateCode = affilateCode;
    _affilateIncome = affilateIncome;
    _cashback = cashback;
    _reward = reward;
    _totalIncome = totalIncome;
    _shopName = shopName;
    _ownerName = ownerName;
    _shopNumber = shopNumber;
    _shopAddress = shopAddress;
    _regNumber = regNumber;
    _shopMessage = shopMessage;
    _shopDetails = shopDetails;
    _shopImage = shopImage;
    _fUrl = fUrl;
    _gUrl = gUrl;
    _tUrl = tUrl;
    _lUrl = lUrl;
    _isVendor = isVendor;
    _fCheck = fCheck;
    _gCheck = gCheck;
    _tCheck = tCheck;
    _lCheck = lCheck;
    _mailSent = mailSent;
    _shippingCost = shippingCost;
    _currentBalance = currentBalance;
    _date = date;
    _ban = ban;
    _error = error;
  }

  User.withError(String errorMessage) {
    _error = errorMessage;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _facebookId = json['facebook_id'];
    _googleId = json['google_id'];
    _photo = json['photo'];
    _zip = json['zip'];
    _city = json['city'];
    _country = json['country'];
    _address = json['address'];
    _phone = json['phone'];
    _fax = json['fax'];
    _email = json['email'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _isProvider = json['is_provider'];
    _status = json['status'];
    _verificationLink = json['verification_link'];
    _emailVerified = json['email_verified'];
    _affilateCode = json['affilate_code'];
    _affilateIncome = json['affilate_income'];
    _cashback = json['cashback'];
    _reward = json['reward'];
    _totalIncome = json['total_income'];
    _shopName = json['shop_name'];
    _ownerName = json['owner_name'];
    _shopNumber = json['shop_number'];
    _shopAddress = json['shop_address'];
    _regNumber = json['reg_number'];
    _shopMessage = json['shop_message'];
    _shopDetails = json['shop_details'];
    _shopImage = json['shop_image'];
    _fUrl = json['f_url'];
    _gUrl = json['g_url'];
    _tUrl = json['t_url'];
    _lUrl = json['l_url'];
    _isVendor = json['is_vendor'];
    _fCheck = json['f_check'];
    _gCheck = json['g_check'];
    _tCheck = json['t_check'];
    _lCheck = json['l_check'];
    _mailSent = json['mail_sent'];
    _shippingCost = json['shipping_cost'];
    _currentBalance = json['current_balance'];
    _date = json['date'];
    _ban = json['ban'];
  }

  int? _id;
  String? _name;
  dynamic _facebookId;
  dynamic _googleId;
  String? _photo;
  String? _zip;
  String? _city;
  String? _country;
  String? _address;
  String? _phone;
  dynamic _fax;
  String? _email;
  String? _createdAt;
  String? _updatedAt;
  int? _isProvider;
  int? _status;
  String? _verificationLink;
  String? _emailVerified;
  String? _affilateCode;
  int? _affilateIncome;
  int? _cashback;
  int? _reward;
  int? _totalIncome;
  dynamic _shopName;
  dynamic _ownerName;
  dynamic _shopNumber;
  dynamic _shopAddress;
  dynamic _regNumber;
  dynamic _shopMessage;
  dynamic _shopDetails;
  dynamic _shopImage;
  dynamic _fUrl;
  dynamic _gUrl;
  dynamic _tUrl;
  dynamic _lUrl;
  int? _isVendor;
  int? _fCheck;
  int? _gCheck;
  int? _tCheck;
  int? _lCheck;
  int? _mailSent;
  int? _shippingCost;
  int? _currentBalance;
  dynamic _date;
  int? _ban;
  String? _otp;
  String? _error;

  int? get id => _id;

  String? get name => _name;

  dynamic get facebookId => _facebookId;

  dynamic get googleId => _googleId;

  String? get photo => _photo;

  String? get zip => _zip;

  String? get city => _city;

  String? get country => _country;

  String? get address => _address;

  String? get phone => _phone;

  dynamic get fax => _fax;

  String? get email => _email;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  int? get isProvider => _isProvider;

  int? get status => _status;

  String? get verificationLink => _verificationLink;

  String? get emailVerified => _emailVerified;

  String? get affilateCode => _affilateCode;

  int? get affilateIncome => _affilateIncome;

  int? get cashback => _cashback;

  int? get reward => _reward;

  int? get totalIncome => _totalIncome;

  dynamic get shopName => _shopName;

  dynamic get ownerName => _ownerName;

  dynamic get shopNumber => _shopNumber;

  dynamic get shopAddress => _shopAddress;

  dynamic get regNumber => _regNumber;

  dynamic get shopMessage => _shopMessage;

  dynamic get shopDetails => _shopDetails;

  dynamic get shopImage => _shopImage;

  dynamic get fUrl => _fUrl;

  dynamic get gUrl => _gUrl;

  dynamic get tUrl => _tUrl;

  dynamic get lUrl => _lUrl;

  int? get isVendor => _isVendor;

  int? get fCheck => _fCheck;

  int? get gCheck => _gCheck;

  int? get tCheck => _tCheck;

  int? get lCheck => _lCheck;

  int? get mailSent => _mailSent;

  int? get shippingCost => _shippingCost;

  int? get currentBalance => _currentBalance;

  dynamic get date => _date;

  int? get ban => _ban;

  String? get otp => _otp;

  String? get error => _error;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['facebook_id'] = _facebookId;
    map['google_id'] = _googleId;
    map['photo'] = _photo;
    map['zip'] = _zip;
    map['city'] = _city;
    map['country'] = _country;
    map['address'] = _address;
    map['phone'] = _phone;
    map['fax'] = _fax;
    map['email'] = _email;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['is_provider'] = _isProvider;
    map['status'] = _status;
    map['verification_link'] = _verificationLink;
    map['email_verified'] = _emailVerified;
    map['affilate_code'] = _affilateCode;
    map['affilate_income'] = _affilateIncome;
    map['cashback'] = _cashback;
    map['reward'] = _reward;
    map['total_income'] = _totalIncome;
    map['shop_name'] = _shopName;
    map['owner_name'] = _ownerName;
    map['shop_number'] = _shopNumber;
    map['shop_address'] = _shopAddress;
    map['reg_number'] = _regNumber;
    map['shop_message'] = _shopMessage;
    map['shop_details'] = _shopDetails;
    map['shop_image'] = _shopImage;
    map['f_url'] = _fUrl;
    map['g_url'] = _gUrl;
    map['t_url'] = _tUrl;
    map['l_url'] = _lUrl;
    map['is_vendor'] = _isVendor;
    map['f_check'] = _fCheck;
    map['g_check'] = _gCheck;
    map['t_check'] = _tCheck;
    map['l_check'] = _lCheck;
    map['mail_sent'] = _mailSent;
    map['shipping_cost'] = _shippingCost;
    map['current_balance'] = _currentBalance;
    map['date'] = _date;
    map['ban'] = _ban;
    return map;
  }
}
