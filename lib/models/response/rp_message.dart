import 'dart:convert';
RpMessage rpMessageFromJson(String str) => RpMessage.fromJson(json.decode(str));
String rpMessageToJson(RpMessage data) => json.encode(data.toJson());
class RpMessage {
  RpMessage({
      String? errors, 
      String? message, 
      int? status,}){
    _errors = errors;
    _message = message;
    _status = status;
}

  RpMessage.withError(String errorMessage) {
    _errors = errorMessage;
  }

  RpMessage.fromJson(dynamic json) {
    _errors = json['errors'];
    _message = json['message'];
    _status = json['status'];
  }
  String? _errors;
  String? _message;
  int? _status;

  String? get errors => _errors;
  String? get message => _message;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['errors'] = _errors;
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}