import 'dart:convert';
import 'category_list.dart';

RpOnlyCategory rpOnlyCategoryFromJson(String str) =>
    RpOnlyCategory.fromJson(json.decode(str));

String rpOnlyCategoryToJson(RpOnlyCategory data) => json.encode(data.toJson());

class RpOnlyCategory {
  RpOnlyCategory({
    List<CategoryList>? data,
    String? message,
    int? status,
    String? error,}) {
    _data = data;
    _message = message;
    _status = status;
    _error = error;
  }

  RpOnlyCategory.withError(String errorMessage) {
    _error = errorMessage;
  }

  RpOnlyCategory.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CategoryList.fromJson(v));
      });
    }
    _message = json['message'];
    _status = json['status'];
  }

  List<CategoryList>? _data;
  String? _message;
  String? _error;
  int? _status;

  List<CategoryList>? get data => _data;

  String? get message => _message;
  String? get error => _error;

  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}