import 'dart:convert';
RpOrderDetails rpOrderDetailsFromJson(String str) => RpOrderDetails.fromJson(json.decode(str));
String rpOrderDetailsToJson(RpOrderDetails data) => json.encode(data.toJson());
class RpOrderDetails {
  RpOrderDetails({
      String? message, 
      String? errors, 
      Data? data, 
      int? status,}){
    _message = message;
    _errors = errors;
    _data = data;
    _status = status;
}

  RpOrderDetails.withError(String errorMessage) {
    _errors = errorMessage;
  }

  RpOrderDetails.fromJson(dynamic json) {
    _message = json['message'];
    _errors = json['errors'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  String? _message;
  String? _errors;
  Data? _data;
  int? _status;

  String? get message => _message;
  String? get errors => _errors;
  Data? get data => _data;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['errors'] = _errors;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }

}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      Order? order, 
      List<Products>? products, 
      int? totalPrice, 
      int? totalQty,}){
    _order = order;
    _products = products;
    _totalPrice = totalPrice;
    _totalQty = totalQty;
}

  Data.fromJson(dynamic json) {
    _order = json['order'] != null ? Order.fromJson(json['order']) : null;
    if (json['products'] != null) {
      _products = [];
      json['products'].forEach((v) {
        _products?.add(Products.fromJson(v));
      });
    }
    _totalPrice = json['totalPrice'];
    _totalQty = json['totalQty'];
  }
  Order? _order;
  List<Products>? _products;
  int? _totalPrice;
  int? _totalQty;

  Order? get order => _order;
  List<Products>? get products => _products;
  int? get totalPrice => _totalPrice;
  int? get totalQty => _totalQty;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_order != null) {
      map['order'] = _order?.toJson();
    }
    if (_products != null) {
      map['products'] = _products?.map((v) => v.toJson()).toList();
    }
    map['totalPrice'] = _totalPrice;
    map['totalQty'] = _totalQty;
    return map;
  }

}

Products productsFromJson(String str) => Products.fromJson(json.decode(str));
String productsToJson(Products data) => json.encode(data.toJson());
class Products {
  Products({
      int? id, 
      String? name, 
      String? photo, 
      int? quantity, 
      int? price, 
      int? totalPrice,}){
    _id = id;
    _name = name;
    _photo = photo;
    _quantity = quantity;
    _price = price;
    _totalPrice = totalPrice;
}

  Products.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _photo = json['photo'];
    _quantity = json['quantity'];
    _price = json['price'];
    _totalPrice = json['total_price'];
  }
  int? _id;
  String? _name;
  String? _photo;
  int? _quantity;
  int? _price;
  int? _totalPrice;

  int? get id => _id;
  String? get name => _name;
  String? get photo => _photo;
  int? get quantity => _quantity;
  int? get price => _price;
  int? get totalPrice => _totalPrice;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['photo'] = _photo;
    map['quantity'] = _quantity;
    map['price'] = _price;
    map['total_price'] = _totalPrice;
    return map;
  }

}

Order orderFromJson(String str) => Order.fromJson(json.decode(str));
String orderToJson(Order data) => json.encode(data.toJson());
class Order {
  Order({
      int? id, 
      int? userId, 
      String? cart, 
      String? method, 
      String? shipping, 
      String? pickupLocation, 
      String? totalQty, 
      int? payAmount, 
      dynamic txnid, 
      dynamic chargeId, 
      String? orderNumber, 
      String? paymentStatus, 
      String? customerEmail, 
      String? customerName, 
      String? customerCountry, 
      String? customerUpazila, 
      String? customerPhone, 
      String? customerAddress, 
      String? customerCity, 
      String? customerZip, 
      String? shippingName, 
      String? shippingUpazila, 
      String? shippingCountry, 
      String? shippingEmail, 
      String? shippingPhone, 
      String? shippingAddress, 
      String? shippingCity, 
      String? shippingZip, 
      String? orderNote, 
      dynamic couponCode, 
      dynamic couponDiscount, 
      String? status, 
      String? createdAt, 
      String? updatedAt, 
      dynamic affilateUser, 
      dynamic affilateCharge, 
      String? currencySign, 
      double? currencyValue, 
      int? shippingCost, 
      int? packingCost, 
      int? tax, 
      int? cashback, 
      int? dp, 
      dynamic payId, 
      int? vendorShippingId, 
      int? vendorPackingId, 
      dynamic deletedAt,}){
    _id = id;
    _userId = userId;
    _cart = cart;
    _method = method;
    _shipping = shipping;
    _pickupLocation = pickupLocation;
    _totalQty = totalQty;
    _payAmount = payAmount;
    _txnid = txnid;
    _chargeId = chargeId;
    _orderNumber = orderNumber;
    _paymentStatus = paymentStatus;
    _customerEmail = customerEmail;
    _customerName = customerName;
    _customerCountry = customerCountry;
    _customerUpazila = customerUpazila;
    _customerPhone = customerPhone;
    _customerAddress = customerAddress;
    _customerCity = customerCity;
    _customerZip = customerZip;
    _shippingName = shippingName;
    _shippingUpazila = shippingUpazila;
    _shippingCountry = shippingCountry;
    _shippingEmail = shippingEmail;
    _shippingPhone = shippingPhone;
    _shippingAddress = shippingAddress;
    _shippingCity = shippingCity;
    _shippingZip = shippingZip;
    _orderNote = orderNote;
    _couponCode = couponCode;
    _couponDiscount = couponDiscount;
    _status = status;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _affilateUser = affilateUser;
    _affilateCharge = affilateCharge;
    _currencySign = currencySign;
    _currencyValue = currencyValue;
    _shippingCost = shippingCost;
    _packingCost = packingCost;
    _tax = tax;
    _cashback = cashback;
    _dp = dp;
    _payId = payId;
    _vendorShippingId = vendorShippingId;
    _vendorPackingId = vendorPackingId;
    _deletedAt = deletedAt;
}

  Order.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _cart = json['cart'];
    _method = json['method'];
    _shipping = json['shipping'];
    _pickupLocation = json['pickup_location'];
    _totalQty = json['totalQty'];
    _payAmount = json['pay_amount'];
    _txnid = json['txnid'];
    _chargeId = json['charge_id'];
    _orderNumber = json['order_number'];
    _paymentStatus = json['payment_status'];
    _customerEmail = json['customer_email'];
    _customerName = json['customer_name'];
    _customerCountry = json['customer_country'];
    _customerUpazila = json['customer_upazila'];
    _customerPhone = json['customer_phone'];
    _customerAddress = json['customer_address'];
    _customerCity = json['customer_city'];
    _customerZip = json['customer_zip'];
    _shippingName = json['shipping_name'];
    _shippingUpazila = json['shipping_upazila'];
    _shippingCountry = json['shipping_country'];
    _shippingEmail = json['shipping_email'];
    _shippingPhone = json['shipping_phone'];
    _shippingAddress = json['shipping_address'];
    _shippingCity = json['shipping_city'];
    _shippingZip = json['shipping_zip'];
    _orderNote = json['order_note'];
    _couponCode = json['coupon_code'];
    _couponDiscount = json['coupon_discount'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _affilateUser = json['affilate_user'];
    _affilateCharge = json['affilate_charge'];
    _currencySign = json['currency_sign'];
    _currencyValue = json['currency_value'];
    _shippingCost = json['shipping_cost'];
    _packingCost = json['packing_cost'];
    _tax = json['tax'];
    _cashback = json['cashback'];
    _dp = json['dp'];
    _payId = json['pay_id'];
    _vendorShippingId = json['vendor_shipping_id'];
    _vendorPackingId = json['vendor_packing_id'];
    _deletedAt = json['deleted_at'];
  }
  int? _id;
  int? _userId;
  String? _cart;
  String? _method;
  String? _shipping;
  String? _pickupLocation;
  String? _totalQty;
  int? _payAmount;
  dynamic _txnid;
  dynamic _chargeId;
  String? _orderNumber;
  String? _paymentStatus;
  String? _customerEmail;
  String? _customerName;
  String? _customerCountry;
  String? _customerUpazila;
  String? _customerPhone;
  String? _customerAddress;
  String? _customerCity;
  String? _customerZip;
  String? _shippingName;
  String? _shippingUpazila;
  String? _shippingCountry;
  String? _shippingEmail;
  String? _shippingPhone;
  String? _shippingAddress;
  String? _shippingCity;
  String? _shippingZip;
  String? _orderNote;
  dynamic _couponCode;
  dynamic _couponDiscount;
  String? _status;
  String? _createdAt;
  String? _updatedAt;
  dynamic _affilateUser;
  dynamic _affilateCharge;
  String? _currencySign;
  double? _currencyValue;
  int? _shippingCost;
  int? _packingCost;
  int? _tax;
  int? _cashback;
  int? _dp;
  dynamic _payId;
  int? _vendorShippingId;
  int? _vendorPackingId;
  dynamic _deletedAt;

  int? get id => _id;
  int? get userId => _userId;
  String? get cart => _cart;
  String? get method => _method;
  String? get shipping => _shipping;
  String? get pickupLocation => _pickupLocation;
  String? get totalQty => _totalQty;
  int? get payAmount => _payAmount;
  dynamic get txnid => _txnid;
  dynamic get chargeId => _chargeId;
  String? get orderNumber => _orderNumber;
  String? get paymentStatus => _paymentStatus;
  String? get customerEmail => _customerEmail;
  String? get customerName => _customerName;
  String? get customerCountry => _customerCountry;
  String? get customerUpazila => _customerUpazila;
  String? get customerPhone => _customerPhone;
  String? get customerAddress => _customerAddress;
  String? get customerCity => _customerCity;
  String? get customerZip => _customerZip;
  String? get shippingName => _shippingName;
  String? get shippingUpazila => _shippingUpazila;
  String? get shippingCountry => _shippingCountry;
  String? get shippingEmail => _shippingEmail;
  String? get shippingPhone => _shippingPhone;
  String? get shippingAddress => _shippingAddress;
  String? get shippingCity => _shippingCity;
  String? get shippingZip => _shippingZip;
  String? get orderNote => _orderNote;
  dynamic get couponCode => _couponCode;
  dynamic get couponDiscount => _couponDiscount;
  String? get status => _status;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  dynamic get affilateUser => _affilateUser;
  dynamic get affilateCharge => _affilateCharge;
  String? get currencySign => _currencySign;
  double? get currencyValue => _currencyValue;
  int? get shippingCost => _shippingCost;
  int? get packingCost => _packingCost;
  int? get tax => _tax;
  int? get cashback => _cashback;
  int? get dp => _dp;
  dynamic get payId => _payId;
  int? get vendorShippingId => _vendorShippingId;
  int? get vendorPackingId => _vendorPackingId;
  dynamic get deletedAt => _deletedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['cart'] = _cart;
    map['method'] = _method;
    map['shipping'] = _shipping;
    map['pickup_location'] = _pickupLocation;
    map['totalQty'] = _totalQty;
    map['pay_amount'] = _payAmount;
    map['txnid'] = _txnid;
    map['charge_id'] = _chargeId;
    map['order_number'] = _orderNumber;
    map['payment_status'] = _paymentStatus;
    map['customer_email'] = _customerEmail;
    map['customer_name'] = _customerName;
    map['customer_country'] = _customerCountry;
    map['customer_upazila'] = _customerUpazila;
    map['customer_phone'] = _customerPhone;
    map['customer_address'] = _customerAddress;
    map['customer_city'] = _customerCity;
    map['customer_zip'] = _customerZip;
    map['shipping_name'] = _shippingName;
    map['shipping_upazila'] = _shippingUpazila;
    map['shipping_country'] = _shippingCountry;
    map['shipping_email'] = _shippingEmail;
    map['shipping_phone'] = _shippingPhone;
    map['shipping_address'] = _shippingAddress;
    map['shipping_city'] = _shippingCity;
    map['shipping_zip'] = _shippingZip;
    map['order_note'] = _orderNote;
    map['coupon_code'] = _couponCode;
    map['coupon_discount'] = _couponDiscount;
    map['status'] = _status;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['affilate_user'] = _affilateUser;
    map['affilate_charge'] = _affilateCharge;
    map['currency_sign'] = _currencySign;
    map['currency_value'] = _currencyValue;
    map['shipping_cost'] = _shippingCost;
    map['packing_cost'] = _packingCost;
    map['tax'] = _tax;
    map['cashback'] = _cashback;
    map['dp'] = _dp;
    map['pay_id'] = _payId;
    map['vendor_shipping_id'] = _vendorShippingId;
    map['vendor_packing_id'] = _vendorPackingId;
    map['deleted_at'] = _deletedAt;
    return map;
  }

}