import 'dart:convert';
RpOrderList rpOrderListFromJson(String str) => RpOrderList.fromJson(json.decode(str));
String rpOrderListToJson(RpOrderList data) => json.encode(data.toJson());
class RpOrderList {
  RpOrderList({
      String? message, 
      String? errors, 
      List<OrderList>? data,
      int? status,}){
    _message = message;
    _errors = errors;
    _data = data;
    _status = status;
}

  RpOrderList.fromJson(dynamic json) {
    _message = json['message'];
    _errors = json['errors'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(OrderList.fromJson(v));
      });
    }
    _status = json['status'];
  }
  String? _message;
  String? _errors;
  List<OrderList>? _data;
  int? _status;

  String? get message => _message;
  String? get errors => _errors;
  List<OrderList>? get data => _data;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['errors'] = _errors;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['status'] = _status;
    return map;
  }

}

OrderList dataFromJson(String str) => OrderList.fromJson(json.decode(str));
String dataToJson(OrderList data) => json.encode(data.toJson());
class OrderList {
  OrderList({
      int? id, 
      String? totalQty, 
      int? payAmount, 
      String? createdAt, 
      String? orderNumber, 
      String? status, 
      String? paymentStatus, 
      String? method,}){
    _id = id;
    _totalQty = totalQty;
    _payAmount = payAmount;
    _createdAt = createdAt;
    _orderNumber = orderNumber;
    _status = status;
    _paymentStatus = paymentStatus;
    _method = method;
}

  OrderList.fromJson(dynamic json) {
    _id = json['id'];
    _totalQty = json['totalQty'];
    _payAmount = json['pay_amount'];
    _createdAt = json['created_at'];
    _orderNumber = json['order_number'];
    _status = json['status'];
    _paymentStatus = json['payment_status'];
    _method = json['method'];
  }
  int? _id;
  String? _totalQty;
  int? _payAmount;
  String? _createdAt;
  String? _orderNumber;
  String? _status;
  String? _paymentStatus;
  String? _method;

  int? get id => _id;
  String? get totalQty => _totalQty;
  int? get payAmount => _payAmount;
  String? get createdAt => _createdAt;
  String? get orderNumber => _orderNumber;
  String? get status => _status;
  String? get paymentStatus => _paymentStatus;
  String? get method => _method;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['totalQty'] = _totalQty;
    map['pay_amount'] = _payAmount;
    map['created_at'] = _createdAt;
    map['order_number'] = _orderNumber;
    map['status'] = _status;
    map['payment_status'] = _paymentStatus;
    map['method'] = _method;
    return map;
  }

}