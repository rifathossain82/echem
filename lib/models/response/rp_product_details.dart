import 'dart:convert';
RpProductDetails rpProductDetailsFromJson(String str) => RpProductDetails.fromJson(json.decode(str));
String rpProductDetailsToJson(RpProductDetails data) => json.encode(data.toJson());
class RpProductDetails {
  RpProductDetails({
      ProductDetails? data,
      String? errors, 
      String? message, 
      int? status,}){
    _data = data;
    _errors = errors;
    _message = message;
    _status = status;
}
  RpProductDetails.withError(String errorMessage) {
    _errors = errorMessage;
  }
  RpProductDetails.fromJson(dynamic json) {
    _data = json['data'] != null ? ProductDetails.fromJson(json['data']) : null;
    _errors = json['errors'];
    _message = json['message'];
    _status = json['status'];
  }
  ProductDetails? _data;
  String? _errors;
  String? _message;
  int? _status;

  ProductDetails? get data => _data;
  String? get errors => _errors;
  String? get message => _message;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['errors'] = _errors;
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}

ProductDetails dataFromJson(String str) => ProductDetails.fromJson(json.decode(str));
String dataToJson(ProductDetails data) => json.encode(data.toJson());
class ProductDetails {
  ProductDetails({
      int? id, 
      String? sku, 
      num? weight,
      String? productType, 
      int? userId, 
      int? categoryId, 
      int? subcategoryId, 
      dynamic attributes, 
      String? name, 
      String? slug, 
      String? photo, 
      dynamic file, 
      List<String>? size, 
      List<String>? sizeQty, 
      List<String>? sizePrice, 
      List<String>? color,
      String? price,
      String? previousPrice,
      int? cashback, 
      String? details, 
      int? stock, 
      String? policy,
      List<String>? colors,
      int? productCondition, 
      String? ship, 
      String? type, 
      String? license, 
      String? licenseQty, 
      dynamic link, 
      dynamic platform, 
      dynamic measure, 
      List<String>? wholeSellQty, 
      List<String>? wholeSellDiscount, 
      int? isCatalog, 
      int? catalogId, 
      dynamic deletedAt, 
      dynamic averageRating, 
      List<Galleries>? galleries,}){
    _id = id;
    _sku = sku;
    _weight = weight;
    _productType = productType;
    _userId = userId;
    _categoryId = categoryId;
    _subcategoryId = subcategoryId;
    _attributes = attributes;
    _name = name;
    _slug = slug;
    _photo = photo;
    _file = file;
    _size = size;
    _sizeQty = sizeQty;
    _sizePrice = sizePrice;
    _color = color;
    _price = price;
    _previousPrice = previousPrice;
    _cashback = cashback;
    _details = details;
    _stock = stock;
    _policy = policy;
    _colors = colors;
    _productCondition = productCondition;
    _ship = ship;
    _type = type;
    _license = license;
    _licenseQty = licenseQty;
    _link = link;
    _platform = platform;
    _measure = measure;
    _wholeSellQty = wholeSellQty;
    _wholeSellDiscount = wholeSellDiscount;
    _isCatalog = isCatalog;
    _catalogId = catalogId;
    _deletedAt = deletedAt;
    _averageRating = averageRating;
    _galleries = galleries;
}

  ProductDetails.fromJson(dynamic json) {
    _id = json['id'];
    _sku = json['sku'];
    _weight = json['weight'];
    _productType = json['product_type'];
    _userId = json['user_id'];
    _categoryId = json['category_id'];
    _subcategoryId = json['subcategory_id'];
    _attributes = json['attributes'];
    _name = json['name'];
    _slug = json['slug'];
    _photo = json['photo'];
    _file = json['file'];
    _size = json['size'] != "" ? json['size'].cast<String>() : [];
    _sizeQty = json['size_qty'] != "" ? json['size_qty'].cast<String>() : [];
    _sizePrice = json['size_price'] != "" ? json['size_price'].cast<String>() : [];
    _color = json['color'] != "" ? json['color'].cast<String>() : [];
    _price = json['price'] == null ? '0.0' : json['price'].toString();
    _previousPrice = json['previous_price'] == null ? '0.0' : json['previous_price'].toString();
    _cashback = json['cashback'];
    _details = json['details'];
    _stock = json['stock'];
    _policy = json['policy'];
    _colors = json['colors'] != "" ? json['colors'].cast<String>() : [];
    _productCondition = json['product_condition'];
    _ship = json['ship'];
    _type = json['type'];
    _license = json['license'];
    _licenseQty = json['license_qty'];
    _link = json['link'];
    _platform = json['platform'];
    _measure = json['measure'];
    _wholeSellQty = json['whole_sell_qty'] != "" ? json['whole_sell_qty'].cast<String>() : [];
    _wholeSellDiscount = json['whole_sell_discount'] != "" ? json['whole_sell_discount'].cast<String>() : [];
    _isCatalog = json['is_catalog'];
    _catalogId = json['catalog_id'];
    _deletedAt = json['deleted_at'];
    _averageRating = json['average_rating'];
    if (json['galleries'] != null) {
      _galleries = [];
      json['galleries'].forEach((v) {
        _galleries?.add(Galleries.fromJson(v));
      });
    }
  }
  int? _id;
  String? _sku;
  num? _weight;
  String? _productType;
  int? _userId;
  int? _categoryId;
  int? _subcategoryId;
  dynamic _attributes;
  String? _name;
  String? _slug;
  String? _photo;
  dynamic _file;
  List<String>? _size;
  List<String>? _sizeQty;
  List<String>? _sizePrice;
  List<String>? _color;
  String? _price;
  String? _previousPrice;
  int? _cashback;
  String? _details;
  int? _stock;
  String? _policy;
  List<String>? _colors;
  int? _productCondition;
  String? _ship;
  String? _type;
  String? _license;
  String? _licenseQty;
  dynamic _link;
  dynamic _platform;
  dynamic _measure;
  List<String>? _wholeSellQty;
  List<String>? _wholeSellDiscount;
  int? _isCatalog;
  int? _catalogId;
  dynamic _deletedAt;
  dynamic _averageRating;
  List<Galleries>? _galleries;

  int? get id => _id;
  String? get sku => _sku;
  num? get weight => _weight;
  String? get productType => _productType;
  int? get userId => _userId;
  int? get categoryId => _categoryId;
  int? get subcategoryId => _subcategoryId;
  dynamic get attributes => _attributes;
  String? get name => _name;
  String? get slug => _slug;
  String? get photo => _photo;
  dynamic get file => _file;
  List<String>? get size => _size;
  List<String>? get sizeQty => _sizeQty;
  List<String>? get sizePrice => _sizePrice;
  List<String>? get color => _color;
  String? get price => _price;
  String? get previousPrice => _previousPrice;
  int? get cashback => _cashback;
  String? get details => _details;
  int? get stock => _stock;
  String? get policy => _policy;
  List<String>? get colors => _colors;
  int? get productCondition => _productCondition;
  String? get ship => _ship;
  String? get type => _type;
  String? get license => _license;
  String? get licenseQty => _licenseQty;
  dynamic get link => _link;
  dynamic get platform => _platform;
  dynamic get measure => _measure;
  List<String>? get wholeSellQty => _wholeSellQty;
  List<String>? get wholeSellDiscount => _wholeSellDiscount;
  int? get isCatalog => _isCatalog;
  int? get catalogId => _catalogId;
  dynamic get deletedAt => _deletedAt;
  dynamic get averageRating => _averageRating;
  List<Galleries>? get galleries => _galleries;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['sku'] = _sku;
    map['weight'] = _weight;
    map['product_type'] = _productType;
    map['user_id'] = _userId;
    map['category_id'] = _categoryId;
    map['subcategory_id'] = _subcategoryId;
    map['attributes'] = _attributes;
    map['name'] = _name;
    map['slug'] = _slug;
    map['photo'] = _photo;
    map['file'] = _file;
    map['size'] = _size;
    map['size_qty'] = _sizeQty;
    map['size_price'] = _sizePrice;
    map['color'] = _color;
    map['price'] = _price;
    map['previous_price'] = _previousPrice;
    map['cashback'] = _cashback;
    map['details'] = _details;
    map['stock'] = _stock;
    map['policy'] = _policy;
    map['colors'] = _colors;
    map['product_condition'] = _productCondition;
    map['ship'] = _ship;
    map['type'] = _type;
    map['license'] = _license;
    map['license_qty'] = _licenseQty;
    map['link'] = _link;
    map['platform'] = _platform;
    map['measure'] = _measure;
    map['whole_sell_qty'] = _wholeSellQty;
    map['whole_sell_discount'] = _wholeSellDiscount;
    map['is_catalog'] = _isCatalog;
    map['catalog_id'] = _catalogId;
    map['deleted_at'] = _deletedAt;
    map['average_rating'] = _averageRating;
    if (_galleries != null) {
      map['galleries'] = _galleries?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

Galleries galleriesFromJson(String str) => Galleries.fromJson(json.decode(str));
String galleriesToJson(Galleries data) => json.encode(data.toJson());
class Galleries {
  Galleries({
      int? id, 
      int? productId, 
      String? photo,}){
    _id = id;
    _productId = productId;
    _photo = photo;
}

  Galleries.fromJson(dynamic json) {
    _id = json['id'];
    _productId = json['product_id'];
    _photo = json['photo'];
  }
  int? _id;
  int? _productId;
  String? _photo;

  int? get id => _id;
  int? get productId => _productId;
  String? get photo => _photo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['product_id'] = _productId;
    map['photo'] = _photo;
    return map;
  }

}