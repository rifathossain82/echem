import 'dart:convert';

import 'package:echem/models/response/product_list.dart';
RpProducts rpProductFromJson(String str) => RpProducts.fromJson(json.decode(str));
String rpProductToJson(RpProducts data) => json.encode(data.toJson());
class RpProducts {
  RpProducts({
      List<ProductList>? data,
      Currency? currency, 
      String? message, 
      int? status,}){
    _data = data;
    _currency = currency;
    _message = message;
    _status = status;
}

  RpProducts.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(ProductList.fromJson(v));
      });
    }
    _currency = json['currency'] != null ? Currency.fromJson(json['currency']) : null;
    _message = json['message'];
    _status = json['status'];
  }
  List<ProductList>? _data;
  Currency? _currency;
  String? _message;
  int? _status;

  List<ProductList>? get data => _data;
  Currency? get currency => _currency;
  String? get message => _message;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    if (_currency != null) {
      map['currency'] = _currency?.toJson();
    }
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}

Currency currencyFromJson(String str) => Currency.fromJson(json.decode(str));
String currencyToJson(Currency data) => json.encode(data.toJson());
class Currency {
  Currency({
      int? id, 
      String? name, 
      String? sign, 
      double? value, 
      int? isDefault,}){
    _id = id;
    _name = name;
    _sign = sign;
    _value = value;
    _isDefault = isDefault;
}

  Currency.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _sign = json['sign'];
    _value = json['value'];
    _isDefault = json['is_default'];
  }
  int? _id;
  String? _name;
  String? _sign;
  double? _value;
  int? _isDefault;

  int? get id => _id;
  String? get name => _name;
  String? get sign => _sign;
  double? get value => _value;
  int? get isDefault => _isDefault;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['sign'] = _sign;
    map['value'] = _value;
    map['is_default'] = _isDefault;
    return map;
  }

}
