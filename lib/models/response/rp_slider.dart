import 'dart:convert';
RpSlider rpSliderFromJson(String str) => RpSlider.fromJson(json.decode(str));
String rpSliderToJson(RpSlider data) => json.encode(data.toJson());
class RpSlider {
  RpSlider({
      List<SliderList>? data,
      String? message, 
      String? errors, 
      int? status,}){
    _data = data;
    _message = message;
    _errors = errors;
    _status = status;
}
  RpSlider.withError(String errorMessage) {
    _errors = errorMessage;
  }
  RpSlider.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(SliderList.fromJson(v));
      });
    }
    _message = json['message'];
    _errors = json['errors'];
    _status = json['status'];
  }
  List<SliderList>? _data;
  String? _message;
  String? _errors;
  int? _status;

  List<SliderList>? get data => _data;
  String? get message => _message;
  String? get errors => _errors;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['message'] = _message;
    map['errors'] = _errors;
    map['status'] = _status;
    return map;
  }

}

SliderList dataFromJson(String str) => SliderList.fromJson(json.decode(str));
String dataToJson(SliderList data) => json.encode(data.toJson());
class SliderList {
  SliderList({
      int? id, 
      String? link, 
      String? photo,}){
    _id = id;
    _link = link;
    _photo = photo;
}

  SliderList.fromJson(dynamic json) {
    _id = json['id'];
    _link = json['link'];
    _photo = json['photo'];
  }
  int? _id;
  String? _link;
  String? _photo;

  int? get id => _id;
  String? get link => _link;
  String? get photo => _photo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['link'] = _link;
    map['photo'] = _photo;
    return map;
  }

}