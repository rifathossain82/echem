import 'dart:convert';
RpVideoData rpVideoDataFromJson(String str) => RpVideoData.fromJson(json.decode(str));
String rpVideoDataToJson(RpVideoData data) => json.encode(data.toJson());
class RpVideoData {
  RpVideoData({
      String? message, 
      String? errors, 
      List<VideoList>? data,
      int? status,}){
    _message = message;
    _errors = errors;
    _data = data;
    _status = status;
}

  RpVideoData.fromJson(dynamic json) {
    _message = json['message'];
    _errors = json['errors'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(VideoList.fromJson(v));
      });
    }
    _status = json['status'];
  }
  String? _message;
  String? _errors;
  List<VideoList>? _data;
  int? _status;

  String? get message => _message;
  String? get errors => _errors;
  List<VideoList>? get data => _data;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['errors'] = _errors;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['status'] = _status;
    return map;
  }

}

VideoList videoListFromJson(String str) => VideoList.fromJson(json.decode(str));
String videoListToJson(VideoList data) => json.encode(data.toJson());
class VideoList {
  VideoList({
    int? id,
    String? link,
    String? title,
    String? authorName,
    String? authorUrl,
    String? type,
    int? height,
    int? width,
    String? version,
    String? providerName,
    String? providerUrl,
    int? thumbnailHeight,
    int? thumbnailWidth,
    String? thumbnailUrl,
    String? html,}){
    _id = id;
    _link = link;
    _title = title;
    _authorName = authorName;
    _authorUrl = authorUrl;
    _type = type;
    _height = height;
    _width = width;
    _version = version;
    _providerName = providerName;
    _providerUrl = providerUrl;
    _thumbnailHeight = thumbnailHeight;
    _thumbnailWidth = thumbnailWidth;
    _thumbnailUrl = thumbnailUrl;
    _html = html;
  }

  VideoList.fromJson(dynamic json) {
    _id = json['id'];
    _link = json['link'];
    _title = json['title'];
    _authorName = json['author_name'];
    _authorUrl = json['author_url'];
    _type = json['type'];
    _height = json['height'];
    _width = json['width'];
    _version = json['version'];
    _providerName = json['provider_name'];
    _providerUrl = json['provider_url'];
    _thumbnailHeight = json['thumbnail_height'];
    _thumbnailWidth = json['thumbnail_width'];
    _thumbnailUrl = json['thumbnail_url'];
    _html = json['html'];
  }
  int? _id;
  String? _link;
  String? _title;
  String? _authorName;
  String? _authorUrl;
  String? _type;
  int? _height;
  int? _width;
  String? _version;
  String? _providerName;
  String? _providerUrl;
  int? _thumbnailHeight;
  int? _thumbnailWidth;
  String? _thumbnailUrl;
  String? _html;

  int? get id => _id;
  String? get link => _link;
  String? get title => _title;
  String? get authorName => _authorName;
  String? get authorUrl => _authorUrl;
  String? get type => _type;
  int? get height => _height;
  int? get width => _width;
  String? get version => _version;
  String? get providerName => _providerName;
  String? get providerUrl => _providerUrl;
  int? get thumbnailHeight => _thumbnailHeight;
  int? get thumbnailWidth => _thumbnailWidth;
  String? get thumbnailUrl => _thumbnailUrl;
  String? get html => _html;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['link'] = _link;
    map['title'] = _title;
    map['author_name'] = _authorName;
    map['author_url'] = _authorUrl;
    map['type'] = _type;
    map['height'] = _height;
    map['width'] = _width;
    map['version'] = _version;
    map['provider_name'] = _providerName;
    map['provider_url'] = _providerUrl;
    map['thumbnail_height'] = _thumbnailHeight;
    map['thumbnail_width'] = _thumbnailWidth;
    map['thumbnail_url'] = _thumbnailUrl;
    map['html'] = _html;
    return map;
  }

}