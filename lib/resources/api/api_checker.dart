import 'dart:convert';
import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../constants/strings.dart';

class ApiChecker {
  static void checkApi(http.Response response, BuildContext context) {
    if (response.statusCode == 401) {
      // Get.find<AuthController>().clearSharedData();
      //  Get.to(() => LogInScreen(),);
      final responseJson = jsonDecode(response.body);
      if (responseJson['message'] != null) {
        showCustomSnackBar(context,responseJson['message']);
      } else {
        showCustomSnackBar(context,responseJson['errors']);
      }
    } else if (response.statusCode == 500) {
      showCustomSnackBar(context,kServerCode500Message);
    } else {
      final responseJson = jsonDecode(response.body);
      if (responseJson['message'] != null) {
        showCustomSnackBar(context, responseJson['message']);
      } else {
        showCustomSnackBar(context, responseJson['errors']);
      }
    }
  }

  static String checkApiMsg(http.Response response) {
    if (response.statusCode == 401) {
      final responseJson = jsonDecode(response.body);
      if (responseJson['message'] != null) {
        return responseJson['message'];
      } else {
        return responseJson['errors'];
      }
    }else{
      if (response.statusCode == 500) {
        return kServerCode500Message;
      } else {
        final responseJson = jsonDecode(response.body);
        if (responseJson['message'] != null) {
          return responseJson['message'];
        } else {
          return responseJson['errors'];
        }
      }
    }
}
}
