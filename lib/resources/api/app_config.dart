var thisYear = DateTime.now().year.toString();

const String appName = "Echem"; //this shows in the splash screen
String copyrightText = "@ $appName " + thisYear; //this shows in the splash screen

//configure this
const bool https = true;

//configure this
const domainPath = "xyz.com.bd"; // directly inside the public folder

//do not configure these below
const String apiEndPath = "api";
const String publicFolder = "public";
const String protocol = https ? "https://" : "http://";
const String rawBaseUrl = "$protocol$domainPath";
const String baseUrl = "$rawBaseUrl/$apiEndPath";
const String basePath = rawBaseUrl;
//image path
const String imagePath = "xyz";
const String kProductImagePath = "$imagePath/products/";
const String kGalleryImagePath = "$imagePath/galleries/";
const String kCategoryImagePath = "$imagePath/categories/";
const String kSliderImagePath = "$imagePath/sliders/";
const String kBannerImagePath = "$imagePath/banners/";
const String kBrandImagePath = "$imagePath/partner/";


//configure this if you are using amazon s3 like services
//give direct link to file like https://[[bucketname]].s3.ap-southeast-1.amazonaws.com/
//otherwise do not change anythink

//End point
//data end point
const String kSliderListUri = '/sliders';
const String kBrandListUri = '/partners';
const String kVideosListUri = '/our-videos';
const String kBottomBannerUri = '/bottom-banners';
const String kCategoryListUri = '/apicatagory';
const String kOnlyCategoryListUri = '/only-category-with-count';
const String kProductListUri = '/product-list';
const String kProductDetailsUri = '/product-details';
const String kAddressListUri = '/country';
const String kInitialUri = '/iniatial';
//order end point
const String kOrderCreateUri = '/order';
const String kOrderListUri = '/order-list';
const String kOrderDetailsUri = '/order-show';
const String kOrderCancelUri = '/order-cancel';
//auth end point
const String kUserLoginUri = '/login';
const String kUserRegistrationUri = '/register';
const String kSignupVerificationUri = '/otp-verification';
const String kForgotRequestUri = '/forgot-password';
const String kForgotPasswordUpdateUri = '/forgot-password-update';
const String kUserPasswordUpdateUri = '/reset-password';
const String kProfileUpdateUri = '/profile-update';

/// city endpoint
const String kCityUri = "/country";
