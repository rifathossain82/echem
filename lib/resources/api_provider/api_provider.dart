import 'dart:convert';
import 'dart:developer';
import 'package:echem/models/response/city_model.dart';
import 'package:echem/models/response/rp_iniatial.dart';
import 'package:http/http.dart' as http;
import 'package:echem/models/response/rp_categories.dart';
import 'package:echem/models/response/rp_only_category.dart';
import 'package:echem/resources/api/api_checker.dart';
import 'package:http/http.dart';
import '../../models/response/rp_brand_data.dart';
import '../../models/response/rp_video_data.dart';
import '../api/api_client.dart';
import '../api/app_config.dart';

class ApiProvider {
  final apiClient = ApiClient(appBaseUrl: baseUrl);

  //fetch init data
  Future<RpIniatial> fetchInitialData() async {
    try {
      Response response = await apiClient.getData(kInitialUri);
      if (response.statusCode == 200) {
        return RpIniatial.fromJson(jsonDecode(response.body));
      } else {
        return RpIniatial.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpIniatial.withError("Data not found / Connection issue");
    }
  }
  //fetch categories (category, subcategory and child category)
  Future<RpCategories> fetchCategoryList() async {
    try {
      Response response = await apiClient.getData(kCategoryListUri);
      if (response.statusCode == 200) {
        return RpCategories.fromJson(jsonDecode(response.body));
      } else {
        return RpCategories.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpCategories.withError("Data not found / Connection issue");
    }
  }

  //fetch only category
  Future<RpOnlyCategory> fetchOnlyCategoryList() async {
    try {
      Response response = await apiClient.getData(kOnlyCategoryListUri);
      if (response.statusCode == 200) {
        return RpOnlyCategory.fromJson(jsonDecode(response.body));
      } else {
        return RpOnlyCategory.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpOnlyCategory.withError("Data not found / Connection issue");
    }
  }
  //fetch brand list (partner)
  Future<RpBrandData> fetchBrandList(String url) async {
    try {
      Response response = await apiClient.getData(url);
      if (response.statusCode == 200) {
        return RpBrandData.fromJson(jsonDecode(response.body));
      } else {
        return RpBrandData.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpBrandData.withError("Data not found / Connection issue");
    }
  }

  //fetch all video
  Future<List<VideoList>> fetchVideoList(String url) async {
    try {
      Response response = await apiClient.getData(url);
      List<VideoList> videoList = [];
      if (response.statusCode == 200) {
        RpVideoData data = RpVideoData.fromJson(jsonDecode(response.body));

        if (data.data!.isNotEmpty) {
          for (int i = 0; i < data.data!.length; i++) {
            VideoList singleVideo =
                await fetchSingleVideo(data.data![i].link ?? "");
            videoList.add(
              VideoList(
                  id: data.data![i].id,
                  link: data.data![i].link,
                  title: singleVideo.title,
                  authorName: singleVideo.authorName,
                  authorUrl: singleVideo.authorUrl,
                  type: singleVideo.type,
                  thumbnailUrl: singleVideo.thumbnailUrl,
                  html: singleVideo.html),
            );
          }
          return videoList;
        } else {
          return [];
        }
      } else {
        return videoList;
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return [];
    }
  }

  //fetch Single Video For Youtube Api
  Future<VideoList> fetchSingleVideo(String videoId) async {
    try {
      http.Response response = await http.get(Uri.parse('https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=$videoId&format=json'),);
      log('====> Http Response: ${response.body}');
      if (response.statusCode == 200) {
        log('singe video');
        return VideoList.fromJson(jsonDecode(response.body));
      } else {
        return VideoList();
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return VideoList();
    }
  }

  /// fetch city
  Future<CityModel> fetchCity() async {
    try {
      Response response = await apiClient.getData(kCityUri);
      if (response.statusCode == 200) {
        return CityModel.fromJson(jsonDecode(response.body));
      } else {
        return CityModel.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return CityModel.withError("Data not found / Connection issue");
    }
  }
}
