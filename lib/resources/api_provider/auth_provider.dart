import 'dart:convert';
import 'dart:developer';
import 'package:echem/constants/strings.dart';
import 'package:echem/main.dart';
import 'package:echem/resources/api/api_checker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/response/rp_login_data.dart';
import '../../models/response/rp_message.dart';
import '../../ui/widgets/custom_loader.dart';
import '../api/api_client.dart';
import '../api/app_config.dart';
import 'package:http/http.dart' as http;

class AuthProvider {
  final apiClient = ApiClient(appBaseUrl: baseUrl);

  Future<RpLoginData> userLogin(BuildContext context, {
    required String userName,
    required String password,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "email_or_phone": userName,
      "password": password,
    };
    try {
      Response response = await apiClient.postData(
        kUserLoginUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpLoginData.fromJson(jsonDecode(response.body));
      } else {
        return RpLoginData.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpLoginData.withError("Data not found / Connection issue");
    }
  }

  //send otp
 /* Future<RpMessage> userRegistration(BuildContext context, {
    required String name,
    required String address,
    required String email,
    required String phone,
    required String password,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "name": name,
      "address": address,
      "email": email,
      "phone": phone,
      "password": password,
      "password_confirmation": password,
    };
    try {
      Response response = await apiClient.postData(
        kUserRegistrationUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpMessage.fromJson(jsonDecode(response.body));
      } else {
        return RpMessage.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpMessage.withError("Data not found / Connection issue");
    }
  }
*/
  //register direct
  Future<RpLoginData> userRegistration(BuildContext context, {
    required String name,
    required String address,
    required String email,
    required String phone,
    required String password,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "name": name,
      "address": address,
      "email": email,
      "phone": phone,
      "password": password,
      "password_confirmation": password,
    };
    try {
      Response response = await apiClient.postData(
        kUserRegistrationUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpLoginData.fromJson(jsonDecode(response.body));
      } else {
        return RpLoginData.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpLoginData.withError("Data not found / Connection issue");
    }
  }

  //user signup then otp verification
  Future<RpLoginData> signOtpVerification(BuildContext context, {
    required String email,
    required String otpCode,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "otp": otpCode,
      "email": email,
    };
    try {
      Response response = await apiClient.postData(
        kSignupVerificationUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpLoginData.fromJson(jsonDecode(response.body));
      } else {
        return RpLoginData.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpLoginData.withError("Data not found / Connection issue");
    }
  }

  //user forgot password request
  Future<RpMessage> forgotPasswordRequest(BuildContext context, {
    required String email,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "email": email,
    };
    try {
      Response response = await apiClient.postData(
        kForgotRequestUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpMessage.fromJson(jsonDecode(response.body));
      } else {
        return RpMessage.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpMessage.withError("Data not found / Connection issue");
    }
  }

  //user forgot password update
  Future<RpMessage> forgotPasswordUpdate(BuildContext context, {
    required String otpCode,
    required String email,
    required String password,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "otp": otpCode,
      "email": email,
      "password": password,
      "password_confirmation": password,
    };
    try {
      Response response = await apiClient.postData(
        kForgotPasswordUpdateUri,
        body: body,
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpMessage.fromJson(jsonDecode(response.body));
      } else {
        return RpMessage.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpMessage.withError("Data not found / Connection issue");
    }
  }

  //user password update
  Future<RpMessage> userPasswordUpdate(BuildContext context, {
    required String oldPassword,
    required String password,
  }) async {
    //added progress dialog
    progressDialog(context);
    final body = {
      "old_password": oldPassword,
      "password": password,
      "password_confirmation": password,
    };
    try {
      Response response = await apiClient.postData(
        kUserPasswordUpdateUri,
        body: body,
        headers: currentUserHeader()
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpMessage.fromJson(jsonDecode(response.body));
      } else {
        return RpMessage.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpMessage.withError("Data not found / Connection issue");
    }
  }

  //current user profile update
  Future<http.Response> profileInfoUpdate(BuildContext context, dynamic body) async {
    progressDialog(context);
    return await apiClient.postData(kProfileUpdateUri, body: body, headers: currentUserHeader());
  }

  Future<void> saveUserInfo(RpLoginData loginData) async {
    try {
      prefs = await SharedPreferences.getInstance();
      String user = jsonEncode(loginData.user);
      await prefs.setString(userInfo, user);
      await prefs.setString(token, loginData.token ?? '');
      await prefs.setString(userName, loginData.user!.name ?? '');
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateSharedUserInfo(RpLoginData loginData) async {
    try {
      prefs = await SharedPreferences.getInstance();
      String user = jsonEncode(loginData.user);
      await prefs.setString(userInfo, user);
      await prefs.setString(userName, loginData.user!.name ?? '');
    } catch (e) {
      rethrow;
    }
  }

  Future<User> getSharedUserInfo() async {
    prefs = await SharedPreferences.getInstance();
    Map? userMap = jsonDecode(prefs.getString(userInfo) ?? '');
    if (userMap != null) {
      return User.fromJson(userMap);
    } else {
      return User.withError("Data not found / Connection issue");
    }
  }

void progressDialog(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => const CustomLoader());
}

//Get current user header
Map<String, String> currentUserHeader() {
  Map<String, String> mainHeaders = {
    'Accept': 'application/json',
    'Cookie': 'laravel_session=Mcup3U1oxjCbdg0Jn4h6o8vMDBx4WPdttWejNlTR',
    'Authorization': 'Bearer ${prefs.getString(token) ?? ''}',
  };
  return mainHeaders;
}

Map<String, String> generalHeader() {
  Map<String, String> mainHeaders = {
    'Accept': 'application/json',
  };
  return mainHeaders;
}}
