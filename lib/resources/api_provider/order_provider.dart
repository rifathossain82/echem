import 'dart:convert';
import 'dart:developer';
import 'package:echem/models/response/rp_order_list.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../../constants/strings.dart';
import '../../main.dart';
import '../../models/body/create_order.dart';
import '../../models/response/rp_message.dart';
import '../../models/response/rp_order_details.dart';
import '../api/api_checker.dart';
import '../api/api_client.dart';
import '../api/app_config.dart';
import 'package:http/http.dart' as http;

class OrderProvider {
  final apiClient = ApiClient(appBaseUrl: baseUrl);

  //create order
  Future<http.Response> createOrder({required CreateOrder createOrder}) async {
    var data = json.encode(createOrder);
    return await apiClient.postData(
      kOrderCreateUri,
      body: data,
      headers: headerWithJson(),
    );
  }

  Future<List<OrderList>> fetchOrderList(String url) async {
    try {
      Response response = await apiClient.getData(url, headers: currentUserHeader());
      log('uri-- ${baseUrl + url} Response data---${response.body}');
      if (response.statusCode == 200) {
        RpOrderList data = RpOrderList.fromJson(jsonDecode(response.body));
        return data.data ?? [];
      } else {
        return [];
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return [];
    }
  }
  Future<RpOrderDetails> fetchOrderDetails(String url) async {
    try {
      Response response = await apiClient.getData(url, headers: currentUserHeader());
      if (response.statusCode == 200) {
        return RpOrderDetails.fromJson(jsonDecode(response.body));
      } else {
        return RpOrderDetails.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpOrderDetails.withError("Data not found / Connection issue");
    }
  }
  Future<RpMessage> orderCancelRequest(BuildContext context, {
    required String orderId,
  }) async {
    //added progress dialog
    BaseMethod.progressDialog(context);
    try {
      Response response = await apiClient.getData('$kOrderCancelUri/$orderId', headers: currentUserHeader());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return RpMessage.fromJson(jsonDecode(response.body));
      } else {
        return RpMessage.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpMessage.withError("Data not found / Connection issue");
    }
  }

  //Get current user header
  Map<String, String> currentUserHeader() {
    Map<String, String> mainHeaders = {
      //'Cookie': 'laravel_session=xmXFsz79Kqyx6sr4rd69G4PUYsnMatKHGqNO3hEF',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString(token) ?? ''}',
    };
    return mainHeaders;
  }

  //Get current user header with request body type json
  Map<String, String> headerWithJson() {
    Map<String, String> mainHeaders = {
      "Content-Type": "application/json; charset=utf-8",
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString(token) ?? ''}',
    };
    return mainHeaders;
  }
}
