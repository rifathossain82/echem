import 'dart:convert';
import 'dart:developer';
import 'package:echem/models/response/product_list.dart';
import 'package:echem/models/response/rp_banner_data.dart';
import 'package:echem/models/response/rp_product_details.dart';
import 'package:echem/models/response/rp_products.dart';
import 'package:echem/models/response/rp_slider.dart';
import 'package:http/http.dart';
import '../api/api_checker.dart';
import '../api/api_client.dart';
import '../api/app_config.dart';

class ProductsProvider {
  final apiClient = ApiClient(appBaseUrl: baseUrl);

  Future<List<ProductList>> fetchProductList(String url) async {
    try {
      Response response = await apiClient.getData(url);
      log('uri-- ${baseUrl + url} Response data---${response.body}');
      if (response.statusCode == 200) {
        RpProducts data = RpProducts.fromJson(jsonDecode(response.body));
        return data.data ?? [];
      } else {
        return [];
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return [];
    }
  }
  //Fetch product details from server base product id
  Future<RpProductDetails> fetchProductDetails(String url) async {
    try {
      Response response = await apiClient.getData(url);
      if(response.statusCode == 200){
        return RpProductDetails.fromJson(jsonDecode(response.body));
      }else {
        return RpProductDetails.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpProductDetails.withError("Data not found / Connection issue");
    }
  }
  Future<RpSlider> fetchSliderList() async {
    try {
      Response response = await apiClient.getData(kSliderListUri);
      if(response.statusCode == 200){
        return RpSlider.fromJson(jsonDecode(response.body));
      }else {
        return RpSlider.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpSlider.withError("Data not found / Connection issue");
    }
  }

  Future<RpBannerData> fetchBannerList() async {
    try {
      Response response = await apiClient.getData(kBottomBannerUri);
      if(response.statusCode == 200){
        return RpBannerData.fromJson(jsonDecode(response.body));
      }else {
        return RpBannerData.withError(ApiChecker.checkApiMsg(response));
      }
    } catch (error, stacktrace) {
      log("Exception occurred: $error stackTrace: $stacktrace");
      return RpBannerData.withError("Data not found / Connection issue");
    }
  }

}
