import '../../models/response/rp_brand_data.dart';
import '../../models/response/rp_categories.dart';
import '../../models/response/rp_iniatial.dart';
import '../../models/response/rp_only_category.dart';
import '../../models/response/rp_video_data.dart';
import '../api/app_config.dart';
import '../api_provider/api_provider.dart';

class ApiRepository {
  final _provider = ApiProvider();

  //fetch init data
  Future<RpIniatial> fetchInitialData() {
    return _provider.fetchInitialData();
  }

  Future<RpCategories> fetchCategoryList() {
    return _provider.fetchCategoryList();
  }

  Future<RpOnlyCategory> fetchOnlyCategoryList() {
    return _provider.fetchOnlyCategoryList();
  }

  Future<RpBrandData> fetchBrandList({
    int limit = 20,
    int skip = 0,
  }) {
    return _provider.fetchBrandList('$kBrandListUri?skip=$skip&page_size=$limit&');
  }

  Future<List<VideoList>> fetchVideoList({
    int startIndex = 0,
    int postLimit = 20,
  }) {
    var url = kVideosListUri + '?skip=$startIndex&page_size=$postLimit&';
    return _provider.fetchVideoList(url);
  }

  int getScrollIndex(int index) {
    return index;
  }
}

class NetworkError extends Error {}
