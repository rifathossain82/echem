import 'dart:developer';

import '../../constants/strings.dart';
import '../../main.dart';
import '../api/api_client.dart';

class AuthRepo {
  final ApiClient apiClient;

  AuthRepo({
    required this.apiClient,
  });

  /*//Register OTP request
  Future<Response> registerOTPRequest({String? phone}) async {
    final bodyValue = {"phone": phone ?? ""};
    return await apiClient.postData(kRegisterOTPUri, body: bodyValue);
  }

  //Register user Email userId and password
  Future<Response> emailRegister(dynamic registration) async {
    return await apiClient.postData(kRegisterUri, body: registration);
  }

  //Login user Email and userId,  password
  Future<Response> emailLogin(LoginBody body) async {
    final bodyValue = {
      "phone": body.username ?? "",
      "password": body.password ?? "",
    };
    return await apiClient.postData(
      emailLoginUri,
      body: bodyValue,
    );
  }
  //Register user Email userId and password
  Future<Response> socialConnect(dynamic body) async {
    return await apiClient.postData(kSocialLoginUri, body: body);
  }
  //Login user Email and userId,  password
  Future<Response> updateProfileInfo({
    required dynamic data,
  }) async {
    return await apiClient.postData(
      kUpdateProfileUri,
      body: data,
      headers: currentUserHeader(),
    );
  }

  //Get current user info
  Future<Response> getCurrentUserInfo(
    Map<String, String>? headers,
  ) async {
    return await apiClient.getData(currentUserProfileUri, headers: headers);
  }

  //Reset Password
  Future<Response> restPassword(String phone) async {
    final bodyValue = {"phone": phone};
    return await apiClient.postData(kResetPasswordUri, body: bodyValue);
  }

  //Reset New Password
  Future<Response> restNewPassword({
    required dynamic data,
  }) async {
    return await apiClient.postData(
      kResetNewPasswordUri,
      body: data,
    );
  }

  //change user password
  Future<Response> changePassword({
    required dynamic data,
  }) async {
    return await apiClient.postData(
      kChangePasswordUri,
      body: data,
      headers: currentUserHeader(),
    );
  }

  //Save User Info Shared
  Future<void> saveUserInfoShared(LoginResponse response) async {
    try {
      await prefs.setString(token, response.token ?? "");
      await prefs.setString(userId, response.id.toString());
      await prefs.setString(userAvatar, "");
      await prefs.setString(userName, response.name ?? "");
      await prefs.setString(userEmail, response.email ?? "");
      await prefs.setString(userPhone, response.phone ?? "");
      await prefs.setString(userAddress, response.address ?? "");
    } catch (e) {
      rethrow;
    }
  }
  //Save social login User Info Shared
  Future<void> saveSocialLoginUserInfoShared(RpSocailLogin response) async {
    try {
      await prefs.setString(token, response.token ?? "");
      await prefs.setString(userId, response.user!.id.toString());
      await prefs.setString(userName, response.user!.name ?? "");
      await prefs.setString(userEmail, response.user!.email ?? "");
      await prefs.setString(userPhone, "");
      await prefs.setString(userAddress, "");
      await prefs.setString(userAvatar, "");
    } catch (e) {
      rethrow;
    }
  }
  Future<void> updateSaveProfileInfo({
    required String name,
    required String email,
    required String phone,
    required String address,
  }) async {
    try {
      await prefs.setString(userName, name);
      await prefs.setString(userEmail, email);
      await prefs.setString(userPhone, phone);
      await prefs.setString(userAddress, address);
    } catch (e) {
      rethrow;
    }
  }

  //Save Name Shared
  Future<void> saveNameShared(Users response) async {
    try {
      await prefs.setString(
          userDisplayName, '${response.firstName} ${response.lastName}');
      await prefs.setString(firstName, response.firstName ?? "");
      await prefs.setString(lastName, response.lastName ?? "");
    } catch (e) {
      rethrow;
    }
  }

  ///Save User name Shared
  Future<void> saveUserNameShared(Users response) async {
    try {
      await prefs.setString(userName, response.username ?? "");
    } catch (e) {
      rethrow;
    }
  }

  //Save User name Shared
  Future<void> saveUserPasswordShared(String pass) async {
    try {
      await prefs.setString(userPassword, pass);
    } catch (e) {
      rethrow;
    }
  }*/
  //Save Name Shared
  Future<void> guestLogin() async {
    try {
      await prefs.setString(token, 'get token');
      await prefs.setString(userName, 'Guest');
      log('message');
    } catch (e) {
      rethrow;
    }
  }
  ///Is login
  bool isLoggedIn() {
    return prefs.containsKey(token);
  }

  ///Clear Shared data
  bool clearSharedData() {
    prefs.remove(token);
    return true;
  }

  String getUserToken() {
    return prefs.getString(token) ?? "";
  }

  ///User password
  String getUserPassword() {
    return prefs.getString(userPassword) ?? "";
  }

  ///User info get
  String getUserDisplayName() {
    return prefs.getString(userDisplayName) ?? "";
  }

  String getUserName() {
    return prefs.getString(userName) ?? "";
  }

  String getUserPhone() {
    return prefs.getString(userPhone) ?? "";
  }

  String getUserAddress() {
    return prefs.getString(userAddress) ?? "";
  }

  String getUserEmail() {
    return prefs.getString(userEmail) ?? "";
  }

  String getUserFName() {
    return prefs.getString(firstName) ?? prefs.getString(userDisplayName) ?? "";
  }

  String getUserLName() {
    return prefs.getString(lastName) ?? "";
  }

  String getUserId() {
    return prefs.getString(userId) ?? "";
  }

  String getUserAvatar() {
    return prefs.getString(userAvatar) ?? "";
  }

  ///Get current user header
  Map<String, String> currentUserHeader() {
    Map<String, String> _mainHeaders = {
      'Accept': 'application/json',
      'Cookie': 'laravel_session=Mcup3U1oxjCbdg0Jn4h6o8vMDBx4WPdttWejNlTR',
      'Authorization': 'Bearer ${getUserToken()}',
    };
    return _mainHeaders;
  }
}
