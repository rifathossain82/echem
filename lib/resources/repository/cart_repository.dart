import 'dart:convert';
import 'dart:developer';
import 'package:echem/models/response/cart_model.dart';
import 'package:sqflite/sqflite.dart';
import '../../constants/strings.dart';
import '../../helper/sqlite_db_helper.dart';

class CartRepository {
  final DatabaseHelper databaseHelper = DatabaseHelper.instance;

  //Get All all row
  Future<List<CartModel>> getAllCartList({
    String search = '',
  }) async {
    List<CartModel> dataList = [];
    Database? db = await databaseHelper.database;
    var results = await db!.query(tableNameCart);
    for (var element in results) {
      var data = CartModel.fromJson(element);
      dataList.add(data);
      log('Data : ${json.encode(data)}');
    }
    return dataList;
  }

  //Insert row
  Future insertCart(CartModel model) async {
    Database? db = await databaseHelper.database;
    var result = await db!.insert(
      tableNameCart,
      model.toJson(),
    );
    return result;
  }

  //Delete row
  Future<int> deleteCart(int id) async {
    Database? db = await databaseHelper.database;
    return await db!.delete(tableNameCart,
        where: '${CartColumn.columnId} = ?', whereArgs: [id]);
  }

  //Delete row
  Future<int> removeFullCart() async {
    Database? db = await databaseHelper.database;
    return await db!.delete(tableNameCart);
  }

  //update product item
  Future updateCart(int id, CartModel model) async {
    Database? db = await databaseHelper.database;
    return await db?.update(tableNameCart, model.toJson(),
        where: '${CartColumn.columnId} = ?', whereArgs: [id]);
  }

 //update specific qty column 
  Future updateCartQty(int id, int qty, ) async {
    Database? db = await databaseHelper.database;
    Map<String, dynamic> row = {
      CartColumn.columnQty: qty
    };
    return await db?.update(tableNameCart, row,
        where: '${CartColumn.columnId} = ?', whereArgs: [id]);
  }
}
