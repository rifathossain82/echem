import 'package:echem/models/response/rp_order_list.dart';
import 'package:echem/resources/api_provider/order_provider.dart';
import '../../models/body/create_order.dart';
import '../../models/response/rp_order_details.dart';
import '../api/app_config.dart';
import 'package:http/http.dart' as http;

class OrderRepository {
  final _provider = OrderProvider();

    Future<List<OrderList>> fetchOrderList({
      int startIndex = 0,
      int postLimit = 20,
    }) {
      var url = kOrderListUri + '?skip=$startIndex&page_size=$postLimit&';
      return _provider.fetchOrderList(url);
    }
  Future<RpOrderDetails> fetchOrderDetails({
    int ? orderId,
  }) {
    return _provider.fetchOrderDetails('$kOrderDetailsUri/$orderId');
  }

  //create order
  Future<http.Response> createOrder({required CreateOrder createOrder}) async {
    return _provider.createOrder(createOrder: createOrder);
  }
}

class NetworkError extends Error {}
