import 'package:echem/models/response/rp_banner_data.dart';
import 'package:echem/models/response/rp_product_details.dart';
import 'package:echem/models/response/rp_slider.dart';
import 'package:echem/resources/api/app_config.dart';
import '../../models/response/product_list.dart';
import '../api_provider/products_provider.dart';

class ProductsRepository {
  final _provider = ProductsProvider();
  Future<List<ProductList>> fetchProductList({
    int startIndex = 0,
    int postLimit = 20,
    String? search,
    String? filterType, // ["best", 'latest', 'featured', 'top', 'hot', 'big', '	trending']
    String? sort, //['low_high', 'high_low', 'new', 'old', 'rating']
    String? carId,
    String? subCatId,
    String? childCatId,
    String? brandId,
    String? minPrice,
    String? maxPrice,
  }) {
    var url = kProductListUri + '?skip=$startIndex&page_size=$postLimit&';
    if (filterType != null) {
      url += 'type=$filterType&';
    }
    if (carId != null) {
      url += 'category=$carId&';
    }
    if (subCatId != null) {
      url += 'sub_category=$subCatId&';
    }
    if (childCatId != null) {
      url += 'child_category=$childCatId&';
    }
    if (search != null) {
      url += 'search=$search&';
    }
    if (brandId != null) {
      url += 'partner=$brandId&';
    }
    if (sort != null) {
      url += 'sort=$sort&';
    }
    if (minPrice != null && maxPrice != null) {
      url += 'min_price=$minPrice&max_price=$maxPrice&';
    } else {}
    return _provider.fetchProductList(url);
  }

  Future<RpProductDetails> fetchProductDetails(String productId) {
    return _provider.fetchProductDetails('$kProductDetailsUri/$productId');
  }
  Future<RpSlider> fetchSliderList() {
    return _provider.fetchSliderList();
  }
  Future<RpBannerData> fetchBannerList() {
    return _provider.fetchBannerList();
  }
}

class NetworkError extends Error {}
