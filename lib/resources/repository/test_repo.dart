import 'package:echem/resources/api/api_client.dart';

class TestRepo {
  final ApiClient apiClient;

  TestRepo({
    required this.apiClient,
  });
}
