import 'package:echem/bloc/auth/auth_bloc.dart';
import 'package:echem/bloc/auth/auth_state.dart';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/main.dart';
import 'package:echem/models/response/rp_login_data.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/custom_alert_dialog.dart';
import '../../widgets/custom_decoration_image.dart';
import '../../widgets/main_app_bar.dart';

class AccountScreen extends StatelessWidget {
  final bool isAccount;
  const AccountScreen({
    Key? key,
    this.isAccount = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).cardColor,
      bottomNavigationBar: isAccount ? const BottomNav() : null,
      drawer: isAccount ? const CategoryDrawer() : null,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              child: isAccount ? const MainCustomAppBar(
                backButton: false,
                isAccountScreen: true,
              ) : const SizedBox(),
            ),
            Expanded(
              child: BlocListener<AuthCubit, AuthState>(
                listener: (context, state) {},
                child: BlocBuilder<AuthCubit, AuthState>(
                  builder: (context, state) {
                    AuthCubit cubit = AuthCubit.get(context);
                    return Container(
                        child: _buildBody(context, cubit.userData));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context, User model) {
    return Container(
      color: Theme.of(context).cardColor,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: CustomButton(
                      title: 'Logout',
                      height: 28,
                      width: 100,
                      textSize: 10,
                      radius: 5,
                      onPress: () {
                        CustomAlertDialog().customAlert(
                          context: context,
                          confirmTitle: 'Logout',
                          title: 'Are you sure?',
                          body: 'Do you want to Logout an app!',
                          color: kPrimaryColor,
                          onPress: () {
                            prefs.clear();
                            NavigatorHelper.goBack(context);
                            NavigatorHelper.openDashboardScreen(context);
                          },
                        );
                      },
                    ),
                  ),
                  kHeightBox30,
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                        color: Theme.of(context).disabledColor,
                        shape: BoxShape.circle,
                        image: customDecorationImage()),
                  ),
                  kHeightBox40,
                ],
              ),
            ),
            kHeightBox5,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        width: SizeConfig.screenWidth! / 3.0,
                        padding: const EdgeInsets.symmetric(
                          vertical: 15,
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topLeft: Radius.circular(10),
                          ),
                        ),
                        child: Text(
                          'Name',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      kWidthBox5,
                      Expanded(
                        child: Container(
                          width: SizeConfig.screenWidth! / 3.0,
                          padding: const EdgeInsets.symmetric(
                            vertical: 15,
                            horizontal: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            model.name ?? '',
                            style:
                                Theme.of(context).textTheme.headline6?.copyWith(
                                      color: kPrimaryColor,
                                    ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  kHeightBox5,
                  Row(
                    children: [
                      Container(
                        width: SizeConfig.screenWidth! / 3.0,
                        padding: const EdgeInsets.symmetric(
                          vertical: 15,
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topLeft: Radius.circular(10),
                          ),
                        ),
                        child: Text(
                          'Phone Number',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      kWidthBox5,
                      Expanded(
                        child: Container(
                          width: SizeConfig.screenWidth! / 3.0,
                          padding: const EdgeInsets.symmetric(
                            vertical: 15,
                            horizontal: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            model.phone ?? '',
                            style:
                                Theme.of(context).textTheme.headline6?.copyWith(
                                      color: kPrimaryColor,
                                    ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  kHeightBox5,
                  Row(
                    children: [
                      Container(
                        width: SizeConfig.screenWidth! / 3.0,
                        padding: const EdgeInsets.symmetric(
                          vertical: 15,
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topLeft: Radius.circular(10),
                          ),
                        ),
                        child: Text(
                          'Address',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      kWidthBox5,
                      Expanded(
                        child: Container(
                          width: SizeConfig.screenWidth! / 3.0,
                          padding: const EdgeInsets.symmetric(
                            vertical: 15,
                            horizontal: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            model.address ?? '',
                            style:
                                Theme.of(context).textTheme.headline6?.copyWith(
                                      color: kPrimaryColor,
                                    ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            kHeightBox5,
            InkWell(
              onTap: () {
                //set user data in update profile screen
                context.read<AuthCubit>().setUserData(model);
                NavigatorHelper.openAccountUpdateScreen(context);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Container(
                  width: SizeConfig.screenWidth,
                  padding: const EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Update Profile Information',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              color: kPrimaryColor,
                            ),
                      ),
                      kWidthBox10,
                      const Spacer(),
                      const Icon(
                        Icons.keyboard_arrow_right_outlined,
                        color: kPrimaryColor,
                        size: 35,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            kHeightBox5,
            InkWell(
              onTap: () {
                NavigatorHelper.openOrderListScreen(context);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Container(
                  width: SizeConfig.screenWidth,
                  padding: const EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'View Order History',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              color: kPrimaryColor,
                            ),
                      ),
                      kWidthBox10,
                      const Spacer(),
                      const Icon(
                        Icons.keyboard_arrow_right_outlined,
                        color: kPrimaryColor,
                        size: 35,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            kHeightBox5,
            /*        GetBuilder<ThemeController>(builder: (themeController){
               return InkWell(
                 onTap: (){
                   themeController.toggleTheme();
                 },
                 child: Padding(
                   padding:
                   const EdgeInsets.symmetric(horizontal: 5),
                   child: Container(
                     width: SizeConfig.screenWidth,
                     padding: const EdgeInsets.symmetric(
                       vertical: 5,
                       horizontal: 10,
                     ),
                     decoration: BoxDecoration(
                       color: Theme.of(context).backgroundColor,
                       borderRadius: BorderRadius.circular(10),
                     ),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: [
                         Text(
                           'Change App Theme',
                           style: Theme.of(context)
                               .textTheme
                               .headline6
                               ?.copyWith(
                             color: kPrimaryColor,
                           ),
                         ),
                         kWidthBox10,
                         const Spacer(),
                         const Icon(
                           Icons.keyboard_arrow_right_outlined,
                           color: kPrimaryColor,
                           size: 35,
                         ),
                       ],
                     ),
                   ),
                 ),
               );
             }),*/
            kHeightBox40,
          ],
        ),
      ),
    );
  }
}
