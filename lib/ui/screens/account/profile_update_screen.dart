import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../bloc/auth/auth_bloc.dart';
import '../../../bloc/auth/auth_state.dart';
import '../../../helper/navigator_helper.dart';
import '../../../resources/api_provider/auth_provider.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/main_app_bar.dart';

class ProfileUpdateScreen extends StatelessWidget {
  const ProfileUpdateScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final oldPassController = TextEditingController();
    final passwordController = TextEditingController();
    final cfPasswordController = TextEditingController();
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {},
        child: BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            AuthCubit cubit = AuthCubit.get(context);
            return SafeArea(
              child: Column(
                children: [
                  const MainCustomAppBar(
                    backButton: false,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(
                        parent: AlwaysScrollableScrollPhysics(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            kHeightBox30,
                            Center(
                              child: Text(
                                'Update Profile',
                                style: Theme.of(context).textTheme.headline3,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            kHeightBox10,
                            InputFormWidget(
                              fieldController: cubit.nameController,
                              hintText: 'Enter your name',
                              title: 'Name',
                              titleColor: kBlackColor2,
                            ),

                            InputFormWidget(
                              fieldController: cubit.phoneController,
                              hintText: 'Enter your phone',
                              title: 'Phone',
                              titleColor: kBlackColor2,
                            ),
                            InputFormWidget(
                              fieldController: cubit.emailController,
                              hintText: 'Enter your email',
                              absorbing: true,
                              title: 'Email',
                              titleColor: kBlackColor2,
                            ),
                            InputFormWidget(
                              fieldController: cubit.addressController,
                              hintText: 'Enter your address',
                              title: 'Address',
                              titleColor: kBlackColor2,
                            ),
                            kHeightBox10,
                            CustomButton(
                              height: 45,
                              width: SizeConfig.screenWidth,
                              title: 'Save Change',
                              textColor: Colors.white,
                              onPress: () async {
                                if(cubit.nameController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter your name');
                                }else if(cubit.phoneController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter your phone');
                                }else if(cubit.emailController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter your email');
                                }else if(cubit.addressController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter your address');
                                }else{
                                  context.read<AuthCubit>().updateProfile(context);
                                }
                              },
                            ),
                            kHeightBox30,
                            Center(
                              child: Text(
                                'Change Password',
                                style: Theme.of(context).textTheme.headline3,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            kHeightBox10,
                            InputFormWidget(
                              fieldController: oldPassController,
                              hintText: 'Old Password',
                              isProtected: true,
                            ),
                            InputFormWidget(
                              fieldController: passwordController,
                              hintText: 'New Password',
                              isProtected: true,
                            ),
                            InputFormWidget(
                              fieldController: cfPasswordController,
                              hintText: 'Confirm Password',
                              isProtected: true,
                            ),
                            kHeightBox10,
                            CustomButton(
                              height: 45,
                              width: SizeConfig.screenWidth,
                              title: 'Update Password',
                              textColor: Colors.white,
                              onPress: () async {
                                if(oldPassController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter old password');
                                }else if(passwordController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter new password');
                                }else if(passwordController.text.length < 6){
                                  showCustomSnackBar(context, 'Password is short');
                                }else if(cfPasswordController.text.isEmpty){
                                  showCustomSnackBar(context, 'Please enter confirm password');
                                }else if(cfPasswordController.text.length < 6){
                                  showCustomSnackBar(context, 'Password is short');
                                }else if(cfPasswordController.text != passwordController.text){
                                  showCustomSnackBar(context, 'Password don\'t match');
                                }else{
                                  var results = await AuthProvider().userPasswordUpdate(
                                    context,
                                    oldPassword: oldPassController.text,
                                    password: passwordController.text,
                                  );
                                  if (results.errors != null) {
                                    Navigator.of(context).pop();
                                    showCustomSnackBar(context, results.errors ?? '');
                                  } else {
                                    Navigator.of(context).pop();
                                    NavigatorHelper.openDashboardScreen(context);
                                    showCustomSnackBar(context, 'Password updated successfully!', isError: false);
                                  }
                                }
                              },
                            ),
                            kHeightBox40,
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      )

    );
  }
}
