import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';
import '../../../../helper/navigator_helper.dart';
import '../../../../resources/api_provider/auth_provider.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/input_from_widget.dart';

class ForgotScreen extends StatelessWidget {
  final String email;

  const ForgotScreen({
    Key? key,
    required this.email,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final otpController = TextEditingController();
    final passwordController = TextEditingController();
    final cfPasswordController = TextEditingController();
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              kHeightBox5,
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics(),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 60,
                        ),
                        Text(
                          'Set New Password',
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                        kHeightBox5,
                        InputFormWidget(
                          fieldController: otpController,
                          hintText: 'Enter OTP',
                          keyType: TextInputType.phone,
                          prefixIcon: Icon(
                            Icons.padding_outlined,
                            size: 18,
                            color: Theme.of(context).textTheme.bodyText1!.color
                          ),
                        ),
                        InputFormWidget(
                          fieldController: passwordController,
                          hintText: 'Password',
                          prefixIcon: Icon(
                            Icons.lock_outline,
                            size: 18,
                            color: Theme.of(context).textTheme.subtitle1!.color,
                          ),
                          isProtected: true,
                        ),
                        InputFormWidget(
                          fieldController: cfPasswordController,
                          hintText: 'Confirm Password',
                          prefixIcon: Icon(
                            Icons.lock_outline,
                            size: 18,
                            color:
                            Theme.of(context).textTheme.subtitle1!.color,
                          ),
                          isProtected: true,
                        ),
                        kHeightBox10,
                        CustomButton(
                          height: 45,
                          width: SizeConfig.screenWidth,
                          btnColor: kPrimaryColor,
                          title: 'Forgot Password',
                          textColor: Colors.white,
                          onPress: () async {
                            if(otpController.text.isEmpty){
                              showCustomSnackBar(context, 'Please enter otp');
                            }else if(otpController.text.length < 6){
                              showCustomSnackBar(context, 'Please enter 6 digit otp code');
                            }else if(passwordController.text.isEmpty){
                              showCustomSnackBar(context, 'Please enter new password');
                            }else if(passwordController.text.length < 6){
                              showCustomSnackBar(context, 'Password is short');
                            }else if(cfPasswordController.text.isEmpty){
                              showCustomSnackBar(context, 'Please enter confirm password');
                            }else if(cfPasswordController.text.length < 6){
                              showCustomSnackBar(context, 'Password is short');
                            }else if(cfPasswordController.text != passwordController.text){
                              showCustomSnackBar(context, 'Password don\'t match');
                            }else{
                              var results = await AuthProvider().forgotPasswordUpdate(
                                context,
                                otpCode: otpController.text,
                                email: email, password: passwordController.text,
                              );
                              if (results.errors != null) {
                                Navigator.of(context).pop();
                                showCustomSnackBar(context, results.errors ?? '');
                              } else {
                                Navigator.of(context).pop();
                                NavigatorHelper.openLoginScreenReplace(context);
                              }
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
