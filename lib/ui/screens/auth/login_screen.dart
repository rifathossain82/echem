import 'package:echem/constants/images.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/colors_data.dart';
import '../../../constants/size_config.dart';
import '../../../resources/api_provider/auth_provider.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/main_app_bar.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userNameController = TextEditingController();
    final passwordController = TextEditingController();
    final emailController = TextEditingController();
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: true,
              iconHide: true,
              rightSideHide: true,
            ),
            kHeightBox5,
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics(),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 30,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      kHeightBox30,
                      Center(
                        child: SizedBox(
                          height: 45,
                          child: Image.asset(Images.homeLogo),
                        ),
                      ),
                      kHeightBox40,
                      Center(
                        child: Text(
                          'Sign In With Email & Password',
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      kHeightBox5,
                      InputFormWidget(
                        fieldController: userNameController,
                        hintText: 'Phone or Email',
                        prefixIcon: Icon(
                          Icons.email_outlined,
                          size: 18,
                          color:
                          Theme.of(context).textTheme.subtitle1!.color,
                        ),
                      ),
                      InputFormWidget(
                        fieldController: passwordController,
                        hintText: 'Password',
                        prefixIcon: Icon(
                          Icons.lock_outline,
                          size: 18,
                          color:
                          Theme.of(context).textTheme.subtitle1!.color,
                        ),
                        isProtected: true,
                      ),
                      kHeightBox40,
                      CustomButton(
                        height: 45,
                        width: SizeConfig.screenWidth,
                        title: 'Login',
                        textColor: Colors.white,
                        onPress: () async {
                          if (userNameController.text.isEmpty) {
                            showCustomSnackBar(context,
                                'Please enter email or phone number');
                          } else if (passwordController.text.isEmpty) {
                            showCustomSnackBar(
                                context, 'Please enter password');
                          } else {
                            var results = await AuthProvider().userLogin(
                              context,
                              password: passwordController.text,
                              userName: userNameController.text,
                            );
                            if (results.errors != null) {
                              Navigator.of(context).pop();
                              showCustomSnackBar(
                                  context, results.errors ?? '');
                            } else {
                              Navigator.of(context).pop();
                              await AuthProvider().saveUserInfo(results);
                              NavigatorHelper.openDashboardScreen(context);
                            }
                          }
                        },
                      ),
                      kHeightBox40,
                      Center(
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'New Here? ',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle2
                                    ?.copyWith(
                                  fontSize: 18,
                                ),
                              ),
                              TextSpan(
                                text: 'Create A New One',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle2
                                    ?.copyWith(
                                  color: kPrimaryColor,
                                  fontSize: 18,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () =>
                                      NavigatorHelper.openSignupScreen(
                                          context),
                              ),
                            ],
                          ),
                        ),
                      ),
                      kHeightBox40,
                      /*Row(
                        children: [
                          Expanded(
                            child: Divider(
                              color: Get.isDarkMode
                                  ? kWhiteColor
                                  : kBlackColor2,
                              height: 1.5,
                              thickness: 1.5,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            'Or',
                            style: Theme.of(context).textTheme.headline6,
                            textAlign: TextAlign.center,
                          ),
                          kWidthBox10,
                          Expanded(
                            child: Divider(
                              color: Get.isDarkMode
                                  ? kWhiteColor
                                  : kBlackColor2,
                              height: 1.5,
                              thickness: 1.5,
                            ),
                          ),
                        ],
                      ),
                      kHeightBox40,
                      CustomButton(
                        height: 45,
                        iconSpace: 15,
                        btnColor: kFbColorsLight,
                        mainAxisAlignment: MainAxisAlignment.center,
                        imageURL: Images.facebook,
                        width: SizeConfig.screenWidth,
                        title: 'Continue With Facebook',
                        textColor: Colors.white,
                        onPress: () async {},
                      ),
                      kHeightBox20,*/
                      Row(
                        children: [
                          Expanded(
                            child: Divider(
                              color: Get.isDarkMode
                                  ? kWhiteColor
                                  : kBlackColor2,
                              height: 1.5,
                              thickness: 1.5,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            'Forgot',
                            style: Theme.of(context).textTheme.headline6,
                            textAlign: TextAlign.center,
                          ),
                          kWidthBox10,
                          Expanded(
                            child: Divider(
                              color: Get.isDarkMode
                                  ? kWhiteColor
                                  : kBlackColor2,
                              height: 1.5,
                              thickness: 1.5,
                            ),
                          ),
                        ],
                      ),
                      kHeightBox40,
                      Center(
                        child: Text(
                          'Forgot your password',
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      kHeightBox5,
                      InputFormWidget(
                        fieldController: emailController,
                        hintText: 'Email Address',
                        keyType: TextInputType.emailAddress,
                        prefixIcon: Icon(
                          Icons.email_outlined,
                          size: 18,
                          color:
                          Theme.of(context).textTheme.subtitle1!.color,
                        ),
                      ),
                      kHeightBox10,
                      CustomButton(
                        height: 45,
                        width: SizeConfig.screenWidth,
                        title: 'Forgot Request',
                        textColor: Colors.white,
                        onPress: () async {
                          if (emailController.text.isEmpty) {
                            showCustomSnackBar(
                                context, 'Please enter email');
                          } else {
                            var results =
                            await AuthProvider().forgotPasswordRequest(
                              context,
                              email: emailController.text,
                            );
                            if (results.errors != null) {
                              Navigator.of(context).pop();
                              showCustomSnackBar(
                                  context, results.errors ?? '');
                            } else {
                              Navigator.of(context).pop();
                              NavigatorHelper.openForgotScreen(
                                context,
                                email: emailController.text,
                              );
                            }
                          }
                        },
                      ),
                      kHeightBox40,
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
