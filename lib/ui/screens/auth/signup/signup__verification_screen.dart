import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';
import '../../../../helper/navigator_helper.dart';
import '../../../../resources/api_provider/auth_provider.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/input_from_widget.dart';

class SignupVerificationScreen extends StatelessWidget {
  final String email;

  const SignupVerificationScreen({
    Key? key,
    required this.email,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final otpController = TextEditingController();

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              kHeightBox5,
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics(),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 60,
                        ),
                        Text(
                          'OTP',
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                        kHeightBox5,
                        InputFormWidget(
                          fieldController: otpController,
                          hintText: 'Enter OTP',
                          keyType: TextInputType.phone,
                          prefixIcon: Icon(
                            Icons.padding_outlined,
                            size: 18,
                            color: Theme.of(context).textTheme.bodyText1!.color
                          ),
                        ),
                        kHeightBox40,
                        CustomButton(
                          height: 45,
                          width: SizeConfig.screenWidth,
                          btnColor: kPrimaryColor,
                          title: 'Verification',
                          textColor: Colors.white,
                          onPress: () async {
                            if(otpController.text.isEmpty){
                              showCustomSnackBar(context, 'Please enter otp');
                            }else if(otpController.text.length < 6){
                              showCustomSnackBar(context, 'Please enter 6 digit otp code');
                            }else{
                              var results = await AuthProvider().signOtpVerification(
                                context,
                                otpCode: otpController.text,
                                email: email,
                              );
                              if (results.errors != null) {
                                Navigator.of(context).pop();
                                showCustomSnackBar(context, results.errors ?? '');
                              } else {
                                Navigator.of(context).pop();
                                await AuthProvider().saveUserInfo(results);
                                NavigatorHelper.openDashboardScreen(context);
                              }
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
