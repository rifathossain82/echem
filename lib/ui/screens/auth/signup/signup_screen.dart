import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';

import '../../../../helper/navigator_helper.dart';
import '../../../../resources/api_provider/auth_provider.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/input_from_widget.dart';
import '../../../widgets/main_app_bar.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final nameController = TextEditingController();
    final addressController = TextEditingController();
    final emailController = TextEditingController();
    final phoneController = TextEditingController();
    final passwordController = TextEditingController();
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: true,
              iconHide: true,
              rightSideHide: true,
            ),
            kHeightBox5,
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics(),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 30,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 60,
                      ),
                      Text(
                        'Profile Details',
                        style: Theme.of(context).textTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                      kHeightBox5,
                      InputFormWidget(
                        fieldController: nameController,
                        hintText: 'Name',
                      ),
                      InputFormWidget(
                        fieldController: addressController,
                        hintText: 'Address',
                      ),
                      kHeightBox10,
                      Text(
                        'Account Details',
                        style: Theme.of(context).textTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                      kHeightBox5,
                      InputFormWidget(
                        fieldController: phoneController,
                        hintText: 'Phone',
                        keyType: TextInputType.phone,
                      ),
                      InputFormWidget(
                        fieldController: emailController,
                        hintText: 'Email',
                        keyType: TextInputType.emailAddress,
                      ),
                      InputFormWidget(
                        fieldController: passwordController,
                        hintText: 'Password',
                        isProtected: true,
                      ),
                      kHeightBox10,
                      CustomButton(
                        height: 45,
                        width: SizeConfig.screenWidth,
                        btnColor: kPrimaryColor,
                        title: 'Continue To Sign Up',
                        textColor: Colors.white,
                        onPress: () async {
                          if (nameController.text.isEmpty) {
                            showCustomSnackBar(context, 'Please enter name');
                          } else if (addressController.text.isEmpty) {
                            showCustomSnackBar(context, 'Please enter address');
                          } else if (phoneController.text.isEmpty) {
                            showCustomSnackBar(context, 'Please enter phone');
                          } else if (phoneController.text.length < 11) {
                            showCustomSnackBar(
                                context, 'Please enter 11 digit phone number');
                          } else if (emailController.text.isEmpty) {
                            showCustomSnackBar(context, 'Please enter email');
                          } else if (passwordController.text.isEmpty) {
                            showCustomSnackBar(
                                context, 'Please enter password');
                          } else if (passwordController.text.length < 6) {
                            showCustomSnackBar(
                                context, 'Please enter minimum digit 6');
                          } else {
                            var results = await AuthProvider().userRegistration(
                              context,
                              name: nameController.text,
                              address: addressController.text,
                              phone: phoneController.text,
                              email: emailController.text,
                              password: passwordController.text,
                            );
                            /*if (results.errors != null) {
                              Navigator.of(context).pop();
                              showCustomSnackBar(context, results.errors ?? '');
                            } else {
                              Navigator.of(context).pop();
                              NavigatorHelper.openSignupVerificationScreen(
                                context,
                                email: emailController.text,
                              );
                            }*/
                            if (results.errors != null) {
                              Navigator.of(context).pop();
                              showCustomSnackBar(context, results.errors ?? '');
                            } else {
                              Navigator.of(context).pop();
                              await AuthProvider().saveUserInfo(results);
                              NavigatorHelper.openDashboardScreen(context);
                            }
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
