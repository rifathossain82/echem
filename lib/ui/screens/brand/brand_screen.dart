import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:flutter/material.dart';
  import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';
import '../home/components/bundle_offer_view.dart';
import 'widgets/top_cateogories_view.dart';

class BrandScreen extends StatelessWidget {
  const BrandScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: false,
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const BundleOfferView(
                      radius: 0,
                      removeCard: false,
                    ),
                    kHeightBox10,
                    Center(
                      child: Text(
                        'Top Brands',
                        style: Theme.of(context).textTheme.subtitle2?.copyWith(
                              color: kErrorColor,
                            ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    kHeightBox10,
                    const BundleOfferView(
                      radius: 0,
                      removeCard: true,
                      itemCount: 20,
                    ),
                    const TopCategoriesView()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
