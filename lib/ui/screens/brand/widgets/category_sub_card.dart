import 'package:echem/constants/size_config.dart';
import 'package:flutter/material.dart';

import '../../../../constants/style_data.dart';
import '../../../widgets/custom_image.dart';

class CategoriesSubCard extends StatelessWidget {
  final bool last;
  final int index;
  final String? image;

  const CategoriesSubCard({
    Key? key,
    required this.index,
    this.last = false,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenWidth(150),
      child: index == 0 ? CustomImage(
        image: image,
        radius: 5,
      ) : Container(
        color: Theme.of(context).cardColor,
        child:  Row(
          children: [
            kWidthBox20,
            SizedBox(
              height: getProportionateScreenWidth(130),
              width: getProportionateScreenWidth(130),
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  shape: BoxShape.circle,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    children: [
                      Expanded(
                        child: CustomImage(
                          image: image,
                          radius: 5,
                        ),
                      ),
                      Text('Lipstick', style: Theme.of(context).textTheme.subtitle1,),
                      kHeightBox10,
                    ],
                  ),
                ),
              ),
            ),
            kWidthBox20,
          ],
        ),
      ),
    );
  }
}
