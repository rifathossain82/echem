import 'package:flutter/material.dart';

import '../../../../constants/size_config.dart';
import 'category_sub_card.dart';

class CategoriesCard extends StatelessWidget {
  const CategoriesCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenWidth(160),
      child: ListView.builder(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: 10,
        padding: EdgeInsets.zero,
        itemBuilder: (context, int index) {
          return Column(
            children: [
              InkWell(
                child: SizedBox(
                    width: index == 0 ? SizeConfig.screenWidth! - 100 : null,
                    child:  CategoriesSubCard(index: index, image: index == 0 ? 'https://www.everydayonsales.com/wp-content/uploads/2021/11/SaSa-Makeup-Promo.jpg' : 'https://inglotcosmetics.com/media/k2/items/cache/0555bbe9368a05bff51437bdc1ff702e_XL.png',)),
              ),
            ],
          );
        },
      ),
    );
  }
}
