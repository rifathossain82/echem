import 'package:flutter/material.dart';
import '../../../../constants/colors_data.dart';
import '../../../../constants/style_data.dart';
import 'top_categories_card.dart';

class TopCategoriesView extends StatelessWidget {
  const TopCategoriesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        kHeightBox10,
        Center(
          child: Text(
            'Top Categories',
            style: Theme.of(context).textTheme.subtitle2?.copyWith(
              color: kErrorColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        kHeightBox10,
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 10,
          padding: EdgeInsets.zero,
          itemBuilder: (context, int index) {
            return const CategoriesCard();
          },
        ),
      ],
    );
  }
}
