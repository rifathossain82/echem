import 'package:echem/bloc/carts/cart_cubit.dart';
import 'package:echem/bloc/carts/cart_state.dart';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/images.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../constants/size_config.dart';
import '../../widgets/custom_alert_dialog.dart';
import '../../widgets/input_from_widget.dart';
import 'widgets/cart_card.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CartCubit()..getAllCartList(),
      child: BlocConsumer<CartCubit, CartState>(
        listener: (context, state) {},
        builder: (context, state) {
          CartCubit cubit = CartCubit.get(context);
          return Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics(),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          NavigatorHelper.openSearchScreen(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: InputFormWidget(
                            prefixIcon: Icon(
                              Icons.search,
                              color: Theme.of(context).disabledColor,
                            ),
                            absorbing: true,
                            hintText: 'Search For Products',
                          ),
                        ),
                      ),
                      kHeightBox10,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 20,
                            child: SvgPicture.asset(
                              Images.cart,
                              color: kBlackColor2,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            'My Bag',
                            style: Theme.of(context).textTheme.subtitle2,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      kHeightBox10,
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: cubit.cartList.length,
                        padding: EdgeInsets.zero,
                        itemBuilder: (context, int index) {
                          return CartCard(
                            cart: cubit.cartList[index],
                            onPress: (){
                              NavigatorHelper.openProductDetailsScreen(context, productId: cubit.cartList[index].id ?? 0);
                            },
                            onDecPress: (){
                              int qty = (cubit.cartList[index].qty ?? 0) - 1;
                              if((cubit.cartList[index].qty ?? 0) > 1){
                                context.read<CartCubit>().updateCartQuantity(cubit.cartList[index].id ?? 0, qty);
                              }else{
                                CustomAlertDialog().customAlert(
                                  context: context,
                                  title: 'Delete',
                                  body: 'Are you sure want to delete?',
                                  color: kPrimaryColor,
                                  confirmTitle: 'Delete',
                                  onPress: () {
                                    Navigator.pop(context);
                                    context.read<CartCubit>().removeFromCart(context, cubit.cartList[index].id ?? 0);
                                  },
                                );
                              }
                            },
                            onIncPress: (){
                              int qty = (cubit.cartList[index].qty ?? 0) + 1;
                              if(cubit.cartList[index].stock == null || (cubit.cartList[index].stock ?? 0) >= qty){
                                context.read<CartCubit>().updateCartQuantity(cubit.cartList[index].id ?? 0, qty);
                              }else{
                                showCustomSnackBar(context, 'No more stock!');
                              }
                            },
                            onRemovePress: (){
                              CustomAlertDialog().customAlert(
                                context: context,
                                title: 'Delete',
                                body: 'Are you sure want to delete?',
                                color: kPrimaryColor,
                                confirmTitle: 'Delete',
                                onPress: () {
                                  Navigator.pop(context);
                                  context.read<CartCubit>().removeFromCart(context, cubit.cartList[index].id ?? 0);
                                },
                              );
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.zero,
                elevation: 1,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: InkWell(
                    onTap: (){
                     if(cubit.cartList.isNotEmpty){
                       if(prefs.containsKey(token)){
                         NavigatorHelper.openCheckoutAddressScreen(context,
                             cart: cubit.cartList,
                             totalQty: cubit.totalQuantity,
                             subtotal: cubit.totalPrice,
                             currencySubtotal: cubit.subtotalCurrencyPrice);
                       }else{
                         NavigatorHelper.openLoginScreen(context);
                       }
                     }else{
                       showCustomSnackBar(context, 'Please add something to the bag!');
                     }
                    },
                    child: Container(
                      width: SizeConfig.screenWidth!,
                      height: 50,
                      decoration: BoxDecoration(
                        color: kErrorColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                'Total Price - $kCurrency ${cubit.totalPrice}.0',
                                style: Theme.of(context).textTheme.headline6!.copyWith(
                                  color: kWhiteColor,
                                  fontSize: 14,
                                  height: 1.0,
                                ),
                              ),
                            ),
                            Container(
                              width: 1.5,
                              margin: const EdgeInsets.symmetric(vertical: 13),
                              height: 50,
                              color: kWhiteColor,
                            ),
                            kWidthBox15,
                            Text(
                              'Next',
                              style: Theme.of(context).textTheme.headline6!.copyWith(
                                color: kWhiteColor,
                                fontSize: 14,
                                height: 1.0,
                              ),
                            ),
                            kWidthBox15,
                            const Icon(
                              Icons.keyboard_arrow_right_outlined,
                              color: kWhiteColor,
                              size: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        },),
    );
  }
}
