import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../constants/images.dart';
import '../../../../constants/size_config.dart';
import '../../../../constants/style_data.dart';
import '../../../../models/response/cart_model.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/custom_image.dart';
import '../../details/components/variation_circle.dart';

class CartCard extends StatelessWidget {
  final CartModel cart;
  final Function onPress, onIncPress, onDecPress, onRemovePress;
  const CartCard({
    Key? key,
    required this.cart,
    required this.onPress,
    required this.onIncPress,
    required this.onDecPress,
    required this.onRemovePress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        kHeightBox10,
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 0.0,
          ),
          width: SizeConfig.screenWidth,
          child: Card(
            color: Theme.of(context).cardColor,
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 1.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Row(
                children: [
                  InkWell(
                    onTap: onPress as Function(),
                    child: CustomImage(
                      width: 100,
                      height: 100,
                      baseUrl: kProductImagePath,
                      image: cart.photo,
                      radius: 5,
                    ),
                  ),
                  kWidthBox10,
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                cart.title ?? '',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            kWidthBox10,
                            InkWell(
                              onTap: onRemovePress as Function(),
                              child: Container(
                                width: 30,
                                height: 30,
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).disabledColor,
                                  shape: BoxShape.circle,
                                ),
                                child: SvgPicture.asset(
                                  Images.close,
                                  color: kBlackColor2,
                                ),
                              ),
                            ),
                            kWidthBox10,
                          ],
                        ),
                        kHeightBox10,
                        SizedBox(
                          child: cart.size == null ? const SizedBox() : Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).disabledColor,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                kOrdinaryShadow,
                                kOrdinaryShadow,
                              ]
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                              child: Row(
                                children: [
                                  Text(
                                    cart.size ?? '',
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  kWidthBox10,
                                  SizedBox(
                                    child: cart.color == null ? const SizedBox()  : VariationCircle(
                                      title: '',
                                      width: 30,
                                      height: 15,
                                      color: Color(int.parse('0xFF${cart.color?.substring(1)}')),
                                      isRounded: false,
                                      border: 0,
                                      borderColor: Colors.transparent,
                                      selected: true,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        kHeightBox10,
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                kCurrency + '${cart.price ?? 1}.0',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            kWidthBox10,
                            CustomButton(
                              title: '-',
                              onPress: onDecPress as Function(),
                              height: 28,
                              width: 35,
                              radius: 5,
                              btnColor: Theme.of(context).cardColor,
                              textColor: kPrimaryColor,
                              textSize: 8,
                              border: 1,
                              borderColor: kPrimaryColor,
                            ),
                            kWidthBox10,
                            Text(
                              '${cart.qty ?? 1}',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  ?.copyWith(fontSize: 20),
                            ),
                            kWidthBox10,
                            CustomButton(
                              onPress: onIncPress as Function(),
                              title: '+',
                              height: 28,
                              width: 35,
                              radius: 5,
                              btnColor: Theme.of(context).cardColor,
                              textColor: kPrimaryColor,
                              textSize: 8,
                              border: 1,
                              borderColor: kPrimaryColor,
                            ),
                          ],
                        ),
                        kHeightBox10,
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
