import 'package:echem/bloc/categories/category_bloc.dart';
import 'package:echem/bloc/categories/category_state.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/models/response/rp_categories.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../bloc/categories/category_event.dart';
import '../../../constants/colors_data.dart';
import '../../widgets/custom_shimmer.dart';
import '../../widgets/input_from_widget.dart';
import 'components/pop_category_view.dart';
import 'components/shop_category_view.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: kPrimaryColor,
      backgroundColor: Theme.of(context).cardColor,
      displacement: 0,
      onRefresh: () async {
        CategoryBloc().add(GetCategoryList());
      },
      child: BlocListener<CategoryBloc, CategoryState>(
        listener: (context, state) {
          if (state is CategoryError) {
            showCustomSnackBar(context, state.message ?? '');
          }
        },
        child: BlocBuilder<CategoryBloc, CategoryState>(
          builder: (context, state) {
            if (state is CategoryInitial) {
              return CustomShimmer.categoryShimmer(context, itemCount: 12);
            } else if (state is CategoryLoading) {
              return CustomShimmer.categoryShimmer(context, itemCount: 12);
            } else if (state is CategoryLoaded) {
              return Center(
                child: state.categoryModel.errors != null
                    ? Text(
                        state.categoryModel.errors ?? '',
                        style: Theme.of(context).textTheme.subtitle1,
                      )
                    : _buildCategoryBody(context, state.categoryModel),
              );
            } else if (state is CategoryError) {
              return Center(
                child: Text(
                  state.message ?? '',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              );
            } else {
              return Center(
                child: Text(
                  'No data available',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget _buildCategoryBody(BuildContext context, RpCategories model) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
             NavigatorHelper.openSearchScreen(context);
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: InputFormWidget(
                prefixIcon: Icon(
                  Icons.search,
                  color: Theme.of(context).disabledColor,
                ),
                absorbing: true,
                hintText: 'Search For Products',
              ),
            ),
          ),
          kHeightBox10,
          PopCategoryView(categoryList: model.data!),
          kHeightBox10,
          if(model.data![0].subs!.isNotEmpty)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Text(
                  'Shop By Concern',
                  style: Theme.of(context).textTheme.subtitle2,
                  textAlign: TextAlign.center,
                ),
              ),
              kHeightBox10,
              ShopCategoryView(
                subList: model.data![0].subs!,
              ),
            ],
          ),
          //const NbBottomWidgets(),
        ],
      ),
    );
  }
}
