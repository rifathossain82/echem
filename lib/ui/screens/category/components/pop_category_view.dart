import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../../../helper/navigator_helper.dart';
import '../../../../models/response/category_list.dart';
import '../widgets/pop_category_card.dart';

class PopCategoryView extends StatelessWidget {
  final List<CategoryList> categoryList;

  const PopCategoryView({
    Key? key,
    required this.categoryList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
      ),
      child: MasonryGridView.count(
        crossAxisCount: 2,
        shrinkWrap: true,
        itemCount: categoryList.length,
        physics: const NeverScrollableScrollPhysics(),
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              NavigatorHelper.openFilterScreen(context,
                  categoryId: (categoryList[index].id ?? 0).toString(),
                  subCategoryList: categoryList[index].subs!  );
            },
            child: PopCategoryCard(
              name: categoryList[index].name ?? '',
              image: categoryList[index].photo ?? '',
              last: index == 10 - 1 ? true : false,
            ),
          );
        },
      ),
    );
  }
}
