import 'package:flutter/material.dart';
import '../../../../helper/navigator_helper.dart';
import '../../../../models/response/rp_categories.dart';
import '../widgets/shop_category_card.dart';

class ShopCategoryView extends StatelessWidget {
  final List<Subs> subList;

  const ShopCategoryView({
    Key? key,
    required this.subList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
      ),
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: subList.length,
        padding: EdgeInsets.zero,
        itemBuilder: (context, int index) {
          return InkWell(
            onTap: () {
              NavigatorHelper.openFilterScreen(context, subCatId: (subList[index].id ?? 0).toString(), subCategoryList: subList  ,);
            },
            child: ShopCategoryCard(
              name: subList[index].name ?? '',
              image: null,
            ),
          );
        },
      ),
    );
  }
}
