import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';
import '../../../widgets/custom_image.dart';

class PopCategoryCard extends StatelessWidget {
  final bool last;
  final String? name;
  final String? image;

  const PopCategoryCard({
    Key? key,
    this.name,
    this.image,
    this.last = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.screenWidth! / 3.2,
      width: SizeConfig.screenWidth,
      child: Stack(
        children: [
          CustomImage(
            height: SizeConfig.screenWidth! / 2.8,
            width: SizeConfig.screenWidth,
            image: image,
            baseUrl: kCategoryImagePath,
            radius: 10,
            innerShadow: true,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Text(
                  name ?? '',
                  style: Theme.of(context).textTheme.subtitle1?.copyWith(
                        color: kWhiteColor,
                        fontWeight: FontWeight.w700,
                      ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
