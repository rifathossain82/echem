import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/ui/widgets/more_widgets.dart';
import 'package:flutter/material.dart';
import '../../../widgets/custom_image.dart';

class ShopCategoryCard extends StatelessWidget {
  final bool last;
  final String? name;
  final String? image;

  const ShopCategoryCard({
    Key? key,
    this.last = false,
    this.name,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenWidth! / 4.2,
      width: SizeConfig.screenWidth,
      padding: const EdgeInsets.only(
        bottom: 5,
      ),
      child: Stack(
        children: [
          CustomImage(
            height: SizeConfig.screenWidth! / 2.8,
            width: SizeConfig.screenWidth,
            image: image,
            radius: 10,
            fit: BoxFit.fitWidth,
            innerShadow: true,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: Text(
                  name ?? '',
                  style: Theme.of(context).textTheme.subtitle1?.copyWith(
                        color: kWhiteColor,
                        fontWeight: FontWeight.w700,
                      ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: const [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: MoreWidgets(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
