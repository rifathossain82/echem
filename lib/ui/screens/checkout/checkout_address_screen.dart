import 'dart:convert';
import 'dart:developer';

import 'package:echem/bloc/checkout/checkout_cubit.dart';
import 'package:echem/bloc/checkout/checkout_state.dart';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/models/response/city_model.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/doube_tap_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../helper/navigator_helper.dart';
import '../../../models/response/cart_model.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/main_app_bar.dart';

var scaffoldKey = GlobalKey<ScaffoldState>();
var formKey = GlobalKey<FormState>();

class CheckoutAddressScreen extends StatefulWidget {
  final List<CartModel> cart;
  final int subtotal;
  final int totalQty;
  final double currencySubtotal;

  const CheckoutAddressScreen({
    Key? key,
    required this.cart,
    required this.subtotal,
    required this.totalQty,
    required this.currencySubtotal,
  }) : super(key: key);

  @override
  State<CheckoutAddressScreen> createState() => _CheckoutAddressScreenState();
}

class _CheckoutAddressScreenState extends State<CheckoutAddressScreen> {



  String dropdownValue = 'Dhaka';
  String upazilaDropdownValue = 'Mohammadpur';

  double shippingCharge = 0;

  void calculateShippingCharge(value){
    print(value);
    double charge = 0;

    for(int i = 0; i < widget.cart.length; i++){
      int productQuantity = widget.cart[i].qty!;
      double productWeight = double.parse(jsonDecode(widget.cart[i].products.toString())['weight'].toString());
      
      if(value == "Dhaka"){
        setState(() {
          dropdownValue = value!;
          charge += 60 + (productWeight * (productQuantity - 1) * 15);
        });
      } else{
        setState(() {
          dropdownValue = value!;
          charge += 120 + (productWeight * (productQuantity - 1) * 20);
        });
      }
    }

    if(value == "Dhaka"){
      setState(() {
        shippingCharge = charge;
      });
    } else{
      setState(() {
        shippingCharge = charge;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    calculateShippingCharge(dropdownValue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        bottomNavigationBar: const BottomNav(),
        drawer: const CategoryDrawer(),
        body:  BlocBuilder<CheckoutCubit, CheckoutState>(
          builder: (context, state) {
            CheckoutCubit cubit = CheckoutCubit.get(context);
            return SafeArea(
              child: Column(
                children: [
                  const MainCustomAppBar(
                    backButton: true,
                  ),
                  kHeightBox5,
                  Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(
                        parent: AlwaysScrollableScrollPhysics(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Form(
                          key: formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              kHeightBox10,
                              InputFormWidget(
                                fieldController: cubit.nameController,
                                hintText: 'Enter your name',
                                title: 'Name',
                                titleColor: kBlackColor2,
                                validation: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Name must not be empty';
                                  }
                                  return null;
                                },
                              ),
                              InputFormWidget(
                                fieldController: cubit.phoneController,
                                hintText: 'Enter your phone number',
                                title: 'Phone',
                                titleColor: kBlackColor2,
                                validation: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Phone must not be empty';
                                  }
                                  return null;
                                },
                              ),
                              InputFormWidget(
                                fieldController: cubit.emailController,
                                hintText: 'Enter your email',
                                title: 'Email',
                                titleColor: kBlackColor2,
                                validation: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Email must not be empty';
                                  }
                                  return null;
                                },
                              ),
                              kHeightBox30,
                              InputFormWidget(
                                fieldController: cubit.addressController,
                                hintText: 'Enter your address',
                                title: 'Address',
                                titleColor: kBlackColor2,
                                validation: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Address must not be empty';
                                  }
                                  return null;
                                },
                              ),
                              const Text("District", style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                              ),),
                              const SizedBox(
                                height: 4,
                              ),
                              Container(
                                width: double.infinity,
                                height: getProportionateScreenHeight(45.0),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).cardColor,
                                  border: Border.all(width: 3, color: Colors.green),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    value: dropdownValue,
                                    hint: const Text('Select District'),
                                    isExpanded: true,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    elevation: 16,
                                    // style: const TextStyle(color: Colors.deepPurple),

                                    onChanged: calculateShippingCharge,
                                    items: cubit.districtList.map<DropdownMenuItem<String>>((dynamic value) {
                                      return DropdownMenuItem<String>(
                                        value: value.zela.toString(),
                                        child: Text(value.zela.toString()),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              // InputFormWidget(
                              //   fieldController: cubit.districtController,
                              //   hintText: 'District (xxxxxxxxxxxxxxxx)',
                              //   title: 'District',
                              //   titleColor: kBlackColor2,
                              //   validation: (String? value) {
                              //     if (value!.isEmpty) {
                              //       return 'District must not be empty';
                              //     }
                              //     return null;
                              //   },
                              // ),
                              const SizedBox(
                                height: 5,
                              ),
                              const Text("Upazila", style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                              ),),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: double.infinity,
                                height: getProportionateScreenHeight(45.0),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).cardColor,
                                  border: Border.all(width: 3, color: Colors.green),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    value: upazilaDropdownValue.isEmpty ? null : upazilaDropdownValue,
                                    hint: const Text('Select Upazila'),
                                    isExpanded: true,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    elevation: 16,
                                    // style: const TextStyle(color: Colors.deepPurple),
                                    onChanged: (String? value) {
                                      // This is called when the user selects an item.
                                      print(value);
                                      setState(() {
                                        upazilaDropdownValue = value!;
                                      });
                                    },
                                    items: cubit.upazilaList.map<DropdownMenuItem<String>>((dynamic value) {
                                      return DropdownMenuItem<String>(
                                        value: value.upazila.toString(),
                                        child: Text(value.upazila.toString()),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),

                              InputFormWidget(
                                fieldController: cubit.postalController,
                                hintText: 'Postal code (00 xxxx)',
                                title: 'Post Code',
                                titleColor: kBlackColor2,
                                validation: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Zip must not be empty';
                                  }
                                  return null;
                                },
                              ),
                              kHeightBox30,
                              InputFormWidget(
                                fieldController: cubit.noteController,
                                hintText: 'Enter order note',
                                title: 'Note (optional)',
                                titleColor: kBlackColor2,
                                maxLines: 4,
                              ),
                              kHeightBox30,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: DoubleTapButton(
                      width: SizeConfig.screenHeight,
                      height: 45,
                      title: 'Back',
                      title2: 'Next',
                      onPress: () => Navigator.of(context).pop(),
                      onPress2: () {
                        if (formKey.currentState!.validate()) {
                          context.read<CheckoutCubit>().setCartData(
                              cart: widget.cart,
                              totalQty: widget.totalQty,
                              subtotal: widget.subtotal,
                              shippingCost: shippingCharge,
                              currencySubtotal: widget.currencySubtotal,
                          );
                          NavigatorHelper.openCheckoutScreen(context, shippingCharge);
                        }

                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),);
  }
}
