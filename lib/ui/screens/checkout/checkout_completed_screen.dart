import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../bloc/checkout/checkout_cubit.dart';
import '../../../bloc/checkout/checkout_state.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';

class CheckoutCompleteScreen extends StatelessWidget {
  final int orderId;
  final String orderNumber;
  const CheckoutCompleteScreen({
    Key? key,
    required this.orderId,
    required this.orderNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        NavigatorHelper.openDashboardScreen(context);
        return true;
      },
      child: Scaffold(
        bottomNavigationBar: const BottomNav(),
        drawer: const CategoryDrawer(),
        body: SafeArea(
          child: BlocBuilder<CheckoutCubit, CheckoutState>(
            builder: (context, state) {
              CheckoutCubit cubit = CheckoutCubit.get(context);
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MainCustomAppBar(
                    backButton: true,
                    onBackPress: (){
                      NavigatorHelper.openDashboardScreen(context);
                    },
                  ),
                  kHeightBox5,
                  Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(
                        parent: AlwaysScrollableScrollPhysics(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            kHeightBox40,
                            const Center(child: Icon(Icons.check, size: 70,),),
                            kHeightBox20,
                            Center(
                              child: Text(
                                'Thank you',
                                style: Theme.of(context).textTheme.headline2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            kHeightBox30,
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 15),
                                child: Text(
                                  'Thank you so much for your purchase.\nYou will soon be notified when we process\nyour order.'
                                  ,
                                  style: Theme.of(context).textTheme.subtitle1?.copyWith(
                                      height: 1.5
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            kHeightBox20,
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Theme.of(context).cardColor,
                              ),
                              child: Column(
                                children: [
                                  Text(
                                    'Here are your order details below',
                                    style: Theme.of(context).textTheme.subtitle1,
                                    textAlign: TextAlign.start,
                                  ),
                                  kHeightBox15,
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Order ID',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6,
                                        ),
                                      ),
                                      kWidthBox10,
                                      Text(
                                        orderNumber,
                                        style:
                                        Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ],
                                  ),
                                  kHeightBox15,
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Order Phone',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6,
                                        ),
                                      ),
                                      kWidthBox10,
                                      Text(
                                        cubit.phoneController.text,
                                        style:
                                        Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ],
                                  ),
                                  kHeightBox15,
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Order Total',
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6,
                                        ),
                                      ),
                                      kWidthBox10,
                                      Text(
                                        kCurrency + ' ${cubit.subtotalPrice + cubit.shippingCost}.00',
                                        style:
                                        Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ],
                                  ),
                                  kHeightBox15,
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Order Address',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6,
                                        ),
                                      ),
                                      kWidthBox10,
                                      Text(
                                        cubit.addressController.text,
                                        style:
                                        Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            kHeightBox20,
                            CustomButton(
                              width: SizeConfig.screenWidth,
                              height: 45,
                              btnColor: kErrorColor,
                              title: 'View Order Details',
                              onPress: () {
                                NavigatorHelper.openOrderDetailsScreen(context, orderId: orderId);
                              },
                            ),
                            kHeightBox10,
                            CustomButton(
                              width: SizeConfig.screenWidth,
                              height: 45,
                              title: 'Shop Again',
                              onPress: () => NavigatorHelper.openDashboardScreen(context),
                            ),
                            kHeightBox30,
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          )
        ),
      ),
    );
  }
}

