import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/doube_tap_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/checkout/checkout_cubit.dart';
import '../../../bloc/checkout/checkout_state.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';

class CheckoutScreen extends StatelessWidget {
  const CheckoutScreen({
    this.charge,
    Key? key,
  }) : super(key: key);

  final double? charge;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: SafeArea(
        child: BlocBuilder<CheckoutCubit, CheckoutState>(
          builder: (context, state) {
            CheckoutCubit cubit = CheckoutCubit.get(context);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MainCustomAppBar(
                  backButton: true,
                ),
                kHeightBox5,
                Expanded(
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics(),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          kHeightBox20,
                          Text(
                            '   Select Payment method:',
                            style: Theme.of(context).textTheme.headline3,
                            textAlign: TextAlign.start,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 40, horizontal: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                InkWell(
                                  onTap: () {
                                    context.read<CheckoutCubit>().updatePaymentType('cod');
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                        padding:
                                        const EdgeInsets.only(right: 10),
                                        child: Icon(
                                          Icons.circle,
                                          color: cubit.paymentType ==
                                              'cod'
                                              ? kPrimaryColor
                                              : Theme.of(context).disabledColor,
                                          size: 25,
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Cash on delivery',
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              ?.copyWith(
                                            color: cubit.paymentType ==
                                                'cod'
                                                ? kErrorColor
                                                : Theme.of(context)
                                                .textTheme
                                                .headline5
                                                ?.color,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                kHeightBox20,
                                InkWell(
                                  onTap: () {
                                    context.read<CheckoutCubit>().updatePaymentType('ssl');
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                        padding:
                                        const EdgeInsets.only(right: 10),
                                        child: Icon(
                                          Icons.circle,
                                          color: cubit.paymentType == 'ssl'
                                              ? kPrimaryColor
                                              : Theme.of(context).disabledColor,
                                          size: 25,
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Digital Payment',
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              ?.copyWith(
                                            color: cubit.paymentType == 'ssl'
                                                ? kErrorColor
                                                : Theme.of(context)
                                                .textTheme
                                                .headline5
                                                ?.color,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context).cardColor,
                            ),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Subtotal',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                    ),
                                    kWidthBox10,
                                    Text(
                                      kCurrency + '${cubit.subtotalPrice}.0',
                                      style:
                                      Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ],
                                ),
                                kHeightBox15,
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Shipping',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                    ),
                                    kWidthBox10,
                                    Text(
                                      kCurrency + '${cubit.shippingCost}',
                                      style:
                                      Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ],
                                ),
                                kHeightBox15,
                               /* Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Payment Gateway Discount',
                                        maxLines: 1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                    ),
                                    kWidthBox10,
                                    Text(
                                      '- ' + kCurrency + '20.00',
                                      style:
                                      Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ],
                                ),
                                kHeightBox15,*/
                                const Divider(
                                  color: kBlackColor2,
                                  height: 1,
                                  thickness: 1.5,
                                ),
                                kHeightBox15,
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Total',
                                        style: Theme.of(context).textTheme.headline6,
                                      ),
                                    ),
                                    kWidthBox10,
                                    Text(
                                      kCurrency + '${cubit.subtotalPrice + cubit.shippingCost!}',
                                      style:
                                      Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          kHeightBox30,
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: DoubleTapButton(
                    width: SizeConfig.screenHeight,
                    height: 45,
                    title: 'Back',
                    title2: 'Confirm Order',
                    onPress: () => Navigator.of(context).pop(),
                    onPress2: () {
                      context.read<CheckoutCubit>().createOrder(context, transId: DateTime.now().microsecondsSinceEpoch.toString());
                    },
                  ),
                ),
              ],
            );
          },
        )
      ),
    );
  }
}
