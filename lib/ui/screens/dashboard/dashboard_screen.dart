import 'dart:io';
import 'package:echem/constants/strings.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/main.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../constants/colors_data.dart';
import '../../../constants/images.dart';
import '../../../constants/style_data.dart';
import '../../../resources/repository/api_repository.dart';
import '../../widgets/bottom_nav_item.dart';
import '../../widgets/custom_alert_dialog.dart';
import '../../widgets/main_app_bar.dart';
import '../account/account_screen.dart';
import '../cart/cart_screen.dart';
import '../category/category_screen.dart';
import '../home/home_screen.dart';

class DashboardScreen extends StatefulWidget {
  static const routeName = 'dashboard';

  const DashboardScreen({Key? key, this.index = 0}) : super(key: key);
  final int index;

  @override
  YourPageState createState() => YourPageState();
}

class YourPageState extends State<DashboardScreen> {
  PageController? _pageController;
  int _pageIndex = 0;
  final List<Widget> _screens = [
    const HomeScreen(),
    const CategoryScreen(),
    const CartScreen(),
    const AccountScreen(isAccount: false,),

  ];
  final GlobalKey<ScaffoldMessengerState> _scaffoldKey = GlobalKey();
  final bool _canExit = Platform.isWindows ? true : false;
  final ApiRepository apiRepository = ApiRepository();

  @override
  void initState() {
    super.initState();

    _pageIndex = widget.index;

    _pageController = PageController(initialPage: widget.index);

    Future.delayed(const Duration(seconds: 1), () {
      setState(() {});
    });
    if(!prefs.containsKey(currencyName)){
      getInitialData();
    }
  }

  //get init data
  void getInitialData() async {
    try {
      var list = await apiRepository.fetchInitialData();
      if (list.errors == null) {
        /**
         * "name": "BDT",
            "sign": "৳",
            "value": 84.6299999999999954525264911353588104248046875,
         */
        await prefs.setString(currencyName, list.currency?.name ?? '');
        await prefs.setDouble(currencyPrice, double.parse(list.currency?.value ?? '0.0'));
        await prefs.setString(currencySign, list.currency?.sign ?? '');
      }
    } on NetworkError {
      showCustomSnackBar(context, 'Please connect the internet');
    }
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_pageIndex != 0) {
          _setPage(0);
          return false;
        } else {
          if (_canExit) {
            return true;
          } else {
            return _onBackPressed();
          }
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        bottomNavigationBar: BottomAppBar(
          elevation: .5,
          notchMargin: 5,
          clipBehavior: Clip.antiAlias,
          shape: const CircularNotchedRectangle(),
          child: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Row(children: [
              BottomNavItem(
                iconData: Images.home,
                label: "Home",
                isSelected: _pageIndex == 0,
                onTap: () => _setPage(0),
              ),
              BottomNavItem(
                  iconData: Images.category,
                  label: "Category",
                  isSelected: _pageIndex == 1,
                  onTap: () => _setPage(1)),
              BottomNavItem(
                  iconData: Images.cart,
                  label: "Bag",
                  isSelected: _pageIndex == 2,
                  onTap: () => _setPage(2)),
              BottomNavItem(
                  iconData: Images.account,
                  label: "Account",
                  isSelected: _pageIndex == 3,
                  onTap: () {
                    if(prefs.containsKey(token)){
                      _setPage(3);
                    }else{
                      NavigatorHelper.openLoginScreen(context);
                    }
                  }),
            ]),
          ),
        ),
        drawer: const CategoryDrawer(),
        body: SafeArea(
          child: Column(
            children: [
               MainCustomAppBar(
                backButton:  false,
                isAccountScreen: _pageIndex == 3 ? true : false,
              ),
              Expanded(
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: _screens.length,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return _screens[index];
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _setPage(int pageIndex) {
    setState(() {
      _pageController?.jumpToPage(pageIndex);
      _pageIndex = pageIndex;
    });
  }

  Future<bool> _onBackPressed() {
    return CustomAlertDialog()
        .customAlert(
          context: context,
          confirmTitle: 'Exit',
          cancelTitle: 'No',
          title: 'Are you sure?',
          body: 'Do you want to exit an app!',
          color: kPrimaryColor,
          onPress: () {
            SystemNavigator.pop();
          },
        )
        .show()
        .then((value) => value as bool);
  }
}
