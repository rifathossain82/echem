import 'package:carousel_slider/carousel_slider.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';
import '../../../../bloc/product_details/details_cubit.dart';
import '../../../../constants/colors_data.dart';
import '../../../widgets/custom_image.dart';

class SliderView extends StatelessWidget {
  final DetailsCubit cubit;

  const SliderView({
    Key? key,
    required this.cubit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          options: CarouselOptions(
              aspectRatio: 1.5,
              viewportFraction: 1,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: false,
              autoPlayInterval: const Duration(seconds: 5),
              autoPlayAnimationDuration: const Duration(milliseconds: 1000),
              autoPlayCurve: Curves.easeInCubic,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
              onPageChanged: (int index, reason) {
                cubit.setCurrentIndex(index);
               
              }),
          items: cubit.productGallery.map(
                (item) {
                  int index = cubit.productGallery.indexOf(item);
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 2,),
                    child: CustomImage(
                      image: item.photo,
                      baseUrl: index == cubit.productGallery.length - 1 ? kProductImagePath : kGalleryImagePath,
                      radius: 0,
                      fit: BoxFit.scaleDown,
                    ),
                  );
                },
              )
              .toList(),
        ),
        kHeightBox10,
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: cubit.productGallery.map((banner) {
            int index = cubit.productGallery.indexOf(banner);
            return Row(
              children: [
                Container(
                  height: cubit.currentIndex == index ? 13 : 8,
                  width: cubit.currentIndex == index ? 13 : 8,
                  decoration: BoxDecoration(
                    color: cubit.currentIndex == index
                        ? kErrorColor
                        : kErrorColor.withOpacity(.5),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 3.5,
                ),
              ],
            );
          }).toList(),
        ),
      ],
    );
  }
}
