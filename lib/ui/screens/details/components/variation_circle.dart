import 'package:echem/constants/colors_data.dart';
import 'package:flutter/material.dart';

class VariationCircle extends StatelessWidget {
  final String title;
  final Function? onPress;
  final Color? color;
  final Color? borderColor;
  final double border, width, height, radius;
  final bool selected;
  final bool isColor;
  final bool isRounded;
  const VariationCircle({
    Key? key,
    required this.title,
    this.onPress,
    this.color,
    this.borderColor,
    this.border = 1,
    this.radius = 0,
    this.width = 30,
    this.height = 30,
    this.selected = false,
    this.isColor = false,
    this.isRounded = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress as Function()?,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: color ?? (selected ? kPrimaryColor : Theme.of(context).backgroundColor),
          borderRadius: isRounded ? null : BorderRadius.circular(radius),
          shape: isRounded ? BoxShape.circle : BoxShape.rectangle,
            border: Border.all(width: border, color: isColor ? selected ? kPrimaryColor : Theme.of(context).textTheme.bodyText2!.color! :
            selected ? Colors.transparent : Theme.of(context).textTheme.bodyText2!.color!)
        ),
        child: Center(child: Text(title, style: Theme.of(context).textTheme.subtitle2?.copyWith(
          fontSize: 10,
          color: selected ? kWhiteColor : Theme.of(context).textTheme.bodyText2!.color
        ),),),
      ),
    );
  }
}
