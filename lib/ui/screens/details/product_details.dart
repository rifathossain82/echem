import 'dart:developer';

import 'package:echem/bloc/carts/cart_cubit.dart';
import 'package:echem/bloc/product_details/details_cubit.dart';
import 'package:echem/bloc/product_details/details_state.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../constants/colors_data.dart';
import '../../../constants/strings.dart';
import '../../../constants/style_data.dart';
import '../../../resources/api/app_config.dart';
import '../../widgets/category_drawer.dart';
import '../../widgets/custom_image.dart';
import '../../widgets/custom_shimmer.dart';
import '../../widgets/grid_view_widgets.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/main_app_bar.dart';
import 'components/slider_view.dart';
import 'components/variation_circle.dart';

var scaffoldKey = GlobalKey<ScaffoldState>();

class DetailsScreen extends StatelessWidget {
  final int productId;

  const DetailsScreen({
    Key? key,
    required this.productId,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocProvider(
      create: (BuildContext context) => DetailsCubit()..getProductDetails(productId.toString()),
      child: BlocConsumer<DetailsCubit, DetailsState>(
          listener: (context, state) {},
          builder: (context, state) {
            DetailsCubit cubit = DetailsCubit.get(context);
            return Scaffold(
              key: scaffoldKey,
              drawer: const CategoryDrawer(),
              body: SafeArea(
                      child: Column(
                        children: [
                          const MainCustomAppBar(
                            backButton: true,
                          ),
                          kHeightBox10,
                          Expanded(
                            child: Stack(
                              children: [
                                SingleChildScrollView(
                                  physics: const BouncingScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics(),
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                     SizedBox(
                                       child: cubit.isLoading ? CustomShimmer.productDetailsShimmer(context) : Column(
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           InkWell(
                                             onTap: () {},
                                             child: Padding(
                                               padding: const EdgeInsets.symmetric(
                                                   horizontal: 10),
                                               child: InputFormWidget(
                                                 prefixIcon: Icon(
                                                   Icons.search,
                                                   color: Theme.of(context).disabledColor,
                                                 ),
                                                 absorbing: true,
                                                 hintText: 'Search For Products',
                                               ),
                                             ),
                                           ),
                                           kHeightBox10,
                                           SizedBox(
                                             child: cubit.productDetails.galleries!.length < 2 ? AspectRatio(
                                               aspectRatio: 1.5,
                                               child: CustomImage(
                                                 image: cubit.productDetails.photo,
                                                 baseUrl: kProductImagePath,
                                                 radius: 0,
                                                 fit: BoxFit.scaleDown,
                                               ),
                                             ) : SliderView(cubit: cubit,),
                                           ),
                                           kHeightBox20,
                                           Column(
                                             crossAxisAlignment:
                                             CrossAxisAlignment.center,
                                             children: [
                                               Text(
                                                 cubit.productDetails.name ?? '',
                                                 maxLines: 2,
                                                 overflow: TextOverflow.ellipsis,
                                                 textAlign: TextAlign.center,
                                                 style: Theme.of(context).textTheme.headline2,
                                               ),
                                               kHeightBox10,
                                               Row(
                                                 mainAxisAlignment:
                                                 MainAxisAlignment.center,
                                                 children: [
                                                  /* Container(
                                                     padding:
                                                     const EdgeInsets.symmetric(
                                                       horizontal: 5,
                                                       vertical: 2,
                                                     ),
                                                     decoration: BoxDecoration(
                                                       color: Colors.red.shade400,
                                                       borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                     ),
                                                     child: Text(
                                                       '100 ml',
                                                       style: Theme.of(context).textTheme.bodyText2?.copyWith(
                                                         color: kWhiteColor,
                                                       ),
                                                     ),
                                                   ),
                                                   kWidthBox10,*/
                                                   if (cubit.productDetails.averageRating != null)
                                                     RatingBarIndicator(
                                                       rating: double.parse(cubit.productDetails.averageRating ?? '0.0'),
                                                       itemBuilder: (context, index) => const Icon(
                                                         Icons.star,
                                                         color: Colors.amber,
                                                       ),
                                                       itemCount: 5,
                                                       itemSize: 18.0,
                                                       direction: Axis.horizontal,
                                                     ),
                                                 ],
                                               ),
                                               kHeightBox10,
                                               RichText(
                                                 text: TextSpan(
                                                   children: [
                                                     TextSpan(
                                                         text: kCurrency + '${BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.price ?? '0.0')} ',
                                                         style: Theme.of(context)
                                                             .textTheme
                                                             .subtitle2
                                                             ?.copyWith(
                                                           fontWeight:
                                                           FontWeight.w600,
                                                         )),
                                                     TextSpan(
                                                       text: kCurrency + '${BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.previousPrice ?? '0.0')}.0',
                                                       style: Theme.of(context)
                                                           .textTheme
                                                           .subtitle2
                                                           ?.copyWith(
                                                         color: Theme.of(context)
                                                             .disabledColor,
                                                         decoration:
                                                         TextDecoration
                                                             .lineThrough,
                                                       ),
                                                     ),
                                                     if(BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.previousPrice ?? '0.0') > BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.price ?? '0.0'))
                                                     TextSpan(
                                                         text:
                                                         ' Save $kCurrency ${BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.previousPrice ?? '0.0') - BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.price ?? '0.0')} (${BaseMethod.discountPercentage(prevPrice: BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.previousPrice ?? '0.0'), price: BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.price ?? '0.0'))}% off)',
                                                         style: Theme.of(context)
                                                             .textTheme
                                                             .subtitle2
                                                             ?.copyWith(
                                                           fontWeight:
                                                           FontWeight.w600,
                                                           color: kErrorColor,
                                                         )),
                                                   ],
                                                 ),
                                               ),
                                               kHeightBox10,

                                               Padding(
                                                 padding: const EdgeInsets.symmetric(horizontal: 10),
                                                 child: Html(data: cubit.productDetails.details ?? '',
                                                     style: {
                                                       // tables will have the below background color
                                                       "table": Style(
                                                         backgroundColor: const Color.fromARGB(0x50, 0xee, 0xee, 0xee),
                                                         width: size.width,
                                                       ),
                                                       // some other granular customizations are also possible
                                                       "tr": Style(
                                                         border: const Border(bottom: BorderSide(color: Colors.grey)),
                                                       ),
                                                       "th": Style(
                                                         padding: const EdgeInsets.all(6),
                                                         backgroundColor: Colors.grey,
                                                       ),
                                                       "td": Style(
                                                         padding: const EdgeInsets.all(6),
                                                         alignment: Alignment.topLeft,
                                                         width: size.width * 0.45,
                                                       ),
                                                       // text that renders h1 elements will be red
                                                       "h1": Style(color: Theme.of(context).textTheme.subtitle1?.color!, fontSize: FontSize.large),
                                                       "h2": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "h3": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "h4": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "h5": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "h6": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "p": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.large),
                                                       "span": Style(color: Theme.of(context).textTheme.subtitle1?.color!,fontSize: FontSize.medium),
                                                       "": Style(fontSize: FontSize.large, color: Colors.red),
                                                     },)

                                               ),
                                             ],
                                           ),
                                           kHeightBox20,
                                           SizedBox(
                                             child: cubit.productDetails.size!.isEmpty ? const SizedBox()
                                                 : Column(
                                               crossAxisAlignment: CrossAxisAlignment.center,
                                               children: [
                                                 kHeightBox10,
                                                 Center(
                                                   child: Text(
                                                     'Select Size',
                                                     style: Theme.of(context).textTheme.subtitle2,
                                                     textAlign: TextAlign.center,
                                                   ),
                                                 ),
                                                 kHeightBox10,
                                                 Row(
                                                     mainAxisAlignment: MainAxisAlignment.center,
                                                     children: cubit.productDetails.size!.map((e){
                                                       int index = cubit.productDetails.size!.indexOf(e);
                                                       String stock =  cubit.productDetails.sizeQty![index];
                                                       String price =  cubit.productDetails.sizePrice![index];
                                                       return Row(
                                                         children: [
                                                           VariationCircle(
                                                             onPress: (){
                                                               context.read<DetailsCubit>().updateSizeSelected(e, stock, price, cubit.productDetails.price!,index);
                                                             },
                                                             title: e,
                                                             isRounded: false,
                                                             radius: 2,
                                                             width: 55,
                                                             height: 22,
                                                             selected: cubit.selectedSize == null ? false : e == cubit.selectedSize ? true : false,
                                                           ),
                                                           if(index + 1 != cubit.productDetails.size!.length)
                                                             kWidthBox10,
                                                         ],
                                                       );
                                                     }).toList()

                                                 ),
                                                 kHeightBox10,
                                               ],
                                             ),
                                           ),
                                           SizedBox(
                                             child: cubit.productDetails.color!.isEmpty ? const SizedBox()
                                                 : Column(
                                               crossAxisAlignment: CrossAxisAlignment.center,
                                               children: [
                                                 kHeightBox10,
                                                 Center(
                                                   child: Text(
                                                     'Select Color',
                                                     style: Theme.of(context).textTheme.subtitle2,
                                                     textAlign: TextAlign.center,
                                                   ),
                                                 ),
                                                 kHeightBox10,
                                                 Row(
                                                     mainAxisAlignment: MainAxisAlignment.center,
                                                     children: cubit.productDetails.color!.map((e){
                                                       int index = cubit.productDetails.color!.indexOf(e);
                                                       return Row(
                                                         children: [
                                                           VariationCircle(
                                                             onPress: (){
                                                               context.read<DetailsCubit>().updateColorSelected(e);
                                                             },
                                                             title: '',
                                                             color: Color(int.parse('0xFF${e.substring(1)}')),
                                                             isColor: true,
                                                             border: cubit.selectedColor == null ? 0 : 2,
                                                             selected: cubit.selectedColor == null ? false : e == cubit.selectedColor ? true : false,
                                                           ),
                                                           if(index + 1 != cubit.productDetails.color!.length)
                                                             kWidthBox10,
                                                         ],
                                                       );
                                                     }).toList()

                                                 ),
                                                 kHeightBox10,
                                               ],
                                             ),
                                           ),
                                         ],
                                       ),
                                     ),
                                      kHeightBox10,
                                      SizedBox(
                                        child: cubit.isLoading || cubit.isProductsShimmer ? CustomShimmer.gridProductWithTitleShimmer(context, title: 'Products You May Like', crossCount: 3)
                                            : cubit.productList.isEmpty ? const SizedBox()
                                            : Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            kHeightBox10,
                                            Center(
                                              child: Text(
                                                'Products You May Like',
                                                style: Theme.of(context).textTheme.subtitle2,
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            kHeightBox10,
                                            GridViewList(
                                              crossCount: 3,
                                              productList: cubit.productList,
                                              isHome: true,
                                              isDetailsScreen: true,
                                              paddingTobBottom: 5,
                                            ),
                                          ],
                                        ),
                                      ),
                                      //const NbBottomWidgets(),
                                    ],
                                  ),
                                ),
                                Positioned(
                                  bottom: 10,
                                  right: 15,
                                  left: 15,
                                  child: cubit.isLoading ? const SizedBox() : InkWell(
                                    onTap: (){
                                      int price = BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.price ?? '0.0');
                                      int prevPrice = BaseMethod.currencyToPriceCalc(mPrice: cubit.productDetails.previousPrice ?? '0.0');
                                      log('currency To Price Calc: $price');
                                      if(cubit.productDetails.size!.isNotEmpty){
                                        if(cubit.selectedSize == null){
                                          showCustomSnackBar(context, 'Select first size');
                                        }else{
                                          if(cubit.productDetails.color!.isNotEmpty){
                                            if(cubit.selectedColor == null){
                                              showCustomSnackBar(context, 'Select first color');
                                            }else{
                                              if(int.parse(cubit.selectedSizeStock!) >= 1){
                                                CartCubit().addToCart(context,
                                                  cubit.productDetails,
                                                  quantity: 1,
                                                  price: cubit.selectedPrice!,
                                                  prevPrice: prevPrice,
                                                  sizeIndex: cubit.selectedSizeIndex!,
                                                  size: cubit.selectedSize!,
                                                  sizeCurrencyPrice: cubit.selectedCurrencySizePrice!,
                                                  color: cubit.selectedColor!,
                                                  stockQty: int.parse(cubit.selectedSizeStock!),);
                                              }else{
                                                showCustomSnackBar(context, 'Out of Stock');
                                              }
                                            }
                                          }else{
                                            if(int.parse(cubit.selectedSizeStock!) >= 1){
                                              CartCubit().addToCart(context,
                                                cubit.productDetails,
                                                quantity: 1,
                                                price: cubit.selectedPrice!,
                                                prevPrice: prevPrice,
                                                sizeIndex: cubit.selectedSizeIndex!,
                                                size: cubit.selectedSize!,
                                                sizeCurrencyPrice: cubit.selectedCurrencySizePrice!,
                                                stockQty: int.parse(cubit.selectedSizeStock!),);
                                            }else{
                                              showCustomSnackBar(context, 'Out of Stock');
                                            }

                                          }

                                        }
                                        log('this variation product');
                                      }else{
                                        if(cubit.productDetails.stock == null || (cubit.productDetails.stock ?? 0) >= 1){
                                          CartCubit().addToCart(context, cubit.productDetails, quantity: 1, price: price, prevPrice: prevPrice,stockQty: cubit.productDetails.stock);
                                        }else{
                                          showCustomSnackBar(context, 'Out of Stock');
                                        }

                                      }
                                    },
                                    child: Container(
                                      width: SizeConfig.screenWidth!,
                                      height: 50,
                                      decoration: BoxDecoration(
                                        color: kSecondaryColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                      /*     Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 45),
                                            child: SizedBox(
                                              width: 15,
                                              child: InkWell(
                                                onTap: (){
                                                  log('currency value: ${prefs.getDouble(currencyPrice)?? 0.0}');
                                                },
                                                child: const Icon(
                                                  Icons.favorite_border_outlined,
                                                  color: kWhiteColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                          kWidthBox5,
                                          Container(
                                            width: 1,
                                            height: 35,
                                            color: Theme.of(context).cardColor,
                                          ),*/
                                          const SizedBox(
                                            width: 15,
                                            child:  Icon(
                                              Icons.add,
                                              color: kWhiteColor,
                                            ),
                                          ),
                                          kWidthBox20,
                                          Center(
                                            child: RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: "Add To Bag",
                                                    style: Theme.of(context).textTheme.headline6!.copyWith(
                                                      color: kWhiteColor,
                                                      fontSize: 14,
                                                      height: 1.0,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
            );
          }),
    );
  }
}
