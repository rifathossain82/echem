import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import '../../brand/brand_screen.dart';
import '../../product_list/product_list_screen.dart';
import '../widgets/bundle_offer_card.dart';

class BundleOfferView extends StatelessWidget {
  final double radius;
  final int itemCount;
  final bool removeCard;

  const BundleOfferView({
    Key? key,
    this.radius = 10,
    this.itemCount = 4,
    this.removeCard = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
      ),
      child: MasonryGridView.count(
        crossAxisCount: 2,
        shrinkWrap: true,
        itemCount: itemCount,
        physics: const NeverScrollableScrollPhysics(),
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              if (index == 0 || index == 1) {
                if(!(radius == 0 || removeCard)){
                  Get.to(() => const BrandScreen());
                }
              } else {
                Get.to(() => const ProductListScreen());
              }
            },
            child: BundleOfferCard(
              last: index == 4 - 1 ? true : false,
              removeCard: removeCard,
              radius: radius,
              image: index == 0
                  ? 'https://cdn.picpng.com/sales/thirty-five-percent-off-discount-56707.png'
                  : index == 1
                      ? 'https://i.postimg.cc/BZwQw3jg/60-606750-special-offer-images-png-transparent-png-removebg-preview.png'
                      : index == 2
                          ? 'https://i.pinimg.com/originals/d7/48/ac/d748acec3dbca0f5310c97b139ebf7e5.png'
                          : 'https://i.pinimg.com/originals/cd/de/bf/cddebf8b161fb5c745e816dfc380f813.png',
            ),
          );
        },
      ),
    );
  }
}
