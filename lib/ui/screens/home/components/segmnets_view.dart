import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../../product_list/product_list_screen.dart';
import '../widgets/segments_card.dart';

class SegmentsView extends StatelessWidget {
  const SegmentsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5,),
      child: MasonryGridView.count(
        crossAxisCount: 2,
        shrinkWrap: true,
        itemCount: 4,
        physics: const NeverScrollableScrollPhysics(),
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        itemBuilder: (BuildContext context, int index) {
          return  InkWell(
            onTap: (){
              Get.to(() => const ProductListScreen());
            },
            child: SegmentsCard(last: index == 4 - 1 ? true : false,),
          );
        },
      ),
    );
  }
}