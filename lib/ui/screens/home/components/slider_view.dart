import 'package:carousel_slider/carousel_slider.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';
import '../../../../models/response/rp_slider.dart';
import '../../../widgets/custom_image.dart';

class SliderView extends StatelessWidget {

  final List<SliderList> sliderList;
  const SliderView({Key? key, required this.sliderList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
          aspectRatio: 2.0,
          viewportFraction: 1,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: false,
          autoPlayInterval: const Duration(seconds: 5),
          autoPlayAnimationDuration: const Duration(milliseconds: 1000),
          autoPlayCurve: Curves.easeInCubic,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          onPageChanged: (int index, reason) {}),
      items: sliderList
          .map(
            (item) => Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 2,
              ),
              child: CustomImage(
                baseUrl: kSliderImagePath,
                image: item.photo,
                fit: BoxFit.cover,
                radius: 0,
              ),
            ),
          )
          .toList(),
    );
  }
}
