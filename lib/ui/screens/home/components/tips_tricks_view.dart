import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../../../bloc/videos/videos_bloc.dart';
import '../../../../bloc/videos/videos_state.dart';
import '../../../../constants/style_data.dart';
import '../../../../resources/api/app_config.dart';
import '../widgets/tips_tricks_card.dart';

class TipsTricksView extends StatelessWidget {
  const TipsTricksView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideosBloc, VideosState>(
      builder: (context, state) {
        switch (state.status) {
          case VideoStatus.failure:
            return const SizedBox();
          case VideoStatus.success:
            if (state.posts.isEmpty) {
              return const SizedBox();
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                kHeightBox10,
                Center(
                  child: Text(
                    '$appName Tips & Tricks',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 5,
                  ),
                  child: MasonryGridView.count(
                    crossAxisCount: 2,
                    shrinkWrap: true,
                    itemCount: state.posts.length > 4 ? 4 : state.posts.length,
                    physics: const NeverScrollableScrollPhysics(),
                    mainAxisSpacing: 5,
                    crossAxisSpacing: 5,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: (){
                          BaseMethod().launchUrlToBrowser('https://www.youtube.com/watch?v=${state.posts[index].link ?? ''}');
                        },
                        child: TipsTricksCard(
                          video: state.posts[index],
                          last: index == (state.posts.length > 4 ? 4 : state.posts.length) - 1 ? true : false,
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          case VideoStatus.initial:
            return CustomShimmer.videoShimmer(context);
        }
      },
    );
  }
}
