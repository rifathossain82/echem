import 'package:echem/helper/navigator_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../../../bloc/only_category/only_category_bloc.dart';
import '../../../../bloc/only_category/only_category_state.dart';
import '../../../widgets/custom_shimmer.dart';
import '../widgets/top_category_card.dart';

class TopCatView extends StatelessWidget {
  const TopCatView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OnlyCategoryBloc, OnlyCategoryState>(
      builder: (context, state) {
        if (state is CategoryInitial) {
          return CustomShimmer.homeCategoryShimmer();
        } else if (state is CategoryLoading) {
          return CustomShimmer.homeCategoryShimmer();
        } else if (state is CategoryLoaded) {
          return Center(
            child: state.categoryModel.data!.isEmpty
                ? const SizedBox()
                : Container(
              padding: const EdgeInsets.only(top: 170),
                    margin: const EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: MasonryGridView.count(
                      crossAxisCount: 4,
                      shrinkWrap: true,
                      itemCount: state.categoryModel.data!.length > 8
                          ? 8
                          : state.categoryModel.data!.length,
                      physics: const NeverScrollableScrollPhysics(),
                      mainAxisSpacing: 5,
                      crossAxisSpacing: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            NavigatorHelper.openProductListScreen(context, categoryId: (state.categoryModel.data![index].id ?? 0).toString());
                          },
                          child: TopCategoryCard(
                            last: index == ((state.categoryModel.data!.length > 8 ? 8 : state.categoryModel.data!.length) - 1)
                                ? true
                                : false,
                            image: state.categoryModel.data![index].photo ?? '',
                            name: state.categoryModel.data![index].name ?? '',
                          ),
                        );
                      },
                    ),
                  ),
          );
        } else if (state is CategoryError) {
          return Center(
            child: Text(
              state.message ?? '',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          );
        } else {
          return Center(
            child: Text(
              'No data available',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          );
        }
      },
    );
  }
}
