import 'package:echem/bloc/best_selling/best_selling_event.dart';
import 'package:echem/bloc/best_selling/best_selling_state.dart';
import 'package:echem/bloc/big_save/big_save_bloc.dart';
import 'package:echem/bloc/big_save/big_save_event.dart';
import 'package:echem/bloc/big_save/big_save_state.dart';
import 'package:echem/bloc/home/home_bloc.dart';
import 'package:echem/bloc/home/home_event.dart';
import 'package:echem/bloc/home/home_state.dart';
import 'package:echem/bloc/new_arrival/new_arrival_bloc.dart';
import 'package:echem/bloc/new_arrival/new_arrival_event.dart';
import 'package:echem/bloc/new_arrival/new_arrival_state.dart';
import 'package:echem/bloc/videos/videos_bloc.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:echem/ui/screens/home/components/tips_tricks_view.dart';
import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/best_selling/best_selling_bloc.dart';
import '../../../bloc/videos/videos_event.dart';
import '../../../constants/colors_data.dart';
import '../../widgets/custom_image.dart';
import '../../widgets/grid_view_widgets.dart';
import '../../widgets/horizontal_list_view.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/more_widgets.dart';
import 'components/slider_view.dart';
import 'components/top_cat_view.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> _loadData(bool reload) async {
    BlocProvider.of<HomeBloc>(context).add(GetSliderList());
    BlocProvider.of<BestSellingBloc>(context).add(GetBestSellingList());
    BlocProvider.of<BigSaveBloc>(context).add(GetBigSaveList());
    BlocProvider.of<VideosBloc>(context).add(VideosFetched());
    BlocProvider.of<NewArrivalBloc>(context).add(GetNewArrivalList());
  }

  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();
    return RefreshIndicator(
        color: kPrimaryColor,
        backgroundColor: Theme.of(context).cardColor,
        displacement: 0,
        onRefresh: () async {
          _loadData(true);
        },
        child: BlocListener<HomeBloc, HomeState>(listener: (context, state) {
          if (state is SliderError) {
            showCustomSnackBar(context, state.message ?? '');
          }
        }, child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeInitial) {
              return CustomShimmer.homeScreenShimmer(context);
            } else if (state is SliderLoading) {
              return CustomShimmer.homeScreenShimmer(context);
            } else if (state is SliderLoaded) {
              return Center(
                child: state.sliderModel.errors != null
                    ? Text(
                        state.sliderModel.errors ?? '',
                        style: Theme.of(context).textTheme.subtitle1,
                      )
                    : SingleChildScrollView(
                        controller: scrollController,
                        physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics(),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                NavigatorHelper.openSearchScreen(context);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: InputFormWidget(
                                  prefixIcon: Icon(
                                    Icons.search,
                                    color: Theme.of(context).disabledColor,
                                  ),
                                  absorbing: true,
                                  hintText: 'Search For Products',
                                ),
                              ),
                            ),
                            Stack(
                              clipBehavior: Clip.none,
                              children: [
                                SliderView(
                                  sliderList: state.sliderModel.data!,
                                ),
                                const TopCatView(),
                              ],
                            ),
                            kHeightBox10,
                            BlocBuilder<BestSellingBloc, BestSellingState>(
                              builder: (context, state) {
                                if (state is BestSellingInitial) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      color: kPrimaryColor,
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 5,
                                            vertical: 10,
                                          ),
                                          child: Row(
                                            children: [
                                              Text(
                                                'Best Seller',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2!
                                                    .copyWith(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                              ),
                                              const Spacer(),
                                              MoreWidgets(onTap: () {

                                              }),
                                            ],
                                          ),
                                        ),
                                        CustomShimmer.horizontalProductShimmer(),
                                      ],
                                    ),
                                  );
                                } else if (state is BestSellingLoading) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      color: kPrimaryColor,
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 5,
                                            vertical: 10,
                                          ),
                                          child: Row(
                                            children: [
                                              Text(
                                                'Best Seller',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2!
                                                    .copyWith(
                                                      color: kWhiteColor,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                              ),
                                              const Spacer(),
                                              MoreWidgets(onTap: () {
                                              }),
                                            ],
                                          ),
                                        ),
                                        CustomShimmer.horizontalProductShimmer(),
                                      ],
                                    ),
                                  );
                                } else if (state is BestSellingLoaded) {
                                  return Center(
                                      child: state.bestSelling.isEmpty
                                          ? const SizedBox()
                                          : Container(
                                              decoration: BoxDecoration(
                                                color: kPrimaryColor,
                                                borderRadius: BorderRadius.circular(0),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      horizontal: 5,
                                                      vertical: 10,
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          'Best Seller',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .subtitle2!
                                                                  .copyWith(
                                                                    color:
                                                                        kWhiteColor,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                  ),
                                                        ),
                                                        const Spacer(),
                                                        MoreWidgets(onTap: () {
                                                          NavigatorHelper.openProductListScreen(context, type: kProductFilterTypeBestSelling);
                                                        }),
                                                      ],
                                                    ),
                                                  ),
                                                  HorizontalListView(
                                                    productList: state.bestSelling,
                                                  )
                                                ],
                                              ),
                                            ));
                                } else {
                                  return const SizedBox();
                                }
                              },
                            ),
                            kHeightBox10,
                            SizedBox(
                              child: state.bannerData.data!.isNotEmpty
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 5,
                                      ),
                                      child: InkWell(
                                        onTap: () {
                                          BaseMethod().launchUrlToBrowser(
                                              state.bannerData.data![0].link ??
                                                  '');
                                        },
                                        child: CustomImage(
                                          height: 200,
                                          width: SizeConfig.screenWidth,
                                          baseUrl: kBannerImagePath,
                                          image: state.bannerData.data![0].photo ?? '',
                                          fit: BoxFit.fitWidth,
                                          radius: 5,
                                        ),
                                      ),
                                    )
                                  : const SizedBox(),
                            ),
                            kHeightBox10,
                            BlocBuilder<BigSaveBloc, BigSaveState>(
                              builder: (context, state) {
                                if (state is BigSaveInitial) {
                                  return CustomShimmer
                                      .gridProductWithTitleShimmer(context,
                                          title: 'Big Save');
                                } else if (state is BigSaveLoading) {
                                  return CustomShimmer
                                      .gridProductWithTitleShimmer(context,
                                          title: 'Big Save');
                                } else if (state is BigSaveLoaded) {
                                  return Center(
                                      child: state.productList.isEmpty
                                          ? const SizedBox()
                                          : Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                kHeightBox10,
                                                Center(
                                                  child: Text(
                                                    'Big Save',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2,
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                kHeightBox10,
                                                GridViewList(
                                                  productList:
                                                      state.productList,
                                                  isHome: true,
                                                ),
                                              ],
                                            ));
                                } else {
                                  return const SizedBox();
                                }
                              },
                            ),
                            kHeightBox10,
                            SizedBox(
                              child: state.bannerData.data!.isNotEmpty &&
                                      state.bannerData.data!.length > 1
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 5,
                                      ),
                                      child: InkWell(
                                        onTap: () {
                                          BaseMethod().launchUrlToBrowser(
                                              state.bannerData.data![1].link ??
                                                  '');
                                        },
                                        child: CustomImage(
                                          height: 200,
                                          width: SizeConfig.screenWidth,
                                          baseUrl: kBannerImagePath,
                                          image: state.bannerData.data![1].photo ?? '',
                                          fit: BoxFit.fitWidth,
                                          radius: 5,
                                        ),
                                      ),
                                    )
                                  : const SizedBox(),
                            ),
                            const TipsTricksView(),
                            kHeightBox10,
                            SizedBox(
                              child: state.bannerData.data!.isNotEmpty &&
                                      state.bannerData.data!.length > 2
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 5,
                                      ),
                                      child: InkWell(
                                        onTap: () {
                                          BaseMethod().launchUrlToBrowser(
                                              state.bannerData.data![2].link ??
                                                  '');
                                        },
                                        child: CustomImage(
                                          height: 200,
                                          width: SizeConfig.screenWidth,
                                          baseUrl: kBannerImagePath,
                                          image: state.bannerData.data![2].photo,
                                          fit: BoxFit.fitWidth,
                                          radius: 5,
                                        ),
                                      ),
                                    )
                                  : const SizedBox(),
                            ),
                            BlocBuilder<NewArrivalBloc, NewArrivalState>(
                              builder: (context, state) {
                                if (state is NewArrivalInitial) {
                                  return CustomShimmer.gridProductWithTitleShimmer(context, title: 'New Arrival', crossCount: 2);
                                } else if (state is NewArrivalLoading) {
                                  return CustomShimmer.gridProductWithTitleShimmer(context,
                                          title: 'New Arrival', crossCount: 2);
                                } else if (state is NewArrivalLoaded) {
                                  return Center(
                                      child: state.productList.isEmpty
                                          ? const SizedBox()
                                          : Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                kHeightBox10,
                                                Center(
                                                  child: Text(
                                                    'New Arrival',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2,
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                kHeightBox10,
                                                GridViewList(
                                                  crossCount: 2,
                                                  productList:
                                                      state.productList,
                                                ),
                                              ],
                                            ));
                                } else {
                                  return const SizedBox();
                                }
                              },
                            ),
                          //  const NbBottomWidgets(),
                            /*kHeightBox10,
                Center(
                  child: Text(
                    'Clearance Sale',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 5,
                  ),
                  child: InkWell(
                    onTap: () {
                      Get.to(() => const ProductListScreen());
                    },
                    child: CustomImage(
                      height: SizeConfig.screenWidth! / 2,
                      width: SizeConfig.screenWidth,
                      image:
                          'https://www.beyoung.in/api/cache/catalog/category-banner/mobile/women_clerance_sale_category_banner_mobile_view_767x430.jpg',
                      radius: 5,
                    ),
                  ),
                ),
                kHeightBox10,
                Center(
                  child: Text(
                    'Miss করলেই লস!',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
               */ /* const GridViewList(),*/ /*
                kHeightBox10,
                Center(
                  child: Text(
                    'Segments You Can\'t Miss',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
                const SegmentsView(),
                kHeightBox10,
                Center(
                  child: Text(
                    'Shop By Concern',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
                const ConcernView(),
                kHeightBox10,
                Center(
                  child: Text(
                    '$appName Tips & Tricks',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,*/

                            /* kHeightBox10,
                Center(
                  child: Text(
                    'Just For You',
                    style: Theme.of(context).textTheme.subtitle2,
                    textAlign: TextAlign.center,
                  ),
                ),
                kHeightBox10,
              */ /*  const GridViewList(
                  crossCount: 2,
                  padding: 10,
                ),*/ /*
                kHeightBox10,
                Container(
                  decoration: BoxDecoration(
                    color: kSecondaryColor,
                    borderRadius: BorderRadius.circular(0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 10,
                        ),
                        child: Row(
                          children: [
                            Text(
                              'Personal Care',
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2!
                                  .copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                            const Spacer(),
                            MoreWidgets(onTap: () {
                              Get.to(() => const ProductListScreen());
                            }),
                          ],
                        ),
                      ),
                   */ /*   const HorizontalListView(
                        productList: [],
                      ),*/ /*
                    ],
                  ),
                ),
                const NbBottomWidgets(),*/
                          ],
                        ),
                      ),
              );
            } else if (state is SliderError) {
              return Center(
                child: Text(
                  state.message ?? '',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              );
            } else {
              return Center(
                child: Text(
                  'No data available',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              );
            }
          },
        )));
  }
}
