import 'package:echem/constants/size_config.dart';
import 'package:flutter/material.dart';

import '../../../../constants/style_data.dart';
import '../../../widgets/custom_image.dart';

class BundleOfferCard extends StatelessWidget {
  final bool last;
  final String? image;
  final double radius;
  final bool removeCard;

  const BundleOfferCard({
    Key? key,
    this.radius = 10,
    this.removeCard = false,
    this.last = false,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:   BoxDecoration(
          gradient: const LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0xFFF6D4F7),
              Colors.blue,
              Colors.red,
              Colors.green,
              Colors.yellow,
              Color(0xFFF6D4F7),
            ],
          ),
        borderRadius: BorderRadius.all(Radius.circular(radius)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: removeCard ? 0 : 1,
          color: removeCard ? Colors.transparent : const Color(0xFFF6D4F7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              kHeightBox5,
               CustomImage(
                height: SizeConfig.screenWidth! / 2.8,
                image: image,
                radius: 0,
              ),
              kHeightBox5,
            ],
          ),
        ),
      ),
    );
  }
}
