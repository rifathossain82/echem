import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/ui/widgets/custom_image.dart';
import 'package:flutter/material.dart';
import '../../../../constants/style_data.dart';

class ConcernCard extends StatelessWidget {
  final bool last;

  const ConcernCard({
    Key? key,
    this.last = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFFF6D4F7),
            kSuccessColor,
            Colors.red,
            kSecondaryColor,
            Color(0xFFF6D4F7),
            kPrimaryColor,
            Color(0xFFF6D4F7),
          ],
        ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0),
        child: Column(
          children: [
            kHeightBox10,
            const Expanded(child: Padding(
              padding: EdgeInsets.all(8.0),
              child: SizedBox(
                width: 100,
                height: 100,
                child: CustomImage(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(35),
                    bottomRight: Radius.circular(20),
                    topRight: Radius.circular(35),
                  ),
                  image: 'https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2022/02/eyemakeup-1645030692.jpg',
                ),
              ),
            ),),
            kHeightBox10,
            Container(
              color: kSecondaryColor,
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    Text(
                      'Acne'.toUpperCase(),
                      style: Theme.of(context).textTheme.headline3?.copyWith(
                        color: kWhiteColor,
                      ),
                    ),
                    kHeightBox5,
                    Text(
                      'Treatment'.toUpperCase(),
                      style: Theme.of(context).textTheme.bodyText2?.copyWith(
                        color: kWhiteColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

