import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:flutter/material.dart';

import '../../../../constants/style_data.dart';
import '../../../widgets/custom_image.dart';

class SegmentsCard extends StatelessWidget {
  final bool last;

  const SegmentsCard({
    Key? key,
    this.last = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFFF6D4F7),
            kSuccessColor,
            Colors.red,
            kSecondaryColor,
            Color(0xFFF6D4F7),
            kPrimaryColor,
            Color(0xFFF6D4F7),
          ],
        ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            kHeightBox5,
            Text(
              'Makeup'.toUpperCase(),
              style: Theme.of(context).textTheme.subtitle1?.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
            kHeightBox10,
            CustomImage(
              height: SizeConfig.screenWidth! / 2.8,
              image: 'https://i.postimg.cc/x8ZStLP2/Skin-Care-removebg-preview.png',
              radius: 0,
            ),
          ],
        ),
      ),
    );
  }
}
