import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/models/response/rp_video_data.dart';
import 'package:flutter/material.dart';
import '../../../../constants/style_data.dart';
import '../../../widgets/custom_image.dart';

class TipsTricksCard extends StatelessWidget {
  final bool last;
  final VideoList video;

  const TipsTricksCard({
    Key? key,
    this.last = false,
    required this.video,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        kHeightBox10,
        SizedBox(
          height: SizeConfig.screenWidth! / 2.8,
          width: SizeConfig.screenWidth,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(
                      width: 3,
                      color: kSecondaryColor,
                    ),
                    borderRadius: BorderRadius.circular(10)),
                child: CustomImage(
                  height: 120,
                  baseUrl: '',
                  image: video.thumbnailUrl,
                  radius: 5,
                ),
              ),
              Align(
                child: SizedBox(
                  width: 35,
                  height: 35,
                  child: Container(
                    padding: const EdgeInsets.all(2),
                    decoration: const BoxDecoration(
                        color: kPrimaryColor, shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        Icons.play_arrow,
                        color: Theme.of(context).cardColor,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        kHeightBox5,
        Text(
          video.title ?? '',
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.headline6,
          textAlign: TextAlign.start,
        ),
      ],
    );
  }
}
