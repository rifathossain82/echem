import 'package:flutter/material.dart';

import '../../../widgets/custom_image.dart';

class TopBrandCard extends StatelessWidget {
  final bool lastLeft;

  const TopBrandCard({
    Key? key,
    this.lastLeft = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CustomImage(
          image: 'https://echem.com.bd/assets/images/products/iCRcpxvebqhY.jpg',
          radius: 0,
          borderRadius: lastLeft ? const BorderRadius.only(
            topRight: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ) : const BorderRadius.only(
            topLeft: Radius.circular(10),
            bottomLeft: Radius.circular(10),
          ),
        ),
      ],
    );
  }
}
