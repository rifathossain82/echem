import 'package:echem/constants/colors_data.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';

import '../../../../constants/style_data.dart';
import '../../../widgets/custom_image.dart';

class TopCategoryCard extends StatelessWidget {
  final bool last;
  final String image;
  final String name;

  const TopCategoryCard({
    Key? key,
    this.last = false,
    required this.image,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: last ?  kSecondaryColor : kPrimaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          kHeightBox15,
          CustomImage(
            height: 40,
            width: 40,
            image: image,
            baseUrl: kCategoryImagePath,
            radius: 0,
            //imageColor: kImageColor,
          ),
          kHeightBox15,
          Text(
            name,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.subtitle2?.copyWith(
              color: last ? kWhiteColor : kWhiteColor,
                ),
            textAlign: TextAlign.center,
          ),
          kHeightBox5,
        ],
      ),
    );
  }
}
