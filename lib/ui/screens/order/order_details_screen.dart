import 'package:echem/bloc/order/order_bloc.dart';
import 'package:echem/bloc/order/order_state.dart';
import 'package:echem/bloc/order/orders_event.dart';
import 'package:echem/constants/images.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/resources/api_provider/order_provider.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:echem/utils/date_converter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../constants/colors_data.dart';
import '../../../constants/size_config.dart';
import '../../../helper/navigator_helper.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/main_app_bar.dart';
import 'widgets/order_product_card.dart';

class OrderDetailsScreen extends StatelessWidget {
  final int orderId;

  const OrderDetailsScreen(
      {Key? key, required this.orderId,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: false,
            ),
            Expanded(
              child: BlocProvider(
                  create: (_) => OrderBloc(orderId: orderId,)..add(OrderDetailsFetched()),
                  child: BlocBuilder<OrderBloc, OrderState>(
                    builder: (context, state) {
                      int total1 = state.orderDetails?.data?.order?.shippingCost ?? 0;
                      int total2 = state.orderDetails?.data?.order?.payAmount ?? 0;
                      int total = total2 + total1;
                      switch (state.status) {
                        case OrderStatus.failure:
                          return const SizedBox();
                        case OrderStatus.success:
                          if (state.orderDetails == null) {
                            return const SizedBox();
                          }
                          return SingleChildScrollView(
                            physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics(),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 10,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  kHeightBox10,
                                  Container(
                                    width: SizeConfig.screenWidth,
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 15,
                                    ),
                                    decoration: BoxDecoration(
                                        color: kSecondaryColor,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              'ORDER ID:  #${state.orderDetails?.data?.order?.orderNumber ?? ''}',
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .headline6
                                                  ?.copyWith(
                                                  color: kWhiteColor),
                                            ),
                                          ],
                                        ),
                                        kHeightBox5,
                                        Text(
                                          DateConverter.dateTimeStringToDateTime(state.orderDetails?.data?.order?.createdAt ?? ''),
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .bodyText2
                                              ?.copyWith(
                                            color: Theme
                                                .of(context)
                                                .disabledColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  kHeightBox10,
                                  Container(
                                    width: SizeConfig.screenWidth,
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 15,
                                    ),
                                    decoration: BoxDecoration(
                                        color: Theme
                                            .of(context)
                                            .disabledColor,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 50,
                                          height: 50,
                                          padding: const EdgeInsets.all(5),
                                          decoration: const BoxDecoration(
                                            color: Color(0xFFCAE9DA),
                                            shape: BoxShape.circle,
                                          ),
                                          child: SvgPicture.asset(
                                            Images.cartProcessing,
                                            color: const Color(0xFF43BE7D),
                                          ),
                                        ),
                                        kWidthBox10,
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment
                                                .start,
                                            children: [
                                              Text(
                                                'Status',
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .subtitle1
                                                    ?.copyWith(
                                                  color: kBlackColor2,
                                                  fontSize: 17,
                                                ),
                                              ),
                                              kHeightBox5,
                                              Text(
                                                (state.orderDetails?.data?.order?.status ?? '').toUpperCase(),
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .subtitle1
                                                    ?.copyWith(
                                                  color: kPrimaryColor,
                                                  fontSize: 15,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        kWidthBox10,
                                        if((state.orderDetails?.data?.order?.status ?? '') == 'pending')
                                          CustomButton(
                                            title: 'Cancel Order',
                                            imageURL: Images.cart,
                                            btnColor: const Color(0xFF6939DD),
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            width: 120,
                                            height: 28,
                                            radius: 5,
                                            textSize: 12,
                                            onPress: () {
                                              showCancelDialog(context);
                                            },
                                          ),
                                      ],
                                    ),
                                  ),
                                  kHeightBox10,
                                  Text(
                                    'Order Overview',
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .subtitle1
                                        ?.copyWith(
                                      fontSize: 17,
                                    ),
                                  ),
                                  kHeightBox10,
                                  CustomButton(
                                    title: '${state.orderDetails?.data?.order?.shippingUpazila ?? ''}, ${state.orderDetails?.data?.order?.shippingCity ?? ''} - ${state.orderDetails?.data?.order?.shippingZip ?? ''}',
                                    imageURL: Images.location,
                                    btnColor: Theme.of(context).disabledColor,
                                    width: SizeConfig.screenWidth,
                                    textColor: kBlackColor2,
                                    height: 35,
                                    radius: 5,
                                    textSize: 12,
                                  ),
                                  kHeightBox5,
                                  CustomButton(
                                    title: '+880 ${state.orderDetails?.data?.order?.shippingPhone ?? ''}',
                                    imageURL: Images.call,
                                    btnColor: Theme
                                        .of(context)
                                        .disabledColor,
                                    width: SizeConfig.screenWidth,
                                    textColor: kBlackColor2,
                                    height: 35,
                                    radius: 5,
                                    textSize: 12,
                                  ),
                                  kHeightBox5,
                                  CustomButton(
                                    title: 'SubTotal - $kCurrency ${state.orderDetails?.data?.order?.payAmount ?? ''}',
                                    imageURL: Images.call,
                                    btnColor: Theme
                                        .of(context)
                                        .disabledColor,
                                    width: SizeConfig.screenWidth,
                                    textColor: kBlackColor2,
                                    height: 35,
                                    radius: 5,
                                    textSize: 12,
                                  ),
                                  kHeightBox5,
                                  CustomButton(
                                    title: 'Shipping Cost - $kCurrency ${state.orderDetails?.data?.order?.shippingCost ?? 0}.00',
                                    imageURL: Images.cartProcessing,
                                    btnColor: Theme.of(context).disabledColor,
                                    width: SizeConfig.screenWidth,
                                    textColor: kBlackColor2,
                                    height: 35,
                                    radius: 5,
                                    textSize: 12,
                                  ),
                                  kHeightBox5,
                                  CustomButton(
                                    title: 'Total - $kCurrency $total.00',
                                    imageURL: Images.card,
                                    btnColor: Theme.of(context).disabledColor,
                                    width: SizeConfig.screenWidth,
                                    textColor: kBlackColor2,
                                    height: 35,
                                    radius: 5,
                                    textSize: 12,
                                  ),

                                  SizedBox(
                                    child: state.orderDetails!.data!.products!.isEmpty ? const SizedBox() : Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        kHeightBox10,
                                        Text(
                                          'Order Products',
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .subtitle1
                                              ?.copyWith(
                                            fontSize: 17,
                                          ),
                                        ),
                                        ListView.builder(
                                          physics: const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount: state.orderDetails?.data?.products?.length,
                                          padding: EdgeInsets.zero,
                                          itemBuilder: (context, int index) {
                                            return  OrderProductCard(product: state.orderDetails!.data!.products![index],);
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                  kHeightBox20,
                                ],
                              ),
                            ),
                          );
                        case OrderStatus.initial:
                          return CustomShimmer.orderDetailsShimmer(context);
                      }
                    },
                  ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  showCancelDialog(BuildContext context,) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          alignment: Alignment.center,
          backgroundColor: Theme
              .of(context)
              .cardColor,
          //const Color(0xFF1E243C),,
          content: Container(
            padding: EdgeInsets.zero,
            width: SizeConfig.screenWidth! - 20,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: SizeConfig.screenWidth,
                  height: 40,
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: const BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'Cancel Order',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline6
                          ?.copyWith(color: kWhiteColor),
                    ),
                  ),
                ),
                kHeightBox40,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    kHeightBox15,
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15),
                      child: Text(
                          'Please let us know your reason to cancel order. We will try your to improve our service based on hour feedback.',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                    /* kHeightBox30,
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: cancelTypeList.length,
                                  itemBuilder: (context, int index) {
                                    return InkWell(
                                      onTap: () {},
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 15,
                                              vertical: 10,
                                            ),
                                            child: Row(
                                              children: [
                                                Container(
                                                  padding: const EdgeInsets
                                                      .only(
                                                      right: 10),
                                                  child: Icon(
                                                    index == 1
                                                        ? Icons
                                                        .check_circle_rounded
                                                        : Icons.circle_outlined,
                                                    color: index == 1
                                                        ? kErrorColor
                                                        : Theme
                                                        .of(context)
                                                        .textTheme
                                                        .headline5
                                                        ?.color,
                                                    size: 20,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    cancelTypeList[index],
                                                    maxLines: 1,
                                                    style: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .headline5
                                                        ?.copyWith(
                                                      color: index == 1
                                                          ? kErrorColor
                                                          : Theme
                                                          .of(context)
                                                          .textTheme
                                                          .headline5
                                                          ?.color,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),*/
                  ],
                ),
                kHeightBox40,
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: CustomButton(
                    width: SizeConfig.screenWidth,
                    btnColor: kPrimaryColor,
                    title: 'Cancel My Order',
                    textColor: Colors.white,
                    height: 32,
                    onPress: () async {
                      var results = await OrderProvider().orderCancelRequest(
                        context,
                        orderId: orderId.toString(),
                      );
                      if (results.errors != null) {
                        Navigator.of(context).pop();
                        showCustomSnackBar(context, results.errors ?? '');
                      } else {
                        NavigatorHelper.openDashboardScreen(context);
                        showCustomSnackBar(context, 'Your Order Canceled', isError: false);
                      }
                    },
                  ),
                ),
                kHeightBox5,
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: CustomButton(
                    width: SizeConfig.screenWidth,
                    btnColor: Theme
                        .of(context)
                        .disabledColor,
                    title: 'Don\'t Cancel Order',
                    textColor: kBlackColor2,
                    height: 32,
                    onPress: () async {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                kHeightBox10,
              ],
            ),
          ),
        );
      },
    );
  }
}
