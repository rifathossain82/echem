import 'package:echem/bloc/order/order_bloc.dart';
import 'package:echem/bloc/order/orders_event.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';
import 'widgets/order_list_view.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: false,
            ),
            Expanded(
              child : BlocProvider(
                create: (_) => OrderBloc()..add(OrderFetched()),
                child: const OrderListView(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
