import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/models/response/rp_order_list.dart';
import 'package:echem/utils/date_converter.dart';
import 'package:flutter/material.dart';

import '../../../../constants/size_config.dart';
import '../../../../constants/style_data.dart';

class OrderCard extends StatelessWidget {
  final OrderList order;
  final bool last;
  const OrderCard({
    Key? key,
    required this.order,
    this.last = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        kHeightBox10,
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          width: SizeConfig.screenWidth,
          child: Card(
            color: Theme.of(context).cardColor,
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 1.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: SizeConfig.screenWidth,
                  height: 45,
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: const BoxDecoration(
                    color: kSecondaryColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          'ORDER ID',
                          style: Theme.of(context)
                              .textTheme
                              .headline6
                              ?.copyWith(color: kWhiteColor),
                        ),
                      ),
                      kWidthBox10,
                      Text(
                        '#${order.orderNumber ?? ''}',
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            ?.copyWith(color: kWhiteColor),
                      ),
                    ],
                  ),
                ),
                kHeightBox10,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Order Date: ',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            DateConverter.stringToLocalDateOnly(order.createdAt ?? ''),
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ],
                      ),
                      kHeightBox10,
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Status: ',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            (order.status ?? '').toString(),
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ],
                      ),
                      kHeightBox10,
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Payment Method: ',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            order.method ?? '',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ],
                      ),
                      kHeightBox10,
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Total: ',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          kWidthBox10,
                          Text(
                            kCurrency + ' ${order.payAmount ?? 0}',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ],
                      ),
                      kHeightBox10,
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        if(last)
          kHeightBox10,
      ],
    );
  }
}
