import 'package:echem/bloc/order/order_bloc.dart';
import 'package:echem/bloc/order/orders_event.dart';
import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../bloc/order/order_state.dart';
import '../../../../helper/navigator_helper.dart';
import '../../../widgets/custom_loader.dart';
import 'order_card.dart';

class OrderListView extends StatefulWidget {
  const OrderListView({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderListView> createState() => OrderListState();
}

class OrderListState extends State<OrderListView> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      builder: (context, state) {
        switch (state.status) {
          case OrderStatus.failure:
            return const Center(child: Text('failed to fetch posts'));
          case OrderStatus.success:
            if (state.posts.isEmpty) {
              return const Center(child: Text('No data available'));
            }
            return ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              controller: _scrollController,
              itemCount: state.hasReachedMax
                  ? state.posts.length
                  : state.posts.length + 1,
              itemBuilder: (BuildContext context, int index) {
                return index >= state.posts.length
                    ? const CustomBottomLoader()
                    : InkWell(
                        onTap: () {
                          NavigatorHelper.openOrderDetailsScreen(context, orderId: state.posts[index].id ?? 0);
                        },
                        child: OrderCard(order: state.posts[index], last: index == state.posts.length - 1 ? true : false,),
                      );
              },
            );
          case OrderStatus.initial:
            return CustomShimmer.buildOrderListShimmer();
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) context.read<OrderBloc>().add(OrderFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
