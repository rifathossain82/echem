import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:echem/ui/widgets/custom_image.dart';
import 'package:flutter/material.dart';
import '../../../../constants/size_config.dart';
import '../../../../constants/style_data.dart';
import '../../../../models/response/rp_order_details.dart';
import '../../../widgets/custom_button.dart';

class OrderProductCard extends StatelessWidget {
  final Products product;
  const OrderProductCard({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        kHeightBox10,
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 0.0,
          ),
          width: SizeConfig.screenWidth,
          child: Card(
            color: Theme.of(context).cardColor,
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 1.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Row(
                children: [
                   InkWell(
                     onTap: () => NavigatorHelper.openProductDetailsScreen(context, productId: product.id ?? 0),
                     child: CustomImage(
                      width: 80,
                      baseUrl: kProductImagePath,
                      image: product.photo,
                      radius: 5,
                  ),
                   ),
                  kWidthBox10,
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                product.name ?? '',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                          ],
                        ),
                        kHeightBox20,
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'Price: ',
                                          style: Theme.of(context).textTheme.subtitle1,
                                        ),
                                        TextSpan(
                                          text: kCurrency + '${product.price ?? 0}',
                                          style: Theme.of(context).textTheme.subtitle2?.copyWith(
                                            color: kPrimaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  kHeightBox5,
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'Subtotal: ',
                                          style: Theme.of(context).textTheme.subtitle1,
                                        ),
                                        TextSpan(
                                          text: kCurrency + '${product.totalPrice ?? 0}',
                                          style: Theme.of(context).textTheme.subtitle2?.copyWith(
                                            color: kPrimaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            kWidthBox10,
                            CustomButton(
                              title: 'x ${product.quantity ?? 0}',
                              height: 28,
                              width: 50,
                              radius: 5,
                              btnColor: Theme.of(context).cardColor,
                              textColor: kPrimaryColor,
                              textSize: 8,
                              border: 1,
                              borderColor: kPrimaryColor,
                            ),
                            kWidthBox10,
                          ],
                        ),
                        kHeightBox10,
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
