import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../bloc/filter/filter_cubit.dart';
import '../../../bloc/filter/filter_state.dart';
import '../../../constants/images.dart';
import '../../../constants/size_config.dart';
import '../../../models/response/rp_categories.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/search_infinity_grid.dart';
import 'widgets/filter_sub_card.dart';
var scaffoldKey = GlobalKey<ScaffoldState>();

class ProductFilterScreen extends StatelessWidget {
  final List<Subs> subCategoryList;
  final String? categoryId, subCatId, childCatId;
  const ProductFilterScreen({
    Key? key,
    this.categoryId,
    this.subCatId,
    this.childCatId,
    required this.subCategoryList
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FilterCubit, FilterState>(
        listener: (context, state) {},
        builder: (context, state) {
          FilterCubit cubit = FilterCubit.get(context);
          return Scaffold(
            key: scaffoldKey,
            bottomNavigationBar: const BottomNav(),
            body: SafeArea(
              child: Column(
                children: [
                  const MainCustomAppBar(
                    backButton: true,
                  ),
                  kHeightBox5,
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: CustomButton(
                            imageURL: Images.sort,
                            btnColor: kPrimaryColor,
                            title: cubit.selectedFilterSortName,
                            textColor: Colors.white,
                            textSize: 12,
                            onPress: () async {
                              showSortDialog(context);
                            },
                          ),
                        ),
                        kWidthBox10,
                        Expanded(
                          flex: 2,
                          child: CustomButton(
                            imageURL: Images.filter,
                            btnColor: kPrimaryColor,
                            title: 'Apply Filter',
                            textSize: 12,
                            textColor: Colors.white,
                            onPress: () {
                              showFilterDialog(context, subCategoryList);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: SearchInfinityGrid(
                    isFilter: true,
                    crossCount: 3,
                    paddingTobBottom: 5,
                    categoryId: categoryId,
                    subCatId: cubit.selectedFilterSubId != null ? (cubit.selectedFilterSubId ?? 0).toString() : subCatId,
                    childCatId: childCatId,
                    type: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ?  kProductFilterTypeNewArrival : null,
                    sorting: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ? null :  cubit.selectedFilterSort.isNotEmpty ? cubit.selectedFilterSort : null,
                    minPrice: cubit.selectedMinPrice.isNotEmpty ? cubit.selectedMinPrice : null,
                    maxPrice: cubit.selectedMaxPrice.isNotEmpty ? cubit.selectedMaxPrice : null,
                  ),),
                ],
              ),
            ),
          );
        },);
  }

  showSortDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          insetPadding: const EdgeInsets.only(top: 80),
          alignment: Alignment.topCenter,
          backgroundColor: kSecondaryColor,
          //const Color(0xFF1E243C),,
          content: SizedBox(
            width: SizeConfig.screenWidth! - 200,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                BlocBuilder<FilterCubit, FilterState>(
                    builder: (context, state) {
                      FilterCubit cubit = FilterCubit.get(context);
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: cubit.filterSortList.length,
                        itemBuilder: (context, int index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                              cubit.updateFilterSort(
                                cubit.filterSortList[index]['sort'] ?? '',
                                cubit.filterSortList[index]['name'] ?? '',
                              );
                              context.read<FilterCubit>().getProductList(true,
                                categoryId: categoryId,
                                subCatId: subCatId,
                                childCatId: childCatId,
                                type: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ?  kProductFilterTypeNewArrival : null,
                                sorting: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ? null :  cubit.selectedFilterSort.isNotEmpty ? cubit.selectedFilterSort : null,
                              );
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                kHeightBox10,
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: Text(
                                    cubit.filterSortList[index]['name'] ?? '',
                                    maxLines: 1,
                                    style: kRegularText2.copyWith(
                                      color: kWhiteColor,
                                    ),
                                  ),
                                ),
                                kHeightBox10,
                                SizedBox(
                                  child: index ==  cubit.filterSortList.length - 1
                                      ? const SizedBox(
                                    height: 15,
                                  )
                                      : Container(
                                    height: .5,
                                    color: Theme.of(context).disabledColor,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },),
                kHeightBox5,
              ],
            ),
          ),
        );
      },
    );
  }

  showFilterDialog(BuildContext context, List<Subs> subCategoryList) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          insetPadding: const EdgeInsets.only(top: 70),
          alignment: Alignment.topCenter,
          backgroundColor: kSecondaryColor,
          //const Color(0xFF1E243C),,
          content: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            width: SizeConfig.screenWidth! - 20,
            child: BlocBuilder<FilterCubit, FilterState>(
              builder: (context, state) {
                FilterCubit cubit = FilterCubit.get(context);
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 25,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: CustomButton(
                              btnColor: kPrimaryColor,
                              title: 'Cancel',
                              textColor: Colors.white,
                              height: 32,
                              onPress: () async {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          kWidthBox10,
                          Expanded(
                            flex: 3,
                            child: CustomButton(
                              btnColor: kPrimaryColor,
                              title: 'Apply Filter',
                              height: 32,
                              textColor: Colors.white,
                              onPress: () {
                                Navigator.of(context).pop();
                                context.read<FilterCubit>().getProductList(true, startIndex: 0,
                                  categoryId: categoryId,
                                  subCatId: cubit.selectedFilterSubId != null ? (cubit.selectedFilterSubId ?? 0).toString() : subCatId,
                                  childCatId: childCatId,
                                  type: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ?  kProductFilterTypeNewArrival : null,
                                  sorting: cubit.selectedFilterSort.isNotEmpty && cubit.selectedFilterSort == 'new_arrival' ? null :  cubit.selectedFilterSort.isNotEmpty ? cubit.selectedFilterSort : null,
                                  minPrice: cubit.selectedMinPrice.isNotEmpty ? cubit.selectedMinPrice : null,
                                  maxPrice: cubit.selectedMaxPrice.isNotEmpty ? cubit.selectedMaxPrice : null,
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    kHeightBox10,
                    SizedBox(
                      height: SizeConfig.screenWidth,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: const ScrollPhysics(),
                              itemCount: cubit.filter2TypeList.length,
                              itemBuilder: (context, int index) {
                                return InkWell(
                                  onTap: () {
                                    cubit.updateFilterType(index);
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      kHeightBox10,
                                      Text(
                                        cubit.filter2TypeList[index],
                                        maxLines: 1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6
                                            ?.copyWith(
                                          color: index ==
                                              cubit.selectedFilterTypeIndex
                                              ? kErrorColor
                                              : kWhiteColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                          kWidthBox5,
                          Container(
                            width: .5,
                            color: Theme.of(context).disabledColor,
                          ),
                          kWidthBox5,
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: EdgeInsets.only(top: cubit.selectedFilterTypeIndex == 1 ? 30 : 0),
                              child: cubit.selectedFilterTypeIndex == 0
                                  ? subCategoryList.isEmpty ? const SizedBox() : ListView.builder(
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                itemCount: subCategoryList.length,
                                itemBuilder: (context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      cubit.updateFilterSubCat(index);
                                    },
                                    child: FilterSubCard(
                                      first: 0 == index ? true : false,
                                      title: subCategoryList[index].name ?? '',
                                      select: index == cubit.selectedFilterSubId ? true : false,
                                    ),
                                  );
                                },
                              )
                                  : ListView.builder(
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                itemCount: cubit.filterPriceList.length,
                                itemBuilder: (context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      cubit.updateFilterPrice(index, cubit.filterPriceList[index]['min'] ?? '', cubit.filterPriceList[index]['max'] ?? '');
                                    },
                                    child: FilterSubCard(
                                      first: 0 == index ? true : false,
                                      title:  cubit.filterPriceList[index]['name'] ?? '',
                                      select: index == cubit.selectedFilterPriceIndex ? true : false,
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    kHeightBox5,
                  ],
                );
              },)
          ),
        );
      },
    );
  }
}
