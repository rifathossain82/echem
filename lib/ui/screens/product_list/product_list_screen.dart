import 'package:echem/bloc/products/products_bloc.dart';
import 'package:echem/bloc/products/products_event.dart';
import 'package:echem/ui/widgets/category_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/main_app_bar.dart';
import '../../widgets/product_infinity_grid.dart';

class ProductListScreen extends StatelessWidget {
  final String? searchKey, categoryId, subCatId, childCatId, type, minPrice, maxPrice, brandId, sorting;

  const ProductListScreen({
    Key? key,
    this.searchKey,
    this.categoryId,
    this.subCatId,
    this.childCatId,
    this.type,
    this.sorting,
    this.brandId,
    this.minPrice,
    this.maxPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNav(),
      drawer: const CategoryDrawer(),
      body: SafeArea(
        child: Column(
          children: [
            const MainCustomAppBar(
              backButton: false,
            ),
            Expanded(
              child: BlocProvider(
                create: (_) => ProductBloc(
                  searchKey: searchKey,
                  categoryId: categoryId,
                  subCatId: subCatId,
                  childCatId: childCatId,
                  type: type,
                  sorting: sorting,
                  brandId: brandId,
                  minPrice: minPrice,
                  maxPrice: maxPrice,
                )..add(ProductsFetched()),
                child: const ProductInfinityGrid(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
