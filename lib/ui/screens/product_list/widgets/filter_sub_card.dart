import 'package:echem/constants/colors_data.dart';
import 'package:flutter/material.dart';
import '../../../../constants/style_data.dart';

class FilterSubCard extends StatelessWidget {
  final String title;
  final bool select;
  final bool first;

  const FilterSubCard({
    Key? key,
    required this.title,
    this.select = false,
    this.first = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10,),
          child: Row(
            children: [
              Icon(
                Icons.circle,
                color: select ? kErrorColor : Colors.white.withOpacity(0.5),
                size: 15,
              ),
              kWidthBox10,
              Text(
                '--- $title',
                maxLines: 1,
                style: Theme.of(context).textTheme.subtitle2?.copyWith(
                      color: Theme.of(context).disabledColor,
                    ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
