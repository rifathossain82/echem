import 'package:echem/bloc/filter/filter_cubit.dart';
import 'package:echem/bloc/filter/filter_state.dart';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/style_data.dart';
import 'package:echem/ui/widgets/custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../constants/images.dart';
import '../../../constants/size_config.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/input_from_widget.dart';
import '../../widgets/main_app_bar.dart';
import '../product_list/widgets/filter_sub_card.dart';
import 'widgets/filter_cat_card.dart';
import '../../widgets/search_infinity_grid.dart';
var scaffoldKey = GlobalKey<ScaffoldState>();

class SearchScreen extends StatelessWidget {
  const SearchScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final searchTxtController = TextEditingController();
    return BlocConsumer<FilterCubit, FilterState>(
        listener: (context, state) {},
        builder: (context, state) {
          FilterCubit cubit = FilterCubit.get(context);

          return Scaffold(
            key: scaffoldKey,
            bottomNavigationBar: const BottomNav(),
            body: SafeArea(
              child: Column(
                children: [
                  const MainCustomAppBar(
                    backButton: true,
                  ),
                  kHeightBox10,
                  InkWell(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: InputFormWidget(
                        fieldController: searchTxtController,
                        prefixIcon: Icon(
                          Icons.search,
                          color: Theme.of(context).disabledColor,
                        ),
                        actionType: TextInputAction.search,
                        hintText: 'Search For Products',
                        onConfirm: (String ? searchKey){
                          if(searchTxtController.text.isEmpty){
                            showCustomSnackBar(context, 'your search key is empty!');
                          }else{
                            context.read<FilterCubit>().getProductList(true,
                              searchKey: searchKey,
                              categoryId: cubit.selectedFilterCategoryId != null ? (cubit.selectedFilterCategoryId ?? 0).toString() : null,
                              brandId: cubit.selectedFilterBrandId != null ? (cubit.selectedFilterBrandId ?? 0).toString() : null,
                              sorting: cubit.selectedSort.isNotEmpty ? cubit.selectedSort : null,
                              minPrice: cubit.selectedMinPrice.isNotEmpty ? cubit.selectedMinPrice : null,
                              maxPrice: cubit.selectedMaxPrice.isNotEmpty ? cubit.selectedMaxPrice : null,
                            );
                          }
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: CustomButton(
                            imageURL: Images.filterSearch,
                            btnColor: kPrimaryColor,
                            title: 'FILTER PRODUCTS',
                            textColor: Colors.white,
                            textSize: 12,
                            onPress: () async {
                              showFilterDialog(context,);
                            },
                          ),
                        ),
                        kWidthBox10,
                        Expanded(
                          child: CustomButton(
                            imageURL: Images.returnIcon,
                            btnColor: const Color(0xFF6A38DE),
                            title: 'RESET FILTERS',
                            textSize: 12,
                            textColor: Colors.white,
                            onPress: () {
                              context.read<FilterCubit>().resetFilter();
                              context.read<FilterCubit>().getProductList(true,startIndex: 0,);
                            },
                          ),
                        ),
                        const SizedBox(
                          width: 40,
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: SearchInfinityGrid(
                    crossCount: 3,
                    paddingTobBottom: 5,
                    categoryId: cubit.selectedFilterCategoryId != null ? (cubit.selectedFilterCategoryId ?? 0).toString() : null,
                    brandId: cubit.selectedFilterBrandId != null ? (cubit.selectedFilterBrandId ?? 0).toString() : null,
                    sorting: cubit.selectedSort.isNotEmpty ? cubit.selectedSort : null,
                    minPrice: cubit.selectedMinPrice.isNotEmpty ? cubit.selectedMinPrice : null,
                    maxPrice: cubit.selectedMaxPrice.isNotEmpty ? cubit.selectedMaxPrice : null,
                  ),),
                ],
              ),
            ),
          );
        });
  }

  showFilterDialog(BuildContext context,) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          insetPadding: const EdgeInsets.only(top: 70),
          alignment: Alignment.topCenter,
          backgroundColor: kSecondaryColor,
          //const Color(0xFF1E243C),,
          content: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            width: SizeConfig.screenWidth! - 20,
            child: BlocBuilder<FilterCubit, FilterState>(
                builder: (context, state) {
                  FilterCubit cubit = FilterCubit.get(context);
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      kHeightBox10,
                      SizedBox(
                        height: SizeConfig.screenWidth,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 1,
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: cubit.filterTypeList.length,
                                itemBuilder: (context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      cubit.updateFilterType(index);
                                    },
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        kHeightBox10,
                                        Text(
                                          cubit.filterTypeList[index],
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6
                                              ?.copyWith(
                                            color: index ==
                                                cubit.selectedFilterTypeIndex
                                                ? kErrorColor
                                                : kWhiteColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                            kWidthBox5,
                            Container(
                              width: .5,
                              color: Theme.of(context).disabledColor,
                            ),
                            kWidthBox5,
                            Expanded(
                              flex: 3,
                              child: cubit.isLoading ? const CustomLoader() : Padding(
                                padding: const EdgeInsets.only(top: 0),
                                child: cubit.selectedFilterTypeIndex == 0 //category
                                    ? ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: cubit.categoryList.length,
                                    itemBuilder: (context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          cubit.updateFilterCategory(cubit.categoryList[index].id ?? 0);
                                        },
                                        child: FilterCatCard(
                                          title: cubit.categoryList[index].name ?? '',
                                          select: (cubit.categoryList[index].id ?? 0) == cubit.selectedFilterCategoryId
                                              ? true
                                              : false,
                                        ),
                                      );
                                    }) : cubit.selectedFilterTypeIndex == 1
                                    ? ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: cubit.brandList.length,
                                    itemBuilder: (context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          cubit.updateFilterBrand(cubit.brandList[index].id ?? 0);
                                        },
                                        child: FilterCatCard(
                                          title: cubit.brandList[index].name ?? '',
                                          select: (cubit.brandList[index].id ?? 0) == cubit.selectedFilterBrandId
                                              ? true
                                              : false,
                                          brand: true,
                                        ),
                                      );
                                    }) : cubit.selectedFilterTypeIndex == 2
                                    ? ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: cubit.filterPriceList.length,
                                    itemBuilder: (context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          cubit.updateFilterPrice(index, cubit.filterPriceList[index]['min'] ?? '', cubit.filterPriceList[index]['max'] ?? '');
                                        },
                                        child: FilterSubCard(
                                          title: cubit.filterPriceList[index]['name'] ?? '',
                                          select: index ==
                                              cubit.selectedFilterPriceIndex
                                              ? true
                                              : false,
                                        ),
                                      );
                                    }) : ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: cubit.sortList.length,
                                  itemBuilder: (context, int index) {
                                    return InkWell(
                                      onTap: () {
                                        cubit.sortSelectedUpdate(cubit.sortList[index]['sort'] ?? '',);
                                      },
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                            child: Text(
                                              cubit.sortList[index]['name'] ?? '',
                                              maxLines: 1,
                                              style: kRegularText2.copyWith(
                                                color: cubit.selectedSort == (cubit.sortList[index]['sort'] ?? '')
                                                    ? kErrorColor
                                                    : kWhiteColor,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      kHeightBox5,
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 25,
                        ),
                        child: Row(
                          children: [
                            CustomButton(
                              width: 120,
                              btnColor: kErrorColor,
                              title: 'Close',
                              textColor: Colors.white,
                              height: 32,
                              onPress: () async {
                                Navigator.of(context).pop();
                              },
                            ),
                            kWidthBox20,
                            CustomButton(
                              btnColor: kPrimaryColor,
                              padding: const EdgeInsets.symmetric(horizontal: 25),
                              title: 'App Filter',
                              textColor: Colors.white,
                              height: 32,
                              onPress: () async {
                                Navigator.of(context).pop();
                                context.read<FilterCubit>().getProductList(true,startIndex: 0,
                                categoryId: cubit.selectedFilterCategoryId != null ? (cubit.selectedFilterCategoryId ?? 0).toString() : null,
                                brandId: cubit.selectedFilterBrandId != null ? (cubit.selectedFilterBrandId ?? 0).toString() : null,
                                sorting: cubit.selectedSort.isNotEmpty ? cubit.selectedSort : null,
                                minPrice: cubit.selectedMinPrice.isNotEmpty ? cubit.selectedMinPrice : null,
                                maxPrice: cubit.selectedMaxPrice.isNotEmpty ? cubit.selectedMaxPrice : null,
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                })

          ),
        );
      },
    );
  }
}
