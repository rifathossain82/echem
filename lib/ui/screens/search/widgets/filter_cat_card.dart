import 'package:echem/constants/colors_data.dart';
import 'package:flutter/material.dart';

class FilterCatCard extends StatelessWidget {
  final String title;
  final bool select;
  final bool brand;

  const FilterCatCard({
    Key? key,
    required this.title,
    this.select = false,
    this.brand = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
            vertical: 10,
          ),
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: brand ? 10 : 0),
                child: brand ? Icon(
                  select ? Icons.check_circle_rounded : Icons.circle_outlined,
                  color: select ? kErrorColor : kWhiteColor,
                  size: 15,
                ) : const SizedBox(),
              ),
              Expanded(
                child: Text(
                  title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline5?.copyWith(
                        color: select
                            ? kErrorColor
                            : kWhiteColor,
                      ),
                ),
              ),
             /* CustomButton(
                width: 80,
                btnColor: select ? kErrorColor : Theme.of(context).cardColor,
                title: '100',
                textColor:  select ? kWhiteColor :  Theme.of(context).textTheme.subtitle2!.color!,
                textSize: 10,
                height: 27,
              )*/
            ],
          ),
        ),
      ],
    );
  }
}
