import 'dart:async';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:echem/constants/images.dart';
import 'package:echem/constants/size_config.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/colors_data.dart';
import '../../widgets/custom_alert_dialog.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = 'splash_screen';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;
  final box = GetStorage();
  @override
  void initState() {
    SharedPreferences.getInstance().then((pr) {
      prefs = pr;
    });
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _animationController.forward();
    _animation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeOut,
    );
    _animation.addListener(() => setState(() {}));
    _animationController.forward();

    //get app init
    Future<bool> appIsRunning = getAppInit();

    super.initState();
  }

  Future<bool> getAppInit() async {
    bool value = false;
    FirebaseFirestore.instance
        .collection('app_init')
        .get()
        .then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        log('${doc.data()}');
        Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
        log('app is running: ${data["app_running"] as bool}'); // check
        value = data["app_running"] as bool; // check
        String title = data["app_title"] as String; // check
        String body = data["app_body"] as String; // check

        Timer(const Duration(seconds: 3), () async {
          if(value){
            navigation();
          }else{
            CustomAlertDialog().customAlert(
              context: context,
              confirmTitle: 'Exit App',
              cancelTitle: 'No',
              title: title,
              body: body,
              color: kPrimaryColor,
              isOneButton: true,
              barrierDismissible: false,
              onPress: () {
                SystemNavigator.pop();
              },
            );
          }

        });
      }
    });
    return value;
  }
  void navigation() async {
    if (box.read('intro') == true) {
      NavigatorHelper.openDashboardScreen(context);
    } else {
      NavigatorHelper.openIntroScreen(context);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: FadeTransition(
          opacity: _animation as Animation<double>,
          child: Hero(
            tag: 'logo',
            child: Image.asset(
              Images.homeLogo,
              width: _animation.value * getProportionateScreenWidth(200),
              height: _animation.value * getProportionateScreenWidth(200),
            ),
          ),
        ),
      ),
    );
  }
}
