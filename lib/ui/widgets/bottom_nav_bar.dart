import 'package:echem/helper/navigator_helper.dart';
import 'package:flutter/material.dart';
import '../../constants/images.dart';
import '../../constants/strings.dart';
import '../../main.dart';
import 'bottom_nav_item.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      elevation: .5,
      notchMargin: 5,
      clipBehavior: Clip.antiAlias,
      shape: const CircularNotchedRectangle(),
      child: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Row(children: [
          BottomNavItem(
            iconData: Images.home,
            label: "Home",
            onTap: () {
              NavigatorHelper.openDashboardScreen(context, index: 0);
            },
          ),
          BottomNavItem(
            iconData: Images.category,
            label: "Category",
            onTap: () {
              NavigatorHelper.openDashboardScreen(context, index: 1);
            },
          ),
          BottomNavItem(
            iconData: Images.cart,
            label: "Bag",
            onTap: () {
              NavigatorHelper.openDashboardScreen(context, index: 2);
            },
          ),
          BottomNavItem(
            iconData: Images.account,
            label: "Account",
            onTap: () {
              if(prefs.containsKey(token)){
                NavigatorHelper.openDashboardScreen(context, index: 3);
              }else{
                NavigatorHelper.openLoginScreen(context);
              }
            },
          ),
        ]),
      ),
    );
  }
}
