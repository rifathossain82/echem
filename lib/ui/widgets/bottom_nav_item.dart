import 'package:echem/constants/colors_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../constants/style_data.dart';

class BottomNavItem extends StatelessWidget {
  final String iconData;
  final Function onTap;
  final bool isSelected;
  final String label;
  final double size;

  const BottomNavItem({
    Key? key,
    required this.iconData,
    required this.label,
    required this.onTap,
    this.isSelected = false,
    this.size = 15,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.only(bottom: 7, left: 5, right: 5, top: 2),
        decoration: BoxDecoration(
          color: isSelected ? Get.isDarkMode ? kBlackColor2 : const Color(0xFFF5F5F5) : Colors.transparent,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: IconButton(
          padding: EdgeInsets.zero,
          iconSize: 20,
          icon: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: size,
                height: size,
                child: SvgPicture.asset(
                  iconData,
                  color:
                  isSelected ? Theme.of(context).primaryColor :  Theme.of(context).textTheme.subtitle1!.color,
                ),
              ),
              kWidthBox5,
              Text(
                label,
                style: Theme.of(context).textTheme.subtitle2
              ),
            ],
          ),
          onPressed: onTap as void Function(),
        ),
      ),
    );
  }
}