import 'package:echem/bloc/categories/category_bloc.dart';
import 'package:echem/bloc/categories/category_state.dart';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/style_data.dart';
import '../../helper/navigator_helper.dart';
import 'custom_shimmer.dart';

class CategoryDrawer extends StatefulWidget {
  const CategoryDrawer({Key? key}) : super(key: key);

  @override
  State<CategoryDrawer> createState() => _CategoryDrawerState();
}

class _CategoryDrawerState extends State<CategoryDrawer> {
  int? _selected;
  int? _selectedSub;
  final _scrollController = ScrollController();


  @override
  Widget build(BuildContext context) {
    return Container(
      color: kSecondaryColor, //const Color(0xFF1E243C),
      width: SizeConfig.screenWidth! - 100,
      height: SizeConfig.screenHeight,
      child: BlocBuilder<CategoryBloc, CategoryState>(
        builder: (context, state) {
          if (state is CategoryInitial) {
            return CustomShimmer.drawerCategoryShimmer();
          } else if (state is CategoryLoading) {
            return CustomShimmer.drawerCategoryShimmer();
          } else if (state is CategoryLoaded) {
            return Center(
              child: state.categoryModel.errors != null
                  ? Text(
                      state.categoryModel.errors ?? '',
                      style: Theme.of(context).textTheme.subtitle1,
                    )
                  : Container(
                      color: kSecondaryColor, //const Color(0xFF1E243C),
                      width: SizeConfig.screenWidth! - 100,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: ListView(
                        controller: _scrollController,
                        shrinkWrap: true,
                        physics: const ScrollPhysics(),
                        children: [
                          kHeightBox20,
                          ListView.builder(
                            key: Key('builder ${_selected.toString()}'),
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            itemCount: state.categoryModel.data?.length,
                            itemBuilder: (context, int index) {
                              return Theme(
                                data: ThemeData().copyWith(
                                  dividerColor: Colors.transparent,
                                ),
                                child: ExpansionTile(
                                  key: Key(index.toString()),
                                  initiallyExpanded: index == _selected,
                                  onExpansionChanged: ((newState) {
                                    if (newState) {
                                      scrollToIndex(index);
                                      setState(() {_selected = index;});
                                    } else {
                                      setState(() {_selected = -1;});
                                    }
                                  }),
                                  trailing: Icon(
                                    index == _selected ? Icons.minimize : Icons.add,
                                    size: 25,
                                  ),
                                  collapsedTextColor: kBlackColor2,
                                  iconColor: kWhiteColor,
                                  collapsedIconColor: kWhiteColor,
                                  tilePadding: EdgeInsets.zero,
                                  childrenPadding: EdgeInsets.zero,
                                  title: InkWell(
                                    onTap: () {
                                      NavigatorHelper.openFilterScreen(context,
                                          categoryId: (state.categoryModel.data?[index].id ?? 0).toString(),
                                          subCategoryList: state.categoryModel.data![index].subs!  );
                                    },
                                    child: Text(
                                      state.categoryModel.data?[index].name ?? '',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText2.copyWith(
                                        color: kWhiteColor,
                                      ),
                                    ),
                                  ),
                                  children: [
                                    SizedBox(
                                      child: state.categoryModel.data![index].subs!.isEmpty
                                          ? const SizedBox()
                                          : ListView.builder(
                                              physics: const NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: state.categoryModel.data![index].subs!.length,
                                              padding: EdgeInsets.zero,
                                              itemBuilder: (context, int subIndex) {
                                                return Container(
                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                  decoration: BoxDecoration(
                                                    border: Border(
                                                      bottom: BorderSide(
                                                        width: .3,
                                                        color: state.categoryModel.data![index].subs!.length - 1 ==
                                                            subIndex
                                                            ? Colors.transparent
                                                            : kStUnderLineColor,
                                                      ),
                                                    ),
                                                  ),
                                                  child: state.categoryModel.data![index].subs![subIndex].childs!.isEmpty ? Padding(
                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                                    child: InkWell(
                                                      onTap: (){
                                                        NavigatorHelper.openFilterScreen(context,
                                                          categoryId: (state.categoryModel.data?[index].id ?? 0).toString(),
                                                          subCatId: (state.categoryModel.data![index].subs![subIndex].id ?? 0).toString(),
                                                          subCategoryList: [] , );
                                                      },
                                                      child: Text(
                                                        state.categoryModel.data![index].subs![subIndex].name ?? '',
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: kRegularText2.copyWith(
                                                          color: kWhiteColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ) : Padding(
                                                    padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                                    child: Theme(
                                                      data: ThemeData().copyWith(
                                                        dividerColor: Colors.transparent,
                                                      ),
                                                      child: ExpansionTile(
                                                        key: Key(_selectedSub.toString()),
                                                        initiallyExpanded: subIndex == _selectedSub,
                                                        onExpansionChanged: ((newState) {
                                                          if (newState) {
                                                            setState(() {_selectedSub = subIndex;});
                                                            scrollToIndex(subIndex);
                                                          } else {
                                                            setState(() {_selectedSub = -1;});
                                                          }
                                                        }),
                                                        trailing: Icon(
                                                          subIndex == _selectedSub ? Icons.keyboard_arrow_up_outlined :
                                                          Icons.keyboard_arrow_down_outlined,
                                                          size: 25,
                                                        ),
                                                        collapsedTextColor: kBlackColor2,
                                                        iconColor: kWhiteColor,
                                                        collapsedIconColor: kWhiteColor,
                                                        tilePadding: EdgeInsets.zero,
                                                        childrenPadding: EdgeInsets.zero,
                                                        title: InkWell(
                                                          onTap: (){
                                                            NavigatorHelper.openFilterScreen(context,
                                                              categoryId: (state.categoryModel.data?[index].id ?? 0).toString(),
                                                              subCatId: (state.categoryModel.data![index].subs![subIndex].id ?? 0).toString(),
                                                              subCategoryList: state.categoryModel.data![index].subs! , );
                                                          },
                                                          child: Text(
                                                            state.categoryModel.data![index].subs![subIndex].name ?? '',
                                                            maxLines: 1,
                                                            style: kRegularText2.copyWith(
                                                              color: kWhiteColor,
                                                            ),
                                                          ),
                                                        ),
                                                        children: [
                                                          ListView.builder(
                                                            physics:
                                                            const NeverScrollableScrollPhysics(),
                                                            shrinkWrap: true,
                                                            itemCount: state.categoryModel.data![index].subs![subIndex].childs!.length,
                                                            padding: EdgeInsets.zero,
                                                            itemBuilder: (context, int childIndex) {
                                                              return InkWell(
                                                                onTap: (){
                                                                  NavigatorHelper.openFilterScreen(context,
                                                                    categoryId: (state.categoryModel.data?[index].id ?? 0).toString(),
                                                                    subCatId: (state.categoryModel.data![index].subs![subIndex].id ?? 0).toString(),
                                                                    childCatId: (state.categoryModel.data![index].subs![subIndex].childs![childIndex].id ?? 0).toString(),
                                                                    subCategoryList: state.categoryModel.data![index].subs! , );
                                                                },
                                                                child: Padding(
                                                                  padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                  child: Text(
                                                                    state.categoryModel.data![index].subs![subIndex].childs![childIndex].name ?? '',
                                                                    maxLines: 1,
                                                                    style: kRegularText2.copyWith(
                                                                      color: kWhiteColor,
                                                                    ),
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
            );
          } else if (state is CategoryError) {
            return Center(
              child: Text(
                state.message ?? '',
                style: Theme.of(context).textTheme.subtitle1,
              ),
            );
          } else {
            return Center(
              child: Text(
                'No data available',
                style: Theme.of(context).textTheme.subtitle1,
              ),
            );
          }
        },
      ),
    );
  }

  void scrollToIndex(int index) {
    setState(() {
      _scrollController.animateTo(
        50.0 * index,
        duration: const Duration(seconds: 1),
        curve: Curves.easeIn,
      );
    });
  }
}

