import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../constants/colors_data.dart';
import '../../constants/images.dart';
import '../../constants/style_data.dart';

DecorationImage customDecorationImage({
  final String? image,
  final BoxFit? fit,
  final String? placeHolder,
  final bool innerShadow = false,
}) {
  return image == null || image.isEmpty
      ? DecorationImage(
    image:  AssetImage(
      placeHolder ?? Images.placeHolder,
    ),
    colorFilter: innerShadow
        ? ColorFilter.mode(
      kBlackColor.withOpacity(.2),
      BlendMode.darken,
    )
        : null,
    fit: fit ?? defaultBoxFit,
  )
      : DecorationImage(
    image: CachedNetworkImageProvider(
      image,
    ),
    colorFilter: innerShadow
        ? ColorFilter.mode(
      kBlackColor.withOpacity(.2),
      BlendMode.darken,
    )
        : null,
    fit: fit ?? defaultBoxFit,
  );
}