import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../constants/colors_data.dart';
import '../../constants/images.dart';
import '../../constants/style_data.dart';

class CustomImage extends StatelessWidget {
  final double? width;
  final double? height;
  final String? image;
  final String? productType;
  final String? baseUrl;
  final String? placeHolder;
  final BoxFit? fit;
  final double radius;
  final Color? imageColor;
  final bool innerShadow;
  final BorderRadius? borderRadius;

  const CustomImage({
    Key? key,
    this.width,
    this.height,
    this.placeHolder,
    this.fit,
    this.baseUrl,
    this.radius = 0,
    this.innerShadow = false,
    this.productType,
    this.borderRadius,
    this.imageColor,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ClipRRect(
        borderRadius: borderRadius  ?? BorderRadius.circular(radius),
        child: image == null || image!.isEmpty
            ? Image.asset(
          Images.placeHolder,
          fit: fit ?? defaultBoxFit,
          color: innerShadow ? Colors.black.withOpacity(.3) : null,
          colorBlendMode: innerShadow ? BlendMode.darken : null,
        )
            : CachedNetworkImage(
          imageUrl: '$baseUrl${image!}',
          fit: fit ?? defaultBoxFit,
          color: innerShadow ? Colors.black.withOpacity(.3) : imageColor,
          colorBlendMode: innerShadow ? BlendMode.darken : null,
          placeholder: (context, url) => Container(
            decoration: BoxDecoration(
              color: kOrdinaryColor2,
              borderRadius: BorderRadius.circular(radius),
            ),
            child: Image.asset(
              placeHolder ?? Images.placeHolder,
              fit: defaultBoxFit,
            ),
          ),
          errorWidget: (context, url, error) => Image.asset(
            placeHolder ?? Images.placeHolder,
            fit: defaultBoxFit,
          ),
        ),
      ),
    );
  }
}