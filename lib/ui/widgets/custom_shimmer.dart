import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:shimmer/shimmer.dart';

import '../../constants/colors_data.dart';
import '../../constants/size_config.dart';
import '../../constants/style_data.dart';
import '../../resources/api/app_config.dart';
import 'input_from_widget.dart';
import 'more_widgets.dart';

class CustomShimmer {

  static homeScreenShimmer(BuildContext context){
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      child: Column(
        children: [
          Padding(
            padding:
            const EdgeInsets.symmetric(horizontal: 10),
            child: InputFormWidget(
              prefixIcon: Icon(
                Icons.search,
                color: Theme.of(context).disabledColor,
              ),
              absorbing: true,
              hintText: 'Search For Products',
            ),
          ),
          sliderShimmer(),
          homeCategoryShimmer(),
          kHeightBox10,
          Container(
            decoration: BoxDecoration(
              color: const Color(0xFF83A4AD),
              borderRadius: BorderRadius.circular(0),
            ),
            child: Column(
              crossAxisAlignment:
              CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 5,
                    vertical: 10,
                  ),
                  child: Row(
                    children: [
                      Text(
                        'Best Seller',
                        style: Theme.of(context)
                            .textTheme
                            .subtitle2!
                            .copyWith(
                          color: kWhiteColor,
                          fontWeight:
                          FontWeight.w600,
                        ),
                      ),
                      const Spacer(),
                      MoreWidgets(onTap: () {
                      }),
                    ],
                  ),
                ),
                CustomShimmer.horizontalProductShimmer(),
              ],
            ),
          ),
          kHeightBox10,
          gridProductWithTitleShimmer(context, title: 'Big Save', crossCount: 3),
          kHeightBox10,
          videoShimmer(context,),
          kHeightBox10,
          gridProductWithTitleShimmer(context, title: 'New Arrival', crossCount: 2),
        ],
      ),
    );
  }

  static sliderShimmer() {
    return AspectRatio(
      aspectRatio: 2.5,
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          width: double.infinity,
        ),
      ),
    );
  }

  static productsListShimmer() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 15,
      padding: const EdgeInsets.all(5),
      itemBuilder: (context, int index) {
        return Row(
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                height: 100,
                width: 100,
                margin: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ),
            kWidthBox10,
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                kHeightBox10,
                Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    height: 20,
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
                kHeightBox10,
                Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    height: 20,
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
                kHeightBox10,
              ],
            )),
          ],
        );
      },
    );
  }

  static productDetailsShimmer(BuildContext context){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
            const EdgeInsets.symmetric(horizontal: 10),
            child: InputFormWidget(
              prefixIcon: Icon(
                Icons.search,
                color: Theme.of(context).disabledColor,
              ),
              absorbing: true,
              hintText: 'Search For Products',
            ),
          ),
          sliderShimmer(),
          kHeightBox10,
          Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Container(
              height: 25,
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ),
          kHeightBox10,
          Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Container(
              height: 12,
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
          kHeightBox10,
          Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Container(
              height: 12,
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
          kHeightBox10,

        ],
      ),
    );
  }

  static homeCategoryShimmer() {
    return MasonryGridView.count(
      crossAxisCount: 4,
      shrinkWrap: true,
      itemCount: 8,
      physics: const NeverScrollableScrollPhysics(),
      mainAxisSpacing: 5,
      crossAxisSpacing: 5,
      itemBuilder: (context, int i) {
        return Container(
          padding: EdgeInsets.all(getProportionateScreenWidth(10),
          ),
          child: Column(
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  width: 40,
                  height: 40,
                ),
              ),
              kHeightBox5,
              Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  width: SizeConfig.screenWidth,
                  height: 10,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static categoryShimmer(BuildContext context, {itemCount = 8}) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: InputFormWidget(
              prefixIcon: Icon(
                Icons.search,
                color: Theme.of(context).disabledColor,
              ),
              absorbing: true,
              hintText: 'Search For Products',
            ),
          ),
          kHeightBox10,
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 5,
            ),
            child: MasonryGridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              itemCount: itemCount,
              physics: const NeverScrollableScrollPhysics(),
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              itemBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: SizeConfig.screenWidth! / 3.2,
                  width: SizeConfig.screenWidth,
                  child: Stack(
                    children: [
                      Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        child: Container(
                          height: SizeConfig.screenWidth! / 3.2,
                          width: SizeConfig.screenWidth,
                          decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          padding: const EdgeInsets.all(5),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 10,
                            ),
                            child: Container(
                              width: 100,
                              height: 12,
                              decoration: BoxDecoration(
                                color: Theme.of(context).cardColor,
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                );
              },
            ),
          ),
          //const NbBottomWidgets(),
        ],
      ),
    );
  }

  static drawerCategoryShimmer() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 15,
      padding: const EdgeInsets.only(right: 10, left: 10, top: 40),
      itemBuilder: (context, int index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(child: Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),),
                kWidthBox20,
                kWidthBox10,
                Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ],
            ),
            kHeightBox10,
            Padding(
              padding: const EdgeInsets.only(left: 0, right: 45,),
              child: Column(
                children: [
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(
                      width: SizeConfig.screenWidth,
                      height: 10,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                  kHeightBox5,
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(
                      width: SizeConfig.screenWidth,
                      height: 10,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            kHeightBox10,
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 45,),
              child:  Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  width: SizeConfig.screenWidth,
                  height: 10,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              ),
            ),
            kHeightBox10,

          ],
        );
      },
    );
  }

  static horizontalProductShimmer({
    itemCount = 10,
  }) {
    return SizedBox(
      height: getProportionateScreenWidth(120),
      child: ListView.builder(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: itemCount,
        itemBuilder: (context, int index) {
          return Column(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 10,
                ),
                height: getProportionateScreenWidth(95),
                width: getProportionateScreenWidth(95),
                child: Container(
                  decoration: BoxDecoration(
                    color: kOrdinaryColor2,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  height: getProportionateScreenWidth(95),
                  width: getProportionateScreenWidth(95),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(
                      height: 12,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                ),
              ),
              kHeightBox5,
              Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  height: 10,
                  width: getProportionateScreenWidth(50),
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  static productsGridShimmer({itemCount = 8, int crossCount = 3}) {
    return MasonryGridView.count(
      crossAxisCount: crossCount,
      shrinkWrap: true,
      itemCount: itemCount,
      padding: const EdgeInsets.all(5),
      physics: const ScrollPhysics(),
      mainAxisSpacing: 9.0,
      crossAxisSpacing: 9.0,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            height: 180,
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        );
      },
    );
  }

  static gridProductWithTitleShimmer(
    BuildContext context, {
    int itemCount = 8,
    required String title,
    int crossCount = 3,
    double padding = 5,
    double paddingTobBottom = 0,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: padding,
        vertical: paddingTobBottom,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          kHeightBox10,
          kHeightBox10,
          Center(
            child: Text(
             title,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2,
              textAlign: TextAlign.center,
            ),
          ),
          kHeightBox10,
          productsGridShimmer(
            itemCount: itemCount,
            crossCount: crossCount,
          ),
        ],
      ),
    );
  }

  static videoShimmer(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        kHeightBox10,
        Center(
          child: Text(
            '$appName Tips & Tricks',
            style: Theme.of(context).textTheme.subtitle2,
            textAlign: TextAlign.center,
          ),
        ),
        kHeightBox10,
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
          ),
          child: MasonryGridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            itemCount: 4,
            physics: const NeverScrollableScrollPhysics(),
            mainAxisSpacing: 5,
            crossAxisSpacing: 5,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  kHeightBox10,
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(
                      height: SizeConfig.screenWidth! / 2.8,
                      width: SizeConfig.screenWidth,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                  kHeightBox5,
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(
                      height: 12,
                      width: SizeConfig.screenWidth,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  //order list shimmer effect
 static buildOrderListShimmer(
      {itemCount = 10, }) {
    return ListView.builder(
      itemCount: itemCount,
      scrollDirection: Axis.vertical,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            kHeightBox10,
            SizedBox(
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Card(
                  color: Theme.of(context).cardColor,
                  margin: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  elevation: 1.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        child: Container(
                          width: SizeConfig.screenWidth,
                          height: 25,
                          decoration: const BoxDecoration(
                            color: Colors.grey,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(5),
                             topRight: Radius.circular(5),
                           ),
                          ),
                        ),
                      ),
                      kHeightBox10,
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 40,),
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            kHeightBox5,
                            Row(
                              children: [
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 40,),
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            kHeightBox5,
                            Row(
                              children: [
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 40,),
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            kHeightBox5,
                            Row(
                              children: [
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 40,),
                                Expanded(
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300]!,
                                    highlightColor: Colors.grey[100]!,
                                    child: Container(
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            kHeightBox5,
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  //order details shimmer effect
  static orderDetailsShimmer(BuildContext context){
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: SizeConfig.screenWidth,
                height: 28,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
              ),
            ),
            kHeightBox10,
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: SizeConfig.screenWidth,
                height: 35,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
              ),
            ),
            Text(
              'Order Overview',
              style: Theme
                  .of(context)
                  .textTheme
                  .subtitle1
                  ?.copyWith(
                fontSize: 17,
              ),
            ),
            kHeightBox5,
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: SizeConfig.screenWidth,
                height: 10,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
              ),
            ),
            kHeightBox5,
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: SizeConfig.screenWidth,
                height: 10,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
              ),
            ),
            kHeightBox5,
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: SizeConfig.screenWidth,
                height: 10,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
              ),
            ),
            kHeightBox10,
            Text(
              'Order Products',
              style: Theme
                  .of(context)
                  .textTheme
                  .subtitle1
                  ?.copyWith(
                fontSize: 17,
              ),
            ),
            kHeightBox10,
            productsListShimmer(),
          ],
        ),
      ),
    );
  }
}
