import 'package:echem/constants/style_data.dart';
import 'package:flutter/material.dart';
import '../../constants/colors_data.dart';

class DoubleTapButton extends StatelessWidget {
  final String title;
  final String title2;
  final Function? onPress;
  final Function? onPress2;
  final double radius;
  final double border;
  final Color? borderColor;
  final EdgeInsets? padding;
  final Color textColor;
  final Color btnColor;
  final double? width;
  final double? height;
  final double? textSize;
  final MainAxisAlignment? mainAxisAlignment;

  const DoubleTapButton({
    Key? key,
    required this.title,
    required this.title2,
    this.width,
    this.height = 36,
    this.textColor = Colors.white,
    this.btnColor = kErrorColor,
    this.borderColor,
    this.border = 0,
    this.onPress,
    this.onPress2,
    this.radius = 10,
    this.textSize = 14,
    this.mainAxisAlignment,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(
          color: btnColor,
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(
              width: border,
              color: borderColor == null ? Colors.transparent : borderColor!)),
      width: width,
      height: height,
      child: Row(
        mainAxisAlignment: mainAxisAlignment ?? MainAxisAlignment.start,
        children: [
          Expanded(
            child: Center(
              child: TextButton(
                child: Text(
                  title,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: textColor,
                        fontSize: textSize,
                      ),
                ),
                onPressed: onPress as void Function()?,
              ),
            ),
          ),
          kWidthBox10,
          Container(
            width: 1.5,
            margin: const EdgeInsets.symmetric(vertical: 13),
            height: 50,
            color: kWhiteColor,
          ),
          kWidthBox10,
          Expanded(
            child: Center(
              child: TextButton(
                child: Text(
                  title2,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: textColor,
                        fontSize: textSize,
                      ),
                ),
                onPressed: onPress2 as void Function()?,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
