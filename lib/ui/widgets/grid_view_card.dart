import 'dart:developer';

import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/models/response/product_list.dart';
import 'package:echem/resources/api/app_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../constants/colors_data.dart';
import '../../constants/style_data.dart';
import '../../utils/base_method.dart';
import 'custom_image.dart';

class GridViewCard extends StatelessWidget {
  final bool last, isHome;
  final int crossCount;
  final ProductList product;

  const GridViewCard({
    Key? key,
    this.crossCount = 3,
    this.last = false,
    this.isHome = false,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      color: Theme.of(context).cardColor,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: SizedBox(
          height: crossCount == 2 ? 200 : 210,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              kHeightBox5,
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: CustomImage(
                    baseUrl: kProductImagePath,
                    image: product.photo,
                    radius: crossCount == 2 ? 0 : 5,
                  ),
                ),
              ),
              SizedBox(
                height: crossCount == 2 ? 10 : 5,
              ),
              Text(
                product.name ?? '',
                maxLines: crossCount == 3 ? 1 : 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle2,
              ),
              SizedBox(
                height: crossCount == 2 ? 10 : 5,
              ),
              if((product.previousPrice ?? 0) > (product.price ?? 0))
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 2,
                ),
                decoration: BoxDecoration(
                  color: Colors.red.shade400,
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                ),
                child: Text(
                  '${BaseMethod.discountPercentage(prevPrice: product.previousPrice ?? 0, price: product.price ?? 0)}% OFF',
                  style: Theme.of(context).textTheme.bodyText2?.copyWith(
                        color: kWhiteColor,
                      ),
                ),
              ),
              SizedBox(
                height: crossCount == 2 ? 10 : 5,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: '$kCurrency ${product.price ?? 0} ',
                        style: Theme.of(context).textTheme.headline6),
                    if((product.previousPrice ?? 0) > (product.price ?? 0))
                      TextSpan(
                        text:  '$kCurrency ${product.previousPrice ?? 0}',
                        style: Theme.of(context).textTheme.subtitle2?.copyWith(
                              color: Theme.of(context).disabledColor,
                              decoration: TextDecoration.lineThrough,
                            ),
                      ),
                  ],
                ),
              ),
              SizedBox(
                height: crossCount == 2 ? 5 : 0,
              ),
              if(product.averageRating != null || isHome)
              RatingBarIndicator(
                rating: double.parse(product.averageRating ?? '0.0',),
                itemBuilder: (context, index) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 12.0,
                direction: Axis.horizontal,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
