import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/models/response/product_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'grid_view_card.dart';

class GridViewList extends StatelessWidget {
  final int crossCount;
  final double padding;
  final double paddingTobBottom;
  final bool isHome, isDetailsScreen;
  final List<ProductList> productList;

  const GridViewList({
    Key? key,
    required this.productList,
    this.crossCount = 3,
    this.padding = 5,
    this.isHome = false,
    this.isDetailsScreen = false,
    this.paddingTobBottom = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: padding,
        vertical: paddingTobBottom,
      ),
      child: MasonryGridView.count(
        crossAxisCount: crossCount,
        itemCount: isHome
            ? productList.length > 15
                ? 15
                : productList.length
            : productList.length,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              if(isDetailsScreen){
                NavigatorHelper.openProductDetailsReplacement(context, productId: productList[index].id ?? 0);
              }else{
                NavigatorHelper.openProductDetailsScreen(context, productId: productList[index].id ?? 0);
              }

            },
            child: GridViewCard(
              crossCount: crossCount,
              last: index == 4 - 1 ? true : false,
              isHome: isHome,
              product: productList[index],
            ),
          );
        },
      ),
    );
  }
}
