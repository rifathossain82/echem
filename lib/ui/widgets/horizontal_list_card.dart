import 'package:echem/constants/size_config.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../../constants/colors_data.dart';
import '../../constants/style_data.dart';
import '../../models/response/product_list.dart';
import '../../resources/api/app_config.dart';
import 'custom_image.dart';


class HorizontalListCard extends StatelessWidget {
  final bool last;
  final ProductList product;

  const HorizontalListCard({
    Key? key,
    required this.product,
    this.last = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenWidth(220),
      child: Card(
        elevation: 1,
        color: Theme.of(context).cardColor,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
               Expanded(
                child: CustomImage(
                  baseUrl: kProductImagePath,
                  image: product.photo,
                  radius: 5,
                ),
              ),
              kHeightBox5,
              Text(
                product.name ?? '',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle2,
              ),
              kHeightBox5,
              if((product.previousPrice ?? 0) > (product.price ?? 0))
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 2,
                ),
                decoration:  BoxDecoration(
                  color: Colors.red.shade400,
                  borderRadius: const BorderRadius.all( Radius.circular(10)),
                ),
                child: Text(
                  '${BaseMethod.discountPercentage(prevPrice: product.previousPrice ?? 0, price: product.price ?? 0)}% OFF',
                  style: Theme.of(context).textTheme.bodyText2?.copyWith(
                    color: kWhiteColor,
                  ),
                ),
              ),
              kHeightBox5,
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: '$kCurrency ${product.price ?? 0} ',
                        style: Theme.of(context).textTheme.headline6
                    ),
                    if((product.previousPrice ?? 0) > (product.price ?? 0))
                    TextSpan(
                      text: kCurrency + '${product.previousPrice ?? 0}',
                      style: Theme.of(context).textTheme.subtitle2?.copyWith(
                        color: Theme.of(context).disabledColor,
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                  ],
                ),
              ),
              if(product.averageRating != null)
                RatingBarIndicator(
                  rating: double.parse(product.averageRating ?? '0.0',),
                itemBuilder: (context, index) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 12.0,
                direction: Axis.horizontal,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
