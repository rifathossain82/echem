import 'package:echem/models/response/product_list.dart';
import 'package:flutter/material.dart';

import '../../constants/size_config.dart';
import '../../helper/navigator_helper.dart';
import 'horizontal_list_card.dart';

class HorizontalListView extends StatelessWidget {
  final List<ProductList> productList;

  const HorizontalListView({
    Key? key,
    required this.productList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getProportionateScreenWidth(230),
      child: ListView.builder(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: productList.length > 10 ? 10 : productList.length,
        padding: EdgeInsets.zero,
        itemBuilder: (context, int index) {
          return Column(
            children: [
              InkWell(
                onTap: (){
                  NavigatorHelper.openProductDetailsScreen(context, productId: productList[index].id ?? 0);
                },
                child: SizedBox(
                    width: SizeConfig.screenWidth! / 3.3,
                    child: HorizontalListCard(
                      product: productList[index],
                    )),
              ),
            ],
          );
        },
      ),
    );
  }
}
