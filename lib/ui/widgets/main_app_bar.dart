import 'dart:developer';
import 'package:echem/constants/colors_data.dart';
import 'package:echem/constants/strings.dart';
import 'package:echem/helper/navigator_helper.dart';
import 'package:echem/main.dart';
import 'package:echem/utils/base_method.dart';
import 'package:flutter/material.dart';
import '../../constants/images.dart';
import '../../constants/style_data.dart';

class MainCustomAppBar extends StatelessWidget {
  final bool backButton, iconHide, rightSideHide, isAccountScreen;
  final Function? onBackPress;
  const MainCustomAppBar({
    Key? key,
    this.backButton = false,
    this.onBackPress,
    this.iconHide = false,
    this.isAccountScreen = false,
    this.rightSideHide = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: .5,
      margin: EdgeInsets.zero,
      color: Theme.of(context).appBarTheme.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 2,
        ),
        child: Row(
          children: [
            Builder(
              builder: (context) => // Ensure Scaffold is in context
              IconButton(
                onPressed: onBackPress != null ? onBackPress as Function()? : () {
                  if (backButton) {
                    Navigator.of(context).pop();
                  } else {
                    log('print');
                    Scaffold.of(context).openDrawer();
                  }
                },
                icon: backButton
                    ? Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color:
                      Theme.of(context).textTheme.subtitle1!.color,
                      shape: BoxShape.circle),
                  child: Icon(
                    Icons.arrow_back_ios_new_outlined,
                    color: Theme.of(context).cardColor,
                    size: 15,
                  ),
                )
                    : const Icon(
                  Icons.menu_outlined,
                  color: kPrimaryColor,
                ),
              ),
            ),
            if (!iconHide)
              Container(
                height: 45,
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Image.asset(Images.homeLogo),
              ),
            kWidthBox10,
            Expanded(child: Container()),
            if (!rightSideHide)
              InkWell(
                onTap: () {
                  if(!isAccountScreen){
                    if (prefs.containsKey(token)) {
                      NavigatorHelper.openAccountScreen(context);
                    } else {
                      NavigatorHelper.openLoginScreen(context);
                    }
                  }
                },
                child: Row(
                  children: [
                    Text(
                      BaseMethod.greeting(),
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    kWidthBox5,
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 2,
                      ),
                      decoration: const BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Text(
                        prefs.containsKey(token)
                            ? prefs.getString(userName) ?? ''
                            : 'Login',
                        style:
                        Theme.of(context).textTheme.bodyText2?.copyWith(
                          color: kWhiteColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            kWidthBox10,
          ],
        ),
      ),
    );
  }
}
