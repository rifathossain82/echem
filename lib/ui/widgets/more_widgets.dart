import 'package:flutter/material.dart';
import '../../constants/colors_data.dart';

class MoreWidgets extends StatelessWidget {
  final Function? onTap;
  final String? title;
  const MoreWidgets({
    Key? key,
    this.onTap,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap as void Function()?,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 6,
          vertical: 3,
        ),
        decoration:  const BoxDecoration(
          color: kWhiteColor,
          borderRadius: BorderRadius.all( Radius.circular(10)),
        ),
        child: Text(
          title ?? 'View All',
          style: Theme.of(context).textTheme.bodyText2?.copyWith(
            color: kBlackColor2,
          ),
        ),
      ),
    );
  }
}
