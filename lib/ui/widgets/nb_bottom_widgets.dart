import 'package:flutter/material.dart';
import '../../constants/colors_data.dart';
import '../../constants/size_config.dart';
import 'custom_image.dart';

class NbBottomWidgets extends StatelessWidget {
  const NbBottomWidgets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Stack(
        children: [
          CustomImage(
            width: SizeConfig.screenWidth,
            height: 200,
            image:
                'https://st.focusedcollection.com/14916868/i/1800/focused_278025542-stock-photo-view-makeup-beauty-products-pink.jpg',
            radius: 0,
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 20,
              ),
              child: Text(
                'A simple yet fully customizable rating bar for flutter which also include a rating bar indicator, supporting any fraction of rating.',
                style: Theme.of(context).textTheme.headline6?.copyWith(
                      color: kBlackColor2,
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
