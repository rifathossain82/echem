import 'package:echem/bloc/products/products_bloc.dart';
import 'package:echem/bloc/products/products_event.dart';
import 'package:echem/bloc/products/products_state.dart';
import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../../helper/navigator_helper.dart';
import 'custom_loader.dart';
import 'grid_view_card.dart';

class ProductInfinityGrid extends StatefulWidget {
  final double padding;
  final int crossCount;
  final double paddingTobBottom;
  const ProductInfinityGrid({
    Key? key,
    this.padding = 5,
    this.crossCount = 2,
    this.paddingTobBottom = 5,
  }) : super(key: key);

  @override
  State<ProductInfinityGrid> createState() => _ProductListState();
}

class _ProductListState extends State<ProductInfinityGrid> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductBloc, ProductsState>(
      builder: (context, state) {
        switch (state.status) {
          case ProductStatus.failure:
            return Center(child: Text('failed to fetch posts', style: Theme.of(context).textTheme.subtitle1,));
          case ProductStatus.success:
            if (state.posts.isEmpty) {
              return  Center(child: Text('No data available',
                style: Theme.of(context).textTheme.subtitle1,));
            }
            return MasonryGridView.count(
              controller: _scrollController,
              crossAxisCount: widget.crossCount,
              shrinkWrap: true,
              itemCount: state.hasReachedMax
                  ? state.posts.length
                  : state.posts.length + 1,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              padding: EdgeInsets.symmetric(
                horizontal: widget.padding,
                vertical: widget.paddingTobBottom,
              ),
              itemBuilder: (BuildContext context, int index) {
                return index >= state.posts.length
                    ? const CustomBottomLoader()
                    : InkWell(
                  onTap: () {
                    NavigatorHelper.openProductDetailsScreen(context, productId: state.posts[index].id ?? 0);
                  },
                  child: GridViewCard(
                    crossCount: widget.crossCount,
                    last: index == 4 - 1 ? true : false, product: state.posts[index],
                  ),
                );
              },
            );
          case ProductStatus.initial:
            return CustomShimmer.productsGridShimmer(crossCount: widget.crossCount, itemCount: 15);
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) context.read<ProductBloc>().add(ProductsFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
