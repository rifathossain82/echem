import 'dart:developer';
import 'package:echem/bloc/filter/filter_cubit.dart';
import 'package:echem/ui/widgets/custom_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../bloc/filter/filter_state.dart';
import '../../helper/navigator_helper.dart';
import 'custom_loader.dart';
import 'grid_view_card.dart';

class SearchInfinityGrid extends StatefulWidget {
  final String? searchKey, categoryId, subCatId, childCatId, type, minPrice, maxPrice, brandId, sorting;
  final double padding;
  final int crossCount;
  final double paddingTobBottom;
  final bool isFilter;

  const SearchInfinityGrid({
    Key? key,
    this.padding = 5,
    this.crossCount = 2,
    this.paddingTobBottom = 5,
    this.searchKey,
    this.categoryId,
    this.subCatId,
    this.childCatId,
    this.type,
    this.sorting,
    this.brandId,
    this.minPrice,
    this.maxPrice,
    this.isFilter = false,
  }) : super(key: key);

  @override
  State<SearchInfinityGrid> createState() => _ProductListState();
}

class _ProductListState extends State<SearchInfinityGrid> {
  final _scrollController = ScrollController();
  final filterCubit = FilterCubit();

  @override
  void initState() {
    getProducts(reload: true);
    _scrollController.addListener(addProducts);
    super.initState();
  }

  void addProducts() async {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      log('===========>>>work');
      filterCubit.changeOffset();
      context.read<FilterCubit>().showBottomLoader();

      getProducts();
    }
  }

  void getProducts({bool reload = false}) async {
    context.read<FilterCubit>().getProductList(reload,
      searchKey: widget.searchKey,
      categoryId: widget.categoryId,
      subCatId: widget.subCatId,
      childCatId: widget.childCatId,
      type: widget.type,
      sorting: widget.sorting,
      brandId: widget.brandId,
      minPrice: widget.minPrice,
      maxPrice: widget.maxPrice,);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterCubit, FilterState>(
      builder: (context, state) {
        FilterCubit cubit = FilterCubit.get(context);
        return Container(
          child: cubit.isProductLoading
              ? CustomShimmer.productsGridShimmer(crossCount: widget.crossCount, itemCount: 15)
              : cubit.productList.isEmpty
                  ? Center(
                      child: Text('No data available',
                        style: Theme.of(context).textTheme.subtitle1,),
                    )
                  : Stack(
                      clipBehavior: Clip.none,
                      children: [
                        MasonryGridView.count(
                          controller: _scrollController,
                          crossAxisCount: widget.crossCount,
                          shrinkWrap: true,
                          itemCount: cubit.productList.length,
                          mainAxisSpacing: 8,
                          crossAxisSpacing: 8,
                          padding: EdgeInsets.symmetric(
                            horizontal: widget.padding,
                            vertical: widget.paddingTobBottom,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {
                                NavigatorHelper.openProductDetailsScreen(
                                    context,
                                    productId:
                                        cubit.productList[index].id ?? 0);
                              },
                              child: GridViewCard(
                                crossCount: widget.crossCount,
                                last: index == 4 - 1 ? true : false,
                                product: cubit.productList[index],
                              ),
                            );
                          },
                        ),
                        Positioned(
                          bottom: -20,
                          child: cubit.isLoadingMore
                              ? const CustomLoader()
                              : const SizedBox(),
                        ),
                      ],
                    ),
        );
      },
    );
  }
}
