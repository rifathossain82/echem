import 'package:echem/constants/strings.dart';
import 'package:echem/main.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../ui/widgets/custom_loader.dart';

class BaseMethod {
  Future<void> launchUrlToBrowser(String url) async {
    if (!await launchUrl(
      Uri.parse(url),
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  static progressDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => const CustomLoader());
  }

  static int discountPercentage({
    required int prevPrice,
    required int price,
  }) {
    var percentage = ((prevPrice - price) / prevPrice * 100).toInt();
    return percentage;
  }

  static int currencyToPriceCalc({
    required String mPrice,
  }) {
    double price = double.parse(mPrice) * (prefs.getDouble(currencyPrice) ?? 0.0);
    return price.round();
  }
  static String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Morning';
    }
    if (hour < 17) {
      return 'Afternoon';
    }
    return 'Evening';
  }
}
